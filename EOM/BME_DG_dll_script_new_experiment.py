# -*- coding: utf-8 -*-
"""
Created on Wed Jul 02 13:14:23 2014

@author: sonja
based on BME_DG_dll_script, but this is now for the new firmware of the card
used for the top Phases with externatl clock and gate and delay list

    # all default values correspond to Fabian's setting with internal clock
"""

import ctypes as C
import numpy as np
import sys
import os
from glob import glob
from time import sleep

sys.path.append("..\DataAnalysis")
import BME_DG_dll_script as BME_dll

import logging
import logging.config

logging.config.fileConfig('..\\..\\loggingConfig.txt')
log = logging.getLogger("myLogger.BME_DG_dll_script")


class FuncFilter(logging.Filter):
    def __init__(self, name=None):
        pass

    def filter(self, rec):
        if rec.funcName in ['sum_over_grid', 'generate_errorbars']:
            return False
        else:
            return True

funcfilter = FuncFilter()
log.addFilter(funcfilter)


class DelayList(object):
    """
    Class to store the DelayList Values in a "per channel" scheme, since the
    Delay Generator DLL expects it in this format. You will probably create
    four instances of this class in your main program, one for each channel
    used and fill them from the PercolationPulseFiles.
    """
    def __init__(self, Channel, delaytime=[], DG_Number=0):
        self.Channel = Channel
        self.DG_Number = DG_Number
        self.delaytime = np.asarray(delaytime)
        self.listlength = np.alen(self.delaytime)
        self.pulsewidth = np.ones(self.listlength)*0.05
        self.stepback = np.arange(1, self.listlength+1)

    def printargs(self):
        print "Channel: "+str(self.Channel)
        print "DG_Number: "+str(self.DG_Number)
        print "delaytimes: "+str(self.delaytime)
        print "pulsewidth: "+str(self.pulsewidth)
        print "stepback: "+str(self.stepback)
        print "listlength: "+str(self.listlength)+'\n'

    def printtimes(self):
        print "Channel: "+str(self.Channel)
        print "delaytimes: "+str(self.delaytime)+'\n'

    def set_DelayList(self, delaytime):
        self.delaytime = np.asarray(delaytime)*1.
        self.listlength = np.alen(self.delaytime)
        self.pulsewidth = np.ones(self.listlength)*0.05
        self.stepback = np.arange(1, self.listlength+1)


class InhibitList(object):
    """
    Class to store the InhibitList Values, since the
    Delay Generator DLL expects it in this format.
    See also class Delaylist
    but this has 2 less class fields since they are not needed for the call of set_InhibitList()
    included TriggerSignal field (see DG_DLL.h) in 151116
    TriggerSignal should be default = 0x400 if channel F defines the list walk through
    """
    def __init__(self, delaytime=[], DG_Number=0, TriggerSignal=0x400):
        self.DG_Number = DG_Number
        self.delaytime = np.asarray(delaytime)
        self.listlength = np.alen(self.delaytime)
        self.stepback = np.arange(1, self.listlength+1)
        self.TriggerSignal = TriggerSignal

    def printargs(self):
        print "Inhibit:"
        print "TriggerSignal: "+str(self.TriggerSignal)
        print "DG_Number: "+str(self.DG_Number)
        print "delaytimes: "+str(self.delaytime)
        print "stepback: "+str(self.stepback)
        print "listlength: "+str(self.listlength)+'\n'

    def printtimes(self):
        print "inhibit channel:"
        print "delaytimes: "+str(self.delaytime)+'\n'

    def set_List(self, delaytime):
        self.delaytime = np.asarray(delaytime)*1.
        self.listlength = np.alen(self.delaytime)
        self.stepback = np.arange(1, self.listlength+1)



def readin_pattern(filename, ch1, ch2, ch3, ch4, ch5):
    """
    Function to read a PulsePatternFile and load the values into DelayList
    Objects and InhibitList Objects, respectively
    Returns four DelayList objects and one InhibitList object
    """
    log.debug("Reading in Pattern")
    data = np.genfromtxt(filename, comments='#', invalid_raise=True)
#    print data
    ch1.set_DelayList(data[:, 0])
    ch2.set_DelayList(data[:, 1])
    ch3.set_DelayList(data[:, 2])
    ch4.set_DelayList(data[:, 3])
    ch5.set_List(data[:, 4])

#    ch1.printargs()
#    ch2.printargs()
#    ch3.printargs()
#    ch4.printargs()
#    ch5.printargs()

    return ch1, ch2, ch3, ch4, ch5


def initialize_DelayLists():
    """
    Function to create four delay list objects, that are returned from this
    function.

    The hardcoded numbers are the channel selector values taken from DG_Data.h

    """
    ch1 = DelayList(2)  # corresponds to A channel
    ch2 = DelayList(3)  # corresponds to B channel
    ch3 = DelayList(4)  # corresponds to C channel
    ch4 = DelayList(5)  # corresponds to E channel, D not used because it is
                        # noisy
#    ch1.printargs()
#    ch2.printargs()
#    ch3.printargs()
#    ch4.printargs()

    return ch1, ch2, ch3, ch4


def initialize_InhibitList(TriggerSignal=0x400):
    """
    Function to creat one inhibitlist object, that are returned from this
    function.

    here no channel number expected
    """
    ch = InhibitList(TriggerSignal=TriggerSignal)
#    ch.printargs()
    return ch


def initialize_DG(DG_Number=0):
    """
    Function to initialize the DG Library and also set the expected data types
    for the called C-Functions
    new path for libraries/cal-file by Sonja Nov 2015
    """
    _mydll = C.cdll.LoadLibrary("C:\\Users\\IQO-User\\Documents\\BME_delayGenerator\\Update_151124_inhibitList4\\bmeW7\\PlxApi641.dll")
    _mydll = C.cdll.LoadLibrary("C:\\Users\\IQO-User\\Documents\\BME_delayGenerator\\Update_151124_inhibitList4\\bmeW7\\DelayGenerator.dll")

    _mydll.Test_BME_G08.restype = C.c_ulong
    _mydll.Test_BME_G08.argtypes = [C.c_ulong]

    _mydll.CalibratePathGroup.restype = C.c_ulong
    _mydll.CalibratePathGroup.argtypes = [C.POINTER(C.c_ulong), C.c_char_p]

    _mydll.CalibrateModule.restype = C.c_ulong
    _mydll.CalibrateModule.argtypes = [C.c_ulong]

    _mydll.LoadAllCardParameters.restype = C.c_ulong
    _mydll.LoadAllCardParameters.argtypes = [C.c_bool, C.c_bool, C.c_bool]

    _mydll.Set_CalibrationLevel.restype = C.c_ulong
    _mydll.Set_CalibrationLevel.argtypes = [C.c_ulong, C.c_ulong]

    _mydll.Set_G08_ClockParameters.restype = C.c_long
    _mydll.Set_G08_ClockParameters.argtypes = [C.c_bool, C.c_ulong, C.c_ulong,
                                            C.c_ulong, C.c_ulong, C.c_long]

    _mydll.Set_TriggerParameters.restype = C.c_long
    _mydll.Set_TriggerParameters.argtypes = [C.c_bool, C.c_double, C.c_double,
				C.c_ulong, C.c_ulong, C.c_bool, C.c_bool, C.c_bool,
                       C.c_bool, C.c_bool, C.c_bool, C.c_bool, C.c_bool, C.c_long]

    _mydll.Set_G08_TriggerParameters.restype = C.c_long
    _mydll.Set_G08_TriggerParameters.argtypes=[C.c_bool, C.c_double, C.c_double,
                                               C.c_bool, C.c_bool, C.c_double,
                                               C.c_double, C.c_ulong, C.c_ulong, C.c_long]

    _mydll.Set_GateFunction.restype = C.c_long
    _mydll.Set_GateFunction.argtypes=[C.c_ulong, C.c_long]

    _mydll.Set_G08_Delay.restype = C.c_ulong
    _mydll.Set_G08_Delay.argtypes = [C.c_ulong, C.c_double, C.c_double, C.c_ulong,
                                     C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                     C.c_bool, C.c_bool, C.c_bool, C.c_long]

    _mydll.Set_DelayList.restype = C.c_ulong
    _mydll.Set_DelayList.argtypes = [C.c_ulong, C.POINTER(C.c_double),
                                     C.POINTER(C.c_double),
                                     C.POINTER(C.c_ulong),
                                     C.c_long, C.c_long]

    _mydll.Set_InhibitList.restype = C.c_ulong
    _mydll.Set_InhibitList.argtypes = [C.POINTER(C.c_double),
                                     C.POINTER(C.c_ulong),
                                     C.c_long, C.c_ulong, C.c_long]

    _mydll.Set_BME_G08.restype = C.c_ulong
    _mydll.Set_BME_G08.argtypes = [C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_bool, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_bool, C.c_bool, C.c_bool,
                                   C.c_ulong, C.c_bool, C.c_ulong, C.c_ulong,
                                   C.c_ulong,
                                   C.c_ulong, C.c_bool, C.c_bool, C.c_double,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_double, C.c_ulong, C.c_ulong,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_bool, C.c_bool, C.c_bool, C.c_bool,
                                   C.c_bool, C.c_bool, C.c_bool, C.c_bool,
                                   C.c_ulong]
    result = _mydll.Reserve_DG_Data(1)
    log.debug("Result of memory allocation: %s", str(result))
    if result != 0:
        log.critical("Initialization of DG failed... aborting Program!")
        sys.exit(result)
    result = _mydll.DetectPciDelayGenerators()
    log.debug('No. of PCI Generators detected: %s', str(result))
    result = _mydll.Initialize_DG_BME(0, C.c_ulong(46), 0)
    # C.c_  ulong(46) means Type BME_SG08P3	/ Value taken from DG_Data.h
    log.debug("Result of initialization routine: %s", str(result))
    if result != 0:
        log.critical("Initialization of DG failed... aborting Program!")
        sys.exit(result)
    result = BME_dll.Set_CalibrationLevel(_mydll)
    if result != 0:
        log.critical("Initialization of DG failed... aborting Program!")
        sys.exit(result)
    result = BME_dll.ReadCalibrationConstants(_mydll)
    if result != 0:
        log.critical("Initialization of DG failed... aborting Program!")
        sys.exit(result)
#    CalibrateModule(mydll)
    result = BME_dll.CalibratePathGroup(_mydll, np.asarray([1]),
                                r"C:\\Users\\IQO-User\\Documents\\BME_delayGenerator\\Update_151124_inhibitList4\\bmeW7\\")
    if result != 0:
        log.critical("Initialization of DG failed... aborting Program!")
        sys.exit(result)
    return _mydll


def Set_G08_ClockParameters(_mydll, ClockEnable=True, OscillatorDivider=2,
                            TriggerDivider=1, TriggerMultiplier=1,
                            ClockSource=1, DG_number=0):
    """
    Call this routine to set the clock parameters of a delay generator
    long Set_G08_ClockParameters(BOOL ClockEnable, unsigned long OscillatorDivider,
				unsigned long TriggerDivider, unsigned long TriggerMultiplier,
				unsigned long ClockSource, long DG_Number);
    """
    result = _mydll.Set_G08_ClockParameters(ClockEnable, OscillatorDivider,
                                            TriggerDivider, TriggerMultiplier,
                                            ClockSource, DG_number)
    return result


def Set_TriggerParameters(_mydll, TriggerTerminate=True, InternalClock=1, TriggerLevel=0.5,
				PresetValue=0x0, GateDivider=1, PositiveGate=True,
                       InternalTrigger=True, InternalArm=False, SoftwareTrigger=False,
                       RisingEdge=True, StopOnPreset=False, ResetWhenDone=True,
                       TriggerEnable=True, DG_Number=0):
    """
    Call this routine to set the trigger parameters of a delay generator
    long Set_TriggerParameters(BOOL TriggerTerminate, double InternalClock, double TriggerLevel,
				unsigned long PresetValue, unsigned long GateDivider
				BOOL PositiveGate, BOOL InternalTrigger, BOOL InternalArm,
                       BOOL SoftwareTrigger, BOOL RisingEdge, BOOL StopOnPreset,
				BOOL ResetWhenDone, BOOL TriggerEnable, long DG_Number)
    """
    result = _mydll.Set_TriggerParameters(TriggerTerminate, InternalClock,
                                          TriggerLevel, PresetValue, GateDivider,
                                          PositiveGate, InternalTrigger,
                                          InternalArm, SoftwareTrigger, RisingEdge,
                                          StopOnPreset, ResetWhenDone,
                                          TriggerEnable, DG_Number)
    return result


def Set_G08_TriggerParameters(_mydll, GateTerminate=True, GateLevel=1.0, GateDelay=-1,
                                              IgnoreGate=True, SynchronizeGate=False,
                                              ForceTrigger=-1, StepBackTime=-1,
                                              BurstCounter=1,
                                              MS_Bus=0x1, DG_Number=0):
    """
    Call this routine to set the additional trigger parameters of the BME_SG08p delay generator
    long Set_G08_TriggerParameters(BOOL GateTerminate, double GateLevel, double GateDelay
				BOOL IgnoreGate, BOOL SynchronizeGate, double ForceTrigger
				double StepBackTime, unsigned long BurstCounter,
				unsigned long MS_Bus, long DG_Number)
    """
    result = _mydll.Set_G08_TriggerParameters(GateTerminate, GateLevel, GateDelay,
                                              IgnoreGate, SynchronizeGate,
                                              ForceTrigger, StepBackTime, BurstCounter,
                                              MS_Bus, DG_Number)
    return result


def Set_GateFunction(_mydll, GateFunction=0, DG_Number=0):
    """
    long Set_GateFunction(unsigned long GateFunction, long DG_Number);
    """
    result = _mydll.Set_GateFunction(GateFunction, DG_Number)
    return result


def Set_G08_Delay(_mydll, Channel=1, FireFirst=0, PulseWidth=0.05, OutputModulo=1,
                                  OutputOffset=0, GoSignal=0x810, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0):
    """
    Call this routine to set the delay parameters of a BME_G08p module
    long Set_G08_Delay(unsigned long Channel, double FireFirst, double PulseWidth,
			unsigned long OutputModulo, unsigned long OutputOffset,
			unsigned long GoSignal, BOOL Positive, BOOL Terminate,
			BOOL Disconnect, BOOL OntoMsBus, BOOL InputPositive, long DG_Number)
    """
    result = _mydll.Set_G08_Delay(Channel, FireFirst, PulseWidth, OutputModulo,
                                  OutputOffset, GoSignal, Positive, Terminate,
                                  	Disconnect, OntoMsBus, InputPositive, DG_Number)
    return result


def set_DelayList(_mydll, ch):
    """
    long CDG_DLL::Set_DelayList(unsigned long Channel, double* p_DelayTime, double* p_PulseWidth,
    			unsigned long* p_StepBack, signed long ListLength, long DG_Number)
    """
    result = _mydll.Set_DelayList(
        ch.Channel, ch.delaytime.ctypes.data_as(C.POINTER(C.c_double)),
        ch.pulsewidth.ctypes.data_as(C.POINTER(C.c_double)),
        ch.stepback.ctypes.data_as(C.POINTER(C.c_ulong)),
        ch.listlength, ch.DG_Number)
    log.debug("Result of Set_DelayList: %s", str(result))
    return result


def set_InhibitList(_mydll, ch):
    """
    new function similar to set_DelayList for the setting of the inhibits
    (new feature in 2015)
    TriggerSignal new in 151116
    long CDG_DLL::Set_InhibitList(double* p_InhibitTime, unsigned long* p_StepBack,
                                  signed long ListLength, unsigned long TriggerSignal, long DG_Number)
    """
    result = _mydll.Set_InhibitList(
        ch.delaytime.ctypes.data_as(C.POINTER(C.c_double)),
        ch.stepback.ctypes.data_as(C.POINTER(C.c_ulong)),
        ch.listlength, ch.TriggerSignal, ch.DG_Number)
    log.debug("Result of Set_InhibitList: %s", str(result))
    return result


def init_allParams(mydll, Number_of_EOMs):
    """
    function to call all set functions for trigger, gate and clock parameters
    with default values for using external clock and gate in the burst mode
    """
    res = Set_G08_ClockParameters(mydll, ClockEnable=True, OscillatorDivider=2,
                            TriggerDivider=1, TriggerMultiplier=1,
                            ClockSource=3, DG_number=0)
    res += Set_TriggerParameters(mydll,TriggerTerminate=True, InternalClock=-1, TriggerLevel=0.5,
				PresetValue=0x0, GateDivider=1, PositiveGate=True,
                       InternalTrigger=True, InternalArm=False, SoftwareTrigger=False,
                       RisingEdge=True, StopOnPreset=False, ResetWhenDone=True,
                       TriggerEnable=False, DG_Number=0)
    res += Set_G08_TriggerParameters(mydll, GateTerminate=True, GateLevel=0.5, GateDelay=-1,
                                              IgnoreGate=True, SynchronizeGate=False,
                                              ForceTrigger=2000, StepBackTime=-1,
                                              BurstCounter=31,
                                              MS_Bus=0x1, DG_Number=0)
    res += Set_GateFunction(mydll, GateFunction=0, DG_Number=0)
    # handle delay channels
    res += Set_G08_Delay(mydll,Channel=2, FireFirst=0, PulseWidth=0.05, OutputModulo=1,   #Aon A
                                  OutputOffset=0, GoSignal=0x410, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0)
    res += Set_G08_Delay(mydll,Channel=3, FireFirst=0, PulseWidth=0.05, OutputModulo=1,     #Bon B
                                  OutputOffset=0, GoSignal=0x410, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0)
    res += Set_G08_Delay(mydll,Channel=4, FireFirst=0, PulseWidth=0.05, OutputModulo=1,   #Aoff   C
                                  OutputOffset=0, GoSignal=0x410, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0)
    res += Set_G08_Delay(mydll,Channel=5, FireFirst=0, PulseWidth=0.05, OutputModulo=1,   #Boff D
                                  OutputOffset=0, GoSignal=0x410, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0)
    #handle E and F channels
    res += Set_G08_Delay(mydll,Channel=6, FireFirst=49, PulseWidth=0.05, OutputModulo=1,   #E   Oszi
                                  OutputOffset=0, GoSignal=0x410, Positive=True,
                                  Terminate=True,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0)
    res += Set_G08_Delay(mydll,Channel=7, FireFirst=29, PulseWidth=0.05, OutputModulo=1,     #F    Reset
                                  OutputOffset=0, GoSignal=0x410, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=True,
                                  InputPositive=True, DG_Number=0)
    return res


def main(argv):
    
    Number_of_EOMs=3

    mydll = initialize_DG()
    res = init_allParams(mydll, Number_of_EOMs)


# set inhibit and delay lists manually
    # handle inhibits
#    inhibit  = initialize_InhibitList(TriggerSignal=0x400)
#    inhibit.set_List([1,.2,.4,.5,.8,2])
#    res += set_InhibitList(mydll, inhibit)
#
#    # handle delays
#    chA, chB, chC, chD = initialize_DelayLists()
#    chA.set_DelayList([0,0,0,0,0,0])
#    chB.set_DelayList([0.05,0.1,0.2,0.3,0.4,0.5])
#    chC.set_DelayList([0,0,0,0,0,0])
#    chD.set_DelayList([0,0,0,0,0,0])
#    res += set_DelayList(mydll, chA)
#    res += set_DelayList(mydll, chB)
#    res += set_DelayList(mydll, chC)
#    res += set_DelayList(mydll, chD)

# load lists from file
#    filename =r'C:\Users\IQO-User\Desktop\DTQW\TopologicalPulseFiles\20151007_topBorders_inclLastSwitch_instep9\01_semi_side_1-nu-2nu_9noswitch_pulses_out.fpm'
#    # initialization of class instances
#    ch1, ch2, ch3, ch4 = initialize_DelayLists()
#    inhibit  = initialize_InhibitList(TriggerSignal=0x400)
##    # fill class fields with switch times from pattern
#    ch1, ch2, ch3, ch4, inhibit = readin_pattern(filename, ch1, ch2, ch3, ch4, inhibit)
#        # wrapper function to call dll-cpp function set_DelayList and set_InhibitList
#    set_DelayList(mydll, ch1)
#    set_DelayList(mydll, ch2)
#    set_DelayList(mydll, ch3)
#    set_DelayList(mydll, ch4)
#    set_InhibitList(mydll, inhibit)
##    # load all parameters to the card and activate DG
#    BME_dll.load_params(mydll)
#    BME_dll.activate_DG(mydll, DG_Number=0)


# try a loop
    pulsefilepath = r'C:\Users\IQO-User\Desktop\DTQW\TopologicalPulseFiles\20151007_topBorders_inclLastSwitch_instep9\Noise'
    pulsefilelist = glob(os.path.join(pulsefilepath, '*_out.fpm'))[0:50]
    ch1, ch2, ch3, ch4 = initialize_DelayLists()
    inhibit  = initialize_InhibitList(TriggerSignal=0x400)
    print "Starting with Pattern: "+ str(os.path.basename(pulsefilelist[0]))
    ch1, ch2, ch3, ch4, inhibit = readin_pattern(pulsefilelist[0], ch1, ch2, ch3, ch4, inhibit)
    set_DelayList(mydll, ch1)
    set_DelayList(mydll, ch2)
    set_DelayList(mydll, ch3)
    set_DelayList(mydll, ch4)
    set_InhibitList(mydll, inhibit)
    BME_dll.load_params(mydll)
    BME_dll.activate_DG(mydll, DG_Number=0)
    raw_input('done yet?')

    for filename in pulsefilelist[1:]:   # go through all pulsefiles
        print "Starting with Pattern: "+ str(os.path.basename(filename))
        ch1, ch2, ch3, ch4, inhibit = readin_pattern(filename, ch1, ch2, ch3, ch4, inhibit)
        set_DelayList(mydll, ch1)
        set_DelayList(mydll, ch2)
        set_DelayList(mydll, ch3)
        set_DelayList(mydll, ch4)
        set_InhibitList(mydll, inhibit)
        BME_dll.load_params(mydll)
#        BME_dll.activate_DG(mydll, DG_Number=0)   #necessary only once
        raw_input('done yet?')

#    raw_input('done yet?')
    BME_dll.deactivate_DG(mydll)
    BME_dll.shutdown_DG(mydll)
    logging.shutdown()

if __name__ == "__main__":
    mydll = main(sys.argv[1:])
