# -*- coding: utf-8 -*-
"""
Created on 14 July 2015

File for Timing Calculations for the new EOM with variable inhibits (two
 particle setup and new card for old EOM in blue lab)

@author: barkhofen
"""
import os
import sys
sys.path.append("..\DataAnalysis")
sys.path.append("..\TopologicalPhases")
from glob import glob
import numpy as np
#import scitools.pprint2 as pp
import timing_calculation_new_experiment as tc
#import pprint as pp
#from collections import deque
from itertools import izip
import matplotlib.pyplot as plt

#import save_settings_tofile as sstf

import logging
import logging.config
import logging.handlers

logging.config.fileConfig('loggingConfig.txt')
log = logging.getLogger("myLogger.timing_calculation")


class FuncFilter(logging.Filter):
    def __init__(self, name=None):
        pass

    def filter(self, rec):
        if rec.funcName in ['sum_over_grid']:
            return False
        else:
            return True

funcfilter = FuncFilter()
log.addFilter(funcfilter)

setting_label = '# Settings for Timing_calculation: '

settings = dict(            #step_delta and pulse_delta extracted by Sonja from
    step_delta=2294.567156e-9,#2.294484851e-6,  # 2.19863e-6 Round trip time in the experiment for the shortest 
                           # possible route
    step2_delta=678.73e-9,  # possibility to have different RTT's for splitstep
                            # versions of the walk
    split_step=0,           # if 0 step2_delta has no effect, if 1 step2_delta
                            # will be used
    pulse_delta=104.691324e-9,#0.1047736285e-6,   # =0.113676e-6 time distance created by the fiber length
                            # difference
    max_steps=5,   # Number of steps to simulate for checking for
                   # possible overlaps between empty switchings and
                   # actual pulse positions
    max_ini_pos=3, #Max number of positions occupied by the initial state
    window_width=14e-9,  # 31e-9 defines how wide the EOM = on window is
    EOM_max_on_time=40e-9,  # defines the maximal time the EOM can be on > window width
    safety_distance=1e-8,  # region around actual pulses, that may not contain
                           # empty switchings
    max_inhibit_length=5e-6,  #Max length of inhibit
    min_inhibit_length=100e-9,
    min_last_inhibit_length=2e-6,
    min_distance_to_inhibit=50e-9,
    empty_switch_distance=1e-7,   # distance of empty s7witch to real switch,
                                # also used for length of last inhibit
    single_switch_min_dist=60e-9,  # hard limit of EOM is 50e-9
    allfiles=1,       # set this flag to load all the pulse-files in path
    sameParams=1,   #allfiles should get the same length and number of switches
    numberofEOMs=1, #Number of EOMs in use
    initial_empty_switch=1, #Is an initial empty switching to be inserted?
    initial_time=50e-9,     #Time after which the initial empty switching occures
    rearrange_inhibits=1,    #Are the inhibits to be rearranged by get_relative_times_within_limits?
    round_inhibits=1,       #Are the inhibits to be rounded to multiples of the clock periode?
    clock_frequency=80e6, #Clock frequency to determine the clock periode 76.401 for MIRA(03.08.2016)
#    sameInhibit=1,
    path=r"\\131.234.203.7\QW_Data\Sampling\Test_four_EOMs\Discrete_HOM_MIRA\Test",
    myfile="outcoupling in step18_pulses",
    outfile="",  # "_out",
    outfile2="",  # "_human_readable",
#    std_pulse_length=0.01,
#    fpga_step=12.5e-9,  # Internal Speed of the FPGA (usually 12,5ns = 80 MHz)
    modulo_on=0,  # Switch to toggle that all calculated times are integer
#                  # multiples of fpga_step

    first_delay= 0,   # since the DelayGen produces different offsets per
    second_delay= 0,  # channel usually pairwise, we need an option to
    third_delay= 0,   # already include this in the file generation
    fourth_delay= 0,  # Delay values could be + or - and have to be in s
    # typical values are 3 ns and -9 ns (usually sums up to 12)
    # The numbering refers to the colum in the resulting files
)



def compile_pulse_list(lines, settings):
    """
    based on compile_pulse_time_list in timing_calculation.py
    Function to extract which walker pulses are to be switched and
    to define what kind of switching has to happen
    Note: Redefinition of step counting compared to previous version (count
    from zero so that even the zeroth pulse can be switched)
    Note: type of pulse_specs changes, since neighbouring pulses should not be
    switched together but separate, if a switch would last longer than
    settings['EOM_max_on_time']
    """
    pulse_specs = []            # list which includes all infos about switch
    pulse_type = []             # list which includes only pulse types
    for step, pulse_list in enumerate(lines):
        step = step           # Because steps start at 1 and not at 0
        pulse_list = pulse_list.split()
        for pos_item in pulse_list:
            if tc.RepresentsInt(pos_item):
                if tc.check_pulses(step, pos_item, settings['max_ini_pos']):
                    time = tc.calculate_pulse_time(settings, step, int(pos_item), settings['max_ini_pos'])
#                    print("Step %s Pulse %s has time %s", str(step), str(pos_item), str(time))
                    pulse_specs.append([step, int(pos_item), time, 0])
            else:
                if (pos_item == 's' or pos_item == 'S'):  # nothing has to happen ->skip
                    pass
                else:
                    pulse_type.append(pos_item)
    for index in np.arange(np.alen(pulse_specs)):
        try:
            pulse_specs[index].append(pulse_type[index])
        except:
            log.warn("\nError in list combination of times and switch type\n" +
                     "There is probably an error in the pulse file!\n")
            sys.exit(1)

    pulse_times = set_pulse_switching_mode(pulse_specs)
    result, result_type = tc.create_switching_times(settings, pulse_times)
    return result, result_type


def set_pulse_switching_mode(pulse_specs):
    """
    based on set_pulse_switching_mode.py in timing_calculation
    Function to determine if a real switching has to occur or if you
    "can leave the EOM on" because of the previous pulse. The pulse switching
    mode is stored in the 4th column in pulse times.
    0 = switch on and off, 1 = switch only on, 2 = switch only off
    Takes now settings['EOM_max_on_time'] into account (in assign_switching_state)
    """
    #generate only a list of the timings
    result_list = []
    start = [[0, 0, 0, 0, 0]]
    for item in pulse_specs:
        start.append(item)
    start.append([0, 0, 0, 0, 0])
    start = np.array(start)

    for idx in np.arange(1, np.alen(start)-1):
        assign_switching_state(idx, start)
    for item in start:
        if item[4] != '0':   # only if switch_type is not zero -> valid pulse
            result_list.append(item)
#    print("Result_list: \n %s", pp.pformat(result_list))
    return result_list


def assign_switching_state(idx, start):
    """
    set switching mode to 0 for no neighbor, to 1 for the first neigbor, to 2
    for last neigbor and to 3 for intermediate neigbor. Also detects adjacent
    pulses, where we have to switch from r to t or vice versa
    Takes now settings['EOM_max_on_time'] as additional condition into account
    It is not a very clean code :(  -> too many different cases
    """
    neighbour_specs = start[:idx,3]    # all previous pulse neighbourhood specs
    dum1 = np.where(neighbour_specs == '1')[0]  # where does the group begun?
    dum2 = np.where(neighbour_specs == '2')[0]
    dum0 = np.where(neighbour_specs == '0')[0]
    if len(dum2) > 0:
        endgroup = max(np.concatenate((dum2, dum0)))
    elif len(dum0) > 0:
        endgroup = max(dum0)
    else:
        endgroup = 0
    if (len(dum1) > 0 and max(dum1) > endgroup):   # then a neighbour group has started
        first_neighbour = start[max(dum1)]
    else:
        first_neighbour = start[idx]        # no left neighbours in the group, but right neighbour?
        if (start[idx, 0] == start[idx+1, 0] and  # same step? (right neighbour)
              start[idx, 4] == start[idx+1, 4] and  # same type?
              abs(int(start[idx, 1]) - int(start[idx+1, 1])) == 2 and  # neighboured positions? e.g. -1 and 1?
              (np.abs(np.float(start[idx+1,2])-np.float(first_neighbour[2]))
              + settings['window_width'] <= settings['EOM_max_on_time'])):   # is switched pulse not too long?
              start[idx, 3] = 1
              return
        else:
            start[idx, 3] = 0
            return

    if (start[idx, 0] == start[idx-1, 0] and    # same step? (left neighbour)
        start[idx, 4] == start[idx-1, 4] and    # same type?
        start[idx, 0] == start[idx+1, 0] and    # same step? (right neighbour)
        start[idx, 4] == start[idx+1, 4] and    # same type?
        abs(int(start[idx, 1]) - int(start[idx-1, 1])) == 2 and     # neighboured positions? e.g. -3 and -1 and 1?
            abs(int(start[idx, 1]) - int(start[idx+1, 1])) == 2 and
            (np.abs(np.float(start[idx+1,2])-np.float(first_neighbour[2]))
            + settings['window_width'] <= settings['EOM_max_on_time'])):   # is switched pulse not too long?
        start[idx, 3] = 3
        return
    elif (start[idx, 0] == start[idx-1, 0] and  # same step? (left neighbour)
          start[idx, 4] == start[idx-1, 4] and  # same type?
          abs(int(start[idx, 1]) - int(start[idx-1, 1])) == 2 and # neighboured positions? e.g. -3 and -1?
          (np.abs(np.float(start[idx,2])-np.float(first_neighbour[2]))
          + settings['window_width'] <= settings['EOM_max_on_time'])):   # is switched pulse not too long?
        start[idx, 3] = 2
        return
    elif (start[idx, 0] == start[idx+1, 0] and  # same step? (right neighbour)
          start[idx, 4] == start[idx+1, 4] and  # same type?
          abs(int(start[idx, 1]) - int(start[idx+1, 1])) == 2 and  # neighboured positions? e.g. -1 and 1?
          (np.abs(np.float(start[idx+1,2])-np.float(first_neighbour[2]))
          + settings['window_width'] <= settings['EOM_max_on_time'])):   # is switched pulse not too long?
        start[idx, 3] = 1
        return


def add_empty_switchings(times, types, settings, pulse_pos, numberofswitch = -1):
    """
    function to add empty switchings at the end of times until numberofswitch
    is reached
    If this is not set then fill up to the next multiple of four
    """
    # check for walker pulses first
    last_switch_time = times[-1]
    tmp = settings['empty_switch_distance'] + last_switch_time
    while np.min(np.abs(tmp-pulse_pos)) <= settings['empty_switch_distance']:
        tmp+=settings['empty_switch_distance']
    if numberofswitch == -1:
        if np.alen(times) % 4 != 0:   # odd number of switchings?-> add empty switch
            times = np.append(times, settings['empty_switch_distance'] + last_switch_time)
            times = np.append(times, settings['empty_switch_distance'] + last_switch_time)
            types = np.append(types, 'e')
            types = np.append(types, 'e')
    else:   # if the desired number of switchings is given
        diff = numberofswitch-np.alen(times)
        for d in np.arange(1, diff/2):    # gives [] if diff < 0

                times = np.append(times, last_switch_time + d*settings['empty_switch_distance'])
                times = np.append(times, last_switch_time + d*settings['empty_switch_distance'])
                types = np.append(types, 'e')
                types = np.append(types, 'e')
    return times, types
    
def add_spaced_empty_switchings(times, inhibits, types, settings, pulse_pos, max_time, numberofswitch = -1):
    """
    function to add empty switchings at the end of times until numberofswitch
    is reached, the function makes sure that there are no big jumps in the inhibit length
    If this is not set then fill up to the next multiple of four
    """
#    max_time=max_time+1e-6
#    numberofswitch=numberofswitch+4
    # check for walker pulses first
    last_switch_time = times[-1]
    distance=max_time-last_switch_time
    
#    print 'ini inhibits'
#    
#    print inhibits
    
#    print 'distance'
#    
#    print distance
    
    tmp = last_switch_time

    while np.min(np.abs(tmp-pulse_pos)) <= settings['empty_switch_distance']:
        tmp+=settings['empty_switch_distance']
    if numberofswitch == -1:
        if np.alen(times) % 4 != 0:   # odd number of switchings?-> add empty switch
            times = np.append(times, settings['empty_switch_distance'] + last_switch_time)
            times = np.append(times, settings['empty_switch_distance'] + last_switch_time)
            types = np.append(types, 'e')
            types = np.append(types, 'e')
    else:   # if the desired number of switchings is given
        diff = numberofswitch-np.alen(times)
        
#        print 'diff'
#        
#        print diff
        
        nr=numberofswitch/4
        

        switch_distance=distance/diff*4     #Choosing the switch distance such that the gap between 
#                                                                                # last_time and max_time is bridged without inhibitis getting too long
#
        for d in np.arange(1, (diff/4)+1):    # gives [] if diff < 0

                times = np.append(times, last_switch_time - settings['min_distance_to_inhibit'] - settings['empty_switch_distance']+d*switch_distance)
                times = np.append(times, last_switch_time - settings['min_distance_to_inhibit'] - settings['empty_switch_distance']+d*switch_distance)
                times = np.append(times, last_switch_time - settings['min_distance_to_inhibit']+(d)*switch_distance)
                times = np.append(times, last_switch_time - settings['min_distance_to_inhibit']+(d)*switch_distance)
                types = np.append(types, 'e')
                types = np.append(types, 'e')
                types = np.append(types, 'e')
                types = np.append(types, 'e')
                
                #Inserting corresponding inhibit
                
                inhibits=inhibits[0:nr]
                
#                print np.alen(inhibits)
                
#                print 'inhibit append'
#                
#                print last_switch_time + d*switch_distance
                
                inhibits = np.append(inhibits, last_switch_time + d*switch_distance)
                
#                print inhibits
                
#       for d in np.arange(1, (diff/4)+1):    # gives [] if diff < 0
#    
#                    times = np.append(times, last_switch_time + d*2*settings['min_distance_to_inhibit']+(d-1)*switch_distance)
#                    times = np.append(times, last_switch_time + d*2*settings['min_distance_to_inhibit']+(d-1)*switch_distance)
#                    times = np.append(times, last_switch_time + d*2*settings['min_distance_to_inhibit']+(d)*switch_distance)
#                    times = np.append(times, last_switch_time + d*2*settings['min_distance_to_inhibit']+(d)*switch_distance)
#                    types = np.append(types, 'e')
#                    types = np.append(types, 'e')
#                    types = np.append(types, 'e')
#                    types = np.append(types, 'e')
#                    
#                    #Inserting corresponding inhibit
#                    
#                    inhibits=inhibits[0:nr+1]
#                    
#                    inhibits = np.append(inhibits, last_switch_time + d*2*settings['min_distance_to_inhibit']+(d)*switch_distance+settings['min_distance_to_inhibit'])               
                
#        print 'new nr of switches'
#        
#        print np.alen(times) 
#        
#        print 'new times'
#        
#        print times
    return times, types, inhibits
    
    
#    
#def add_adapt_empty_switchings(times, types, settings, pulse_pos, inhibits_starts, numberofswitch = -1):
#    """
#    function to add empty switchings at the end of times until numberofswitch
#    is reached
#    If this is not set then fill up to the next multiple of four
#    """
#    # check for walker pulses first
#    last_switch_time = inhibits_starts[-1]
#    tmp = settings['empty_switch_distance'] + last_switch_time
#    while np.min(np.abs(tmp-pulse_pos)) <= settings['empty_switch_distance']:
#        tmp+=settings['empty_switch_distance']
#    if numberofswitch == -1:
#        if np.alen(times) % 4 != 0:   # odd number of switchings?-> add empty switch
#            times = np.append(times, tmp)
#            times = np.append(times, tmp)
#            types = np.append(types, 'e')
#            types = np.append(types, 'e')
#    else:   # if the desired number of switchings is given
#        diff = numberofswitch-np.alen(times)
#        for d in np.arange(0, diff/2):    # gives [] if diff < 0
#                times = np.append(times, tmp + d*settings['empty_switch_distance'])
#                times = np.append(times, tmp + d*settings['empty_switch_distance'])
#                types = np.append(types, 'e')
#                types = np.append(types, 'e')
#    return times, types

def ensure_last_inhibit(settings, inhibits_starts):
    
    clock_periode = 1/settings['clock_frequency']
    
    inhibit_length = []
    
    temp_inhibits_starts=inhibits_starts[1:]
    
    temp=0
    
    for start_time in temp_inhibits_starts:
        
        inhibit_length.append(start_time-temp)
            
        temp=start_time
        
    
#    print inhibit_length
#        
#    print 'length'
#    
#    print inhibit_length[-1]
        
    if inhibit_length[-1]<settings['min_last_inhibit_length']:
        
        inhibits_starts[-1]=np.abs(inhibits_starts[-1])+np.ceil(settings['min_last_inhibit_length']/clock_periode)*clock_periode
        inhibit_length[-1]=np.abs(inhibit_length[-1])+np.ceil(settings['min_last_inhibit_length']/clock_periode)*clock_periode
#    print np.alen(inhibits_starts)
        
    
#        
#    print inhibits_starts
##        
#    print inhibit_length
#        
        
    return inhibit_length, inhibits_starts
        
        
        
        
        
        
        
        
        
    
    


def get_inhibits(times, settings, max_length = -1, last_inhibit_extra_length = 2e-6):
    """
    Function to set the borders of the inhibits, makes sure that inhibits are
    a multiple of 12.5 ns (clock period of delay generator)
    last inhibit gets an extra length of last_inhibit_extra_length
    """
    clock = 1/settings['clock_frequency']     #[s]
    nr = np.alen(times) / 4.   # should be an integer!
    # inhibit start should be the same for all patterns -> no need to search a delay of the osci channel for every pattern
    inhibit_start = 0  #np.floor(np.min(times)/clock)*clock   # first inhibit starts with first switch (resp. mit clock multiple before first switch)
    inhibit_length = []
    inhibit_starts = [inhibit_start]
    for i in np.arange(1,nr):
        inhibit_stop = times[i*4-1] + 0.5*(times[i*4]-times[i*4-1]) # stops between pulse 4 and 5 (or 8 and 9...)
        inhibit_stop = round(np.min(inhibit_stop)/clock)*clock
        inhibit_length.append(inhibit_stop-inhibit_start)
#        if inhibit_length > settings['max_inhibit_length']:     # Making sure inhibit length does not exceed maximum length
#            inhibit_stop = round(inhibit_starts+settings['max_inhibit_length'])
            
            
        inhibit_start = inhibit_stop
        inhibit_starts.append(inhibit_start)
    if max_length == -1 or max_length < (np.max(times) + settings['empty_switch_distance']+last_inhibit_extra_length):
        print 'add'
        inhibit_stop = np.max(times) + settings['empty_switch_distance']+last_inhibit_extra_length  # last inhibit ends with last switch + safety distance of 2mus
        inhibit_stop = round(np.min(inhibit_stop)/clock)*clock
    else:
        inhibit_stop = max_length
        inhibit_stop = round(np.min(inhibit_stop)/clock)*clock
    inhibit_length.append(inhibit_stop-inhibit_start)
    inhibit_starts.append(inhibit_stop)
    return inhibit_length, inhibit_starts


def generate_plot(settings, pulse_pos, times, types, inhibit_start):
    """
    Function to plot the timings, inhibits,...
    """
    plt.figure()
    plt.xlabel('time [ns]')
    plt.xlim([0, np.max(inhibit_start)*1e9+100])
    plt.plot(pulse_pos*1e9,  0.5*np.ones(len(pulse_pos)), 'o', color='b')
    for i in inhibit_start:
        plt.vlines(i*1e9, 0, 1, color = 'g')
    for i in range(0,len(times),2):
        if types[i] == 'r':
            plt.axvspan(times[i]*1e9, times[i+1]*1e9, color = 'orange', alpha=0.3)
        elif types[i] == 't':
            plt.axvspan(times[i]*1e9, times[i+1]*1e9, color = 'r', alpha=0.3)
        elif types[i] == 'e':
            plt.vlines(times[i]*1e9, 0, 1, color = 'k')
    plt.legend()
    
    
    
def get_relative_times(times, inhibits):
    """
    function to translate the switch times into times relative to the inhibit start
    """
    nr = np.alen(times)/4
    time_rel = []
    for i in np.arange(0, nr):
        time_rel=np.concatenate((time_rel, times[4*i:4*(i+1)]-inhibits[i]))
        
        if np.min(times[4*i:4*(i+1)]-inhibits[i])<0:
            
            print 'Negative entry!!!'
        
    return time_rel

def round_inhibits(inhibits):
    
    """
    function to round inhibits to multiples of clock periode
    """
    clock_periode=1./settings['clock_frequency']
    
    for i in np.arange(0,np.size(inhibits)):
        
#        print 'old'
#        
#        print inhibits[i]
          
        rounded_multiples=np.ceil(inhibits[i]/clock_periode)
        inhibits[i]=rounded_multiples*clock_periode
        
#        print 'new'
#        
#        print inhibits[i]
#        
#    print inhibits
    
    return inhibits
    

def round_number(number):
    
    """
    function to round numbers to multiples of clock periode
    """
    clock_periode=1./settings['clock_frequency']
    
    rounded_multiples=np.ceil(number/clock_periode)
    number=rounded_multiples*clock_periode
    
    return number
    
    
    
    

def get_relative_times_within_limits(times, inhibits, types, ini_inhibit_counter):
    """
    function to translate the switch times into times relative to the inhibit start and makes sure that they are not longer than 
    max_inhibit_length
    """
    nr = np.alen(times)/4
    time_rel = []

    n=0         #Number of inserted inhibits
    m=0         #Number of inserted empty switchings
    i=1
    
    
    # Rounding quantities to multiples of clock periode
    
    min_distance_to_inhibit=round_number(settings['min_distance_to_inhibit'])
    
#    print 'min_dist'
#    
#    print min_distance_to_inhibit
    


    # Inserting empty switching when desired

    if (settings['initial_empty_switch']==1 and ini_inhibit_counter==0):
        
            
            
            times+=(settings['initial_time']+100e-9+min_distance_to_inhibit)
        


            #Insert two empty switchings with a distance of 100 ns
            

            
            times=np.insert(times, 0, settings['initial_time']+100e-9)
            times=np.insert(times, 0, settings['initial_time']+100e-9)
            
            times=np.insert(times, 0, settings['initial_time'])
            times=np.insert(times, 0, settings['initial_time'])
            
            time_rel=np.insert(time_rel, 0, settings['initial_time']+100e-9)
            time_rel=np.insert(time_rel, 0, settings['initial_time']+100e-9)
            
            time_rel=np.insert(time_rel, 0, settings['initial_time'])
            time_rel=np.insert(time_rel, 0, settings['initial_time'])
            
            #Increasing counter for empty switches inserted to time_rel
            
            m+=1                
            
            #Inserting corresponding entry in types
            

            

            types=np.insert(types, 0, 'e')
            types=np.insert(types, 0, 'e')
            types=np.insert(types, 0, 'e')
            types=np.insert(types, 0, 'e')
            

#            
#            # Inserting corresponding inhibit

    
            inhibits = np.insert(inhibits, 1, round_number(times[3]+min_distance_to_inhibit))
            

            n+=1
                      
            # Increase counter to make sure empty switchings are not inserted a second time
            
            ini_inhibit_counter+=1
                             
        
    while i <= nr+n:
        
#    while i < 5:
        

        
        if (i+n)<=(nr+n):
        

        
            time_rel=np.concatenate((time_rel, times[4*(i-1+n):4*(i+n)]-inhibits[(i-1+n)]))
            
        print 'look here'
        
        print 'times[4*(i)-1]'
        
        print times[4*(i)-1]
        
        print 'times[4*(i)-3]'
        
        print times[4*(i)-3]
        
        print 'inhibits[i-1]'
        
        print inhibits[i-1]
#            
            
            
        # Inserting empty switching when only second switching is too late
            

        if ((times[4*(i)-1]-inhibits[i-1])>(settings['max_inhibit_length'] - min_distance_to_inhibit)) and ((times[4*(i)-3]-inhibits[i-1])<(settings['max_inhibit_length'] - min_distance_to_inhibit)):
            
            #Making sure second switching does not come behind inserted inhibit
            
            if  times[4*(i)-1]-min_distance_to_inhibit<(times[4*(i)-3]+settings['single_switch_min_dist']+settings['safety_distance']+min_distance_to_inhibit):
                
                inhibits = np.insert(inhibits, i, round_number(times[4*(i)-1]+min_distance_to_inhibit)+settings['safety_distance'])
                
                print 'ok'
                
            else:    
                print 'YEEEEEEEEEEEHHHHHHHHHHAAww'
    #            
    #            print  time_rel
                
                            
                empty_time_1_abs=(times[4*(i)-3]+settings['single_switch_min_dist']+settings['safety_distance'])
                
                empty_time_1=(empty_time_1_abs-inhibits[(i-1)])
                
#                print time_rel[4*(i)-3]
#                
#                print times[4*(i)-3]
#                
#                print empty_time_1
#                
#                print empty_time_1_abs
#                
#                print 'empty time 1'
#                
#                print empty_time_1_abs
                
                
                empty_time_2=(empty_time_1+2*min_distance_to_inhibit)
                
                empty_time_2_abs=(empty_time_1_abs+2*min_distance_to_inhibit)
                
#                print empty_time_2
#                
#                print empty_time_2_abs
#                
#                print 'empty time 2'
#                
#                print empty_time_2_abs
                
    
                
                #Insert empty switching in second part of split-up inhibit
                
                time_rel=np.insert(time_rel, 4*(i)-2, empty_time_2)
                time_rel=np.insert(time_rel, 4*(i)-2, empty_time_2)
                
                times=np.insert(times, 4*(i)-2, empty_time_2_abs)
                times=np.insert(times, 4*(i)-2, empty_time_2_abs)
                
                #Insert empty switching in first part of split-up inhibit
                
                time_rel=np.insert(time_rel, 4*(i)-2, empty_time_1)
                time_rel=np.insert(time_rel, 4*(i)-2, empty_time_1)
                
                times=np.insert(times, 4*(i)-2, empty_time_1_abs)
                times=np.insert(times, 4*(i)-2, empty_time_1_abs)
                
                
                
    
                
                #Increasing counter for empty switches inserted to time_rel
                
                m+=1                
                
                #Inserting corresponding entry in types
                
    
                
                types=np.insert(types, 4*(i)-2, 'e')
                types=np.insert(types, 4*(i)-2, 'e')
                types=np.insert(types, 4*(i)-2, 'e')
                types=np.insert(types, 4*(i)-2, 'e')
                
    
    #            
    #            # Inserting corresponding inhibit
    
#                print 'YEEEEEEEEEEEHHHHHHHHHHAAww'
#                
#                print round_number(empty_time_1_abs+min_distance_to_inhibit)
    #            
    #            
                inhibits = np.insert(inhibits, i, round_number(empty_time_1_abs+min_distance_to_inhibit))
                
    #            print 'inhibit time'
    #            
    #            print round_number(empty_time_1_abs+min_distance_to_inhibit)
    #            
    #            print inhibits
    #
                n+=1
            
  
        
        
        # Making sure the distance of switches within one inhibit is not too large
                
                
        if (times[4*(i)-1]-times[4*(i)-4]) > (settings['max_inhibit_length'] - min_distance_to_inhibit):
            
            
            print 'distance of switches within inhibit too large'
            
            print 'time_rel[4*(i)-1]'
            
            print time_rel[4*(i)-1]
            
            print 'time_rel[4*(i)-4]'
            
            print time_rel[4*(i)-4]
            
    
            empty_time_2=(time_rel[4*(i)-2]-settings['single_switch_min_dist']-settings['safety_distance'])
            
            empty_time_2_abs=(times[4*(i)-2]-settings['single_switch_min_dist']-settings['safety_distance'])
            

            
            empty_time_1=(time_rel[4*(i)-3]+settings['single_switch_min_dist']+settings['safety_distance'])
            
            empty_time_1_abs=(times[4*(i)-3]+settings['single_switch_min_dist']+settings['safety_distance'])
            

            
            #Insert empty switching in second part of split-up inhibit
            
            time_rel=np.insert(time_rel, 4*(i)-2, empty_time_2)
            time_rel=np.insert(time_rel, 4*(i)-2, empty_time_2)
            
            times=np.insert(times, 4*(i)-2, empty_time_2_abs)
            times=np.insert(times, 4*(i)-2, empty_time_2_abs)
            
            #Insert empty switching in first part of split-up inhibit
            
            time_rel=np.insert(time_rel, 4*(i)-2, empty_time_1)
            time_rel=np.insert(time_rel, 4*(i)-2, empty_time_1)
            
            times=np.insert(times, 4*(i)-2, empty_time_1_abs)
            times=np.insert(times, 4*(i)-2, empty_time_1_abs)
            
            
            

            
            #Increasing counter for empty switches inserted to time_rel
            
            m+=1                
            
            #Inserting corresponding entry in types
            

            
            types=np.insert(types, 4*(i)-2, 'e')
            types=np.insert(types, 4*(i)-2, 'e')
            types=np.insert(types, 4*(i)-2, 'e')
            types=np.insert(types, 4*(i)-2, 'e')
            

#            
#            # Inserting corresponding inhibit
#
#            print 'distance of switches within inhibit too large'
#            
#            print round_number(inhibits[i-1]+empty_time_1+min_distance_to_inhibit)
##            
#            
            inhibits = np.insert(inhibits, i, round_number(inhibits[i-1]+empty_time_1+min_distance_to_inhibit))
#
            n+=1
        
        #Making sure that inhibits are not longer than necessary now also accounting for rounding
        

                    
        if inhibits[i] > round_number((np.max(times[4*(i)-4:4*(i)])+min_distance_to_inhibit)):
            
            
            print 'Inhibit longer than necessary'
            
            print round_number((np.max(times[4*(i)-4:4*(i)])+min_distance_to_inhibit))
            
            print 'inhibits[i]'
            
            print inhibits[i]
            
      
#            
            inhibits[i]=round_number((np.max(times[4*(i)-4:4*(i)])+min_distance_to_inhibit))
            
#            print inhibits[i]
#            
#            print round_number((np.max(times[4*(i)-4:4*(i)])+min_distance_to_inhibit))

#        
#        #Inserting empty switchings inhibits when inhibits are too long and making sure that these are not too short
            

#
        if ((inhibits[i] - inhibits[(i-1)]) > settings['max_inhibit_length']) and round_number(inhibits[i-1]+settings['max_inhibit_length']+min_distance_to_inhibit+settings['safety_distance'])<np.min(times[4*(i)-4:4*(i)]):
            #and ((times[4*(i)-1]- inhibits[(i-1)])-min_distance_to_inhibit)<0:
            
            
            print 'Inhibit too long'
            
            print 'inhibits[i]'
            
            print inhibits[i]
            
            print 'inhibits[(i-1)]'
            
            print inhibits[(i-1)]
            
       

            if not i-n-m >= nr:
                
#                print 'Inhibit too long'
#                
#                print round_number(inhibits[i-1]+settings['max_inhibit_length'])
                              
                inhibits = np.insert(inhibits, i, round_number(inhibits[i-1]+settings['max_inhibit_length']))
                

                
                #Increasing counter for empty switches inserted to inhibits

                n+=1
                
                # Inserting empty switch
                

                empty_time_2_abs=(times[4*(i-2)]+settings['max_inhibit_length']-min_distance_to_inhibit)
#        
#            
                empty_time_1_abs=(times[4*(i-2)]+settings['max_inhibit_length']-min_distance_to_inhibit-settings['single_switch_min_dist']-settings['window_width'])
#                

                
                time_rel=np.insert(time_rel, 4*(i-1), (settings['max_inhibit_length']-min_distance_to_inhibit-settings['single_switch_min_dist']-settings['window_width']))
                time_rel=np.insert(time_rel, 4*(i-1), (settings['max_inhibit_length']-min_distance_to_inhibit-settings['single_switch_min_dist']-settings['window_width']))
                time_rel=np.insert(time_rel, 4*(i-1), (settings['max_inhibit_length']-min_distance_to_inhibit))
                time_rel=np.insert(time_rel, 4*(i-1), (settings['max_inhibit_length']-min_distance_to_inhibit)) 
                
                times=np.insert(times, 4*(i-1), empty_time_1_abs)
                times=np.insert(times, 4*(i-1), empty_time_1_abs)
                times=np.insert(times, 4*(i-1), empty_time_2_abs)
                times=np.insert(times, 4*(i-1), empty_time_2_abs) 
    
                #Increasing counter for empty switches inserted to time_rel
    
                m+=1                
                
                #Inserting corresponding entry in types
                
                
                types=np.insert(types, 4*(i-1), 'e')
                types=np.insert(types, 4*(i-1), 'e')
                types=np.insert(types, 4*(i-1), 'e')
                types=np.insert(types, 4*(i-1), 'e')
                
                      
                
                time_rel[4*i]=time_rel[4*i]-settings['max_inhibit_length']
                time_rel[4*i+1]=time_rel[4*i+1]-settings['max_inhibit_length']
                time_rel[4*i+2]=time_rel[4*i+2]-settings['max_inhibit_length']
                time_rel[4*i+3]=time_rel[4*i+3]-settings['max_inhibit_length']
                
                
                if round_number(inhibits[i+1] - inhibits[i]) < settings['min_inhibit_length']:# and (i+n-1)>=0:
                

#                            
                    inhibits[i+1] = round_number(inhibits[i] + settings['min_inhibit_length'])
                            

        
        #Making sure relative times are not greater than corresponding inhibit        
                
#        if np.max(time_rel[4*(i)-4:4*(i)])> ((inhibits[i] - inhibits[(i-1)])-settings['min_distance_to_inhibit']):
        if np.max(times[4*(i)-4:4*(i)])> (inhibits[i]-min_distance_to_inhibit):
            
#            print 'neinnnnnnnnnnnnnnn'
#            
#           # print inhibits
#            
#            print np.max(times[4*(i)-4:4*(i)])
#            
#            print inhibits[i]-min_distance_to_inhibit
            
#            print 'Here 4'
            
            if not i-n-m >= nr:
                
       #Putting inhibit behind last switching + safety distance
                

                
                inhibits[i]=round_number(np.max(times[4*i-4:4*i])+min_distance_to_inhibit)
                

#            time_rel[4*(i+m)-1]=times[4*i-1]-inhibits[i+n-1]
#            time_rel[4*(i+m)-2]=times[4*i-2]-inhibits[i+n-1]
#            time_rel[4*(i+m)-3]=times[4*i-3]-inhibits[i+n-1]
#            time_rel[4*(i+m)-4]=times[4*i-4]-inhibits[i+n-1]
            time_rel[4*(i)-1]=times[4*i-1]-inhibits[i-1]
            time_rel[4*(i)-2]=times[4*i-2]-inhibits[i-1]
            time_rel[4*(i)-3]=times[4*i-3]-inhibits[i-1]
            time_rel[4*(i)-4]=times[4*i-4]-inhibits[i-1]
#            print 'time_rel[4*(i)-1]'
            
#            print time_rel[4*(i)-1]
#            
#            print 'time_rel very old'
#            
#            print time_rel
        
        else:
            
            time_rel[4*(i+0)-1]=times[4*i-1]-inhibits[i-1]
            time_rel[4*(i+0)-2]=times[4*i-2]-inhibits[i-1]
            time_rel[4*(i+0)-3]=times[4*i-3]-inhibits[i-1]
            time_rel[4*(i+0)-4]=times[4*i-4]-inhibits[i-1]
            
    
        i+=1   
            
            
#Calculating inhibit length
    
    inhibit_length=[]
    
    for i in np.arange(1,nr+n+1):                               
  
        inhibit_length.append(round_number(inhibits[i]-inhibits[i-1]))
        

    return time_rel, inhibits, inhibit_length, types, times, ini_inhibit_counter


def get_inhibitSums(inhibits):
    """
    function to generate the sums of the inhibits i0, i1, i2, i3 ->
    i0, i0+i1, i0+i1+i2, i0+i1+i2+i3, ...
    """
    inhibits_sum = np.asarray(inhibits)*0    # initialize empty array for the sums
    for ind, inh in enumerate(inhibits):
        inhibits_sum[ind:] += np.ones(len(inhibits)-(ind))*inh
    return inhibits_sum


def order_switch_times_inhibits(settings, switch_times, switch_types, inhibits):
    """
    Function to make sure only positive switchings happen and empty switchings
    are always positioned at the beginning of an inhibit window
    ordering of h's results from switching scheme:
    B_on, A_on, A_off, B_off, since starting with A gives us identity
    switchings instead of reflections'
    """
    result_array = []
    result_lines = []
    counter=0
    
 
    for x1, x2, x3, x4, t1, t2, t3, t4, i0 in izip(switch_times[::4],
                                               switch_times[1::4],
                                               switch_times[2::4],
                                               switch_times[3::4],
                                               switch_types[::4],
                                               switch_types[1::4],
                                               switch_types[2::4],
                                               switch_types[3::4],
                                               inhibits):

                                 
        counter += 1
        quartett = []
        if x1 > x3:
            first_switch_times = [x3, x4]
            first_switch_type = [t3, t4]
            second_switch_times = [x1, x2]
            second_switch_type = [t1, t2]
        else:
            first_switch_times = [x1, x2]
            first_switch_type = [t1, t2]
            second_switch_times = [x3, x4]
            second_switch_type = [t3, t4]
#        print 'first switch type'
#        print first_switch_type
#        print 'second switch type'
#        print second_switch_type
        if (not(first_switch_type[0] == first_switch_type[1]) or
                not(second_switch_type[0] == second_switch_type[1])):
            log.warn("Something very terrible happened!" +
                     " (Probably invalid char)")
            sys.exit(1)
        if (first_switch_type[0] == 'r' or first_switch_type[0] == 'e'):
            quartett.append(first_switch_times[1] + settings['first_delay'])
            quartett.append(first_switch_times[0] + settings['second_delay'])
#            print '1'
#            print quartett
            # 21
        elif first_switch_type[0] == 't':
            quartett.append(first_switch_times[0] + settings['first_delay'])
            quartett.append(first_switch_times[1] + settings['second_delay'])
#            print '2'
#            print quartett
            # 12
        if (second_switch_type[0] == 'r' or second_switch_type[0] == 'e'):
            quartett.append(second_switch_times[0] + settings['third_delay'])
            quartett.append(second_switch_times[1] + settings['fourth_delay'])
#            print '3'
#            print quartett
            # 34
        elif second_switch_type[0] == 't':
            quartett.append(second_switch_times[1] + settings['third_delay'])
            quartett.append(second_switch_times[0] + settings['fourth_delay'])
#            print '4'
#            print quartett
            # 43
        if tc.security_check(settings, quartett):
            quartett.append(i0)
            result_array.append(quartett)   # append quartett and inhibit
           # del quartett[:]
        else:
            print 'this file'
            print settings['myfile']
            print 'error here'
            print quartett
            print switch_times
            log.warn(pp.pformat("There was a switch error (to fast " +
                                "switching of a single switch)"))
#            print 'here'
#            print quartett
            sys.exit(1)
#    print 'result_array'
#    print result_array
    for item in result_array[::-1]:
#        print str(item)
        if settings['numberofEOMs']==1:
            result_lines.append('{0:.4f}'.format(item[0] * 1e6) + ' ' +
                                '{0:.4f}'.format(item[1] * 1e6) + ' ' +
                                '{0:.4f}'.format(item[2] * 1e6) + ' ' +
                                '{0:.4f}'.format(item[3] * 1e6) + ' ' +
                                '{0:.4f}'.format(item[4] * 1e6) + '\n')
                            
        elif settings['numberofEOMs']==2:
            result_lines.append('{0:.4f}'.format(item[0] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[1] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[2] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[3] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[4] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[0] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[1] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[2] * 1e6) + ' ' +                    
                    '{0:.4f}'.format(item[3] * 1e6) + '\n')
                    
        elif settings['numberofEOMs']==3:
            result_lines.append('{0:.4f}'.format(item[0] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[1] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[2] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[3] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[4] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[0] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[1] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[2] * 1e6) + ' ' +     
                    '{0:.4f}'.format(item[3] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[0] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[1] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[2] * 1e6) + ' ' +
                    '{0:.4f}'.format(item[3] * 1e6) + '\n')
#    print 'result_lines'
#    print result_lines
    return result_lines

  

def save_files(settings, times_relative, times, types, inhibits, inhibit_length, pulse_pos, ini_inhibit_counter):
    """
    Function to save fpm and human_readable file and create a plot
    """
    temp1 = settings['first_delay']
    temp2 = settings['second_delay']
    temp3 = settings['third_delay']
    temp4 = settings['fourth_delay']

    # relates timings to channels according to r or t operation and checks for too fast switchings
#    print 'original inhibits'
#    
#    print inhibits
    
#    if settings['rearrange_inhibits']==1:
#
#        times_relative, inhibits, inhibit_length, types, times, ini_inhibit_counter = get_relative_times_within_limits(times, inhibits, types, ini_inhibit_counter)
#        
#        times_relative, inhibits, inhibit_length, types, times, ini_inhibit_counter = get_relative_times_within_limits(times, inhibits, types, ini_inhibit_counter)
#        
# #       times_relative, inhibits, inhibit_length, types, times, ini_inhibit_counter = get_relative_times_within_limits(times, inhibits, types, ini_inhibit_counter)
#        
#    else:
#        
#        times_relative = get_relative_times(times, inhibits)
    
    #times_relative = get_relative_times(times, inhibits)
    
#    print inhibit_length
        

    switch_time_order_relative = order_switch_times_inhibits(settings, times_relative, types, inhibit_length)
    

    
    
    tc.generate_output_file(settings['outfile'], switch_time_order_relative)


    # for the human readable output we want the actual times, when a
    # switch has to occur. Therefore the delays are set to zero before
    # the human readable file is created (including 5th column with inhbit sum)
    settings['first_delay'] = 0
    settings['second_delay'] = 0
    settings['third_delay'] = 0
    settings['fourth_delay'] = 0
    inhibit_sums = get_inhibitSums(inhibit_length)
#    print 'inhibit_sums'
#    print inhibit_sums
#    print 'times'
#    print times
#    switch_time_order_nodelay = order_switch_times_inhibits(settings, times_relative, types, inhibit_length)
    switch_time_order_nodelay = order_switch_times_inhibits(settings, times, types, inhibit_sums)#tc.order_switch_times(settings, times, types)
#    print 'switch time'
#    print switch_time_order_nodelay
    tc.generate_output_file(settings['outfile2'], switch_time_order_nodelay)

    settings['first_delay'] = temp1
    settings['second_delay'] = temp2
    settings['third_delay'] = temp3
    settings['fourth_delay'] = temp4
#    generate_plot(settings, pulse_pos, times, types, inhibits)


def main():
    pulse_pos = tc.get_all_pulse_positions(settings)
    pulse_pos = np.concatenate(([0], pulse_pos))
    # shift pulse_pos by an offset -> no negative switching times occur
    offset = 0.5*settings['window_width']+221e-9
    pulse_pos+= offset
    
    
    


    if settings["allfiles"] == 0: # work with only one file (-> myfile)
        ini_inhibit_counter=0
        settings["outfile"] = settings["path"] + "\\" + settings["myfile"] + \
            "_out" + ".fpm"
        settings["outfile2"] = settings["path"] + "\\" + settings["myfile"] + \
            "_human_readable" + ".txt"
        settings["myfile"] = settings["path"] + "\\" + settings["myfile"] + ".txt"
        print "starting with file: %s" % os.path.split(settings["myfile"])[0]
        print "starting with file: %s" % os.path.split(settings["myfile"])[1]
        print "starting with file: %s" % os.path.split(settings["myfile"])[2]
        lines = tc.read_pulses(settings)
        switch_times, switch_types = compile_pulse_list(lines, settings)
        switch_times+= offset   #shift switch_times by the same offset
        all_switch_times, all_switch_types = add_empty_switchings(switch_times, switch_types, settings, pulse_pos)

        # group switchings together in bunches of 2 and create corresp. inhibits and include empties if needed
        inhibit_length, inhibits_starts = get_inhibits(all_switch_times, settings)
        
        
        if settings['rearrange_inhibits']==1:

            times_relative, inhibits_starts, inhibit_length, all_switch_types, all_switch_times, ini_inhibit_counter = get_relative_times_within_limits(all_switch_times, inhibits_starts, all_switch_types, ini_inhibit_counter)
        
            times_relative, inhibits_starts, inhibit_length, all_switch_types, all_switch_times, ini_inhibit_counter = get_relative_times_within_limits(all_switch_times, inhibits_starts, all_switch_types, ini_inhibit_counter)
        
 #       times_relative, inhibits, inhibit_length, types, times, ini_inhibit_counter = get_relative_times_within_limits(times, inhibits, types, ini_inhibit_counter)
    
        

        # save files
        save_files(settings, times_relative, all_switch_times, all_switch_types, inhibits_starts, inhibit_length, pulse_pos, ini_inhibit_counter)

    elif settings['sameParams'] == 1:   # load all txt-files in path if allfiles == 1 with the same params
    # and find one parameter set (file length and number of switchings) for all files
        
        max_nr = -1      # maximal number of inhibits (= lines in timing file)

        for pfilename in sorted(glob(os.path.join(settings["path"],'*_pulses.txt'))):   # run thorugh all files to find maximum number of switches
#                 extract base-filename from pfilename and update settings

            ini_inhibit_counter=0
            filenameend = os.path.split(pfilename)[1]
            filename = os.path.splitext(filenameend)[0]
            settings["myfile"] = pfilename

            lines = tc.read_pulses(settings)
            all_switch_times, all_switch_types = compile_pulse_list(lines, settings)
            all_switch_times+= offset   #shift switch_times by the same offset
#            print 'times 1'
            all_switch_times, all_switch_types = add_empty_switchings(all_switch_times, all_switch_types, settings, pulse_pos)
            inhibit_length, inhibits_starts = get_inhibits(all_switch_times, settings, last_inhibit_extra_length = 2e-6)
#            
#            print switch_times
            if settings['rearrange_inhibits']==1:

                times_relative, inhibits_starts, inhibit_length, all_switch_types, all_switch_times, ini_inhibit_counter = get_relative_times_within_limits(all_switch_times, inhibits_starts, all_switch_types, ini_inhibit_counter)
        
                times_relative, inhibits_starts, inhibit_length, all_switch_types, all_switch_times, ini_inhibit_counter = get_relative_times_within_limits(all_switch_times, inhibits_starts, all_switch_types, ini_inhibit_counter)
                

   
            max_nr = np.max([len(all_switch_times), max_nr])
            


        max_time = -1    # maximal file length
        for pfilename in sorted(glob(os.path.join(settings["path"],'*_pulses.txt'))):   # run thorugh all files to find maximum time
#                 extract base-filename from pfilename and update settings
            ini_inhibit_counter=0
            filenameend = os.path.split(pfilename)[1]
            filename = os.path.splitext(filenameend)[0]
            settings["myfile"] = pfilename

            lines = tc.read_pulses(settings)
#            print 'this file'
#            
#            print pfilename
            all_switch_times, all_switch_types = compile_pulse_list(lines, settings)
#            print 'this length'
##            
#            print all_switch_times[-1]
            
#            print all_switch_times
            
            all_switch_times+= offset #shift switch_times by the same offset
            all_switch_times, all_switch_types = add_empty_switchings(all_switch_times, all_switch_types, settings, pulse_pos)
#            print 'this length'
##            
#            print all_switch_times[-1]
            
#            print all_switch_times
            
            inhibit_length, inhibits_starts = get_inhibits(all_switch_times, settings, last_inhibit_extra_length = 0)
            if settings['rearrange_inhibits']==1:

                times_relative, inhibits_starts, inhibit_length, all_switch_types, all_switch_times, ini_inhibit_counter = get_relative_times_within_limits(all_switch_times, inhibits_starts, all_switch_types, ini_inhibit_counter)
        
                times_relative, inhibits_starts, inhibit_length, all_switch_types, all_switch_times, ini_inhibit_counter = get_relative_times_within_limits(all_switch_times, inhibits_starts, all_switch_types, ini_inhibit_counter)
            
            inhibit_length,inhibits_starts= ensure_last_inhibit(settings, inhibits_starts)
#
#            print 'this length'
##            
#            print all_switch_times[-1]
            

            max_time = np.max([inhibits_starts[-1], max_time])  #remember the max
            
#            print 'max time'
#            
#            print max_time
            


        for pfilename in sorted(glob(os.path.join(settings["path"],'*_pulses.txt'))):   # run thorugh all files with correct max_nr and max_time
            ini_inhibit_counter=0
            print "starting with file: %s" % os.path.split(pfilename)[1]
#                 extract base-filename from pfilename and update settings
            filenameend = os.path.split(pfilename)[1]
            filename = os.path.splitext(filenameend)[0]
            settings["myfile"] = pfilename
            settings["outfile"] = settings["path"] + "\\" + filename + \
                "_out" + ".fpm"
            settings["outfile2"] = settings["path"] + "\\" + filename + \
                "_human_readable" + ".txt"

            lines = tc.read_pulses(settings)
            all_switch_times, all_switch_types = compile_pulse_list(lines, settings)
            all_switch_times+= offset   #shift switch_times by the same offset
            all_switch_times, all_switch_types = add_empty_switchings(all_switch_times, all_switch_types, settings, pulse_pos)
            inhibit_length, inhibits_starts = get_inhibits(all_switch_times, settings, last_inhibit_extra_length = 0)
            
#            print 'inhibits 1'
#            
#            print inhibits_starts
            


            if settings['rearrange_inhibits']==1:

                times_relative, inhibits_starts, inhibit_length, all_switch_types, all_switch_times, ini_inhibit_counter = get_relative_times_within_limits(all_switch_times, inhibits_starts, all_switch_types, ini_inhibit_counter)
        
                times_relative, inhibits_starts, inhibit_length, all_switch_types, all_switch_times, ini_inhibit_counter = get_relative_times_within_limits(all_switch_times, inhibits_starts, all_switch_types, ini_inhibit_counter)
#            
#            print 'inhibits 2'
#            
#            print inhibits_starts
#            
            all_switch_times, all_switch_types, inhibits_starts= add_spaced_empty_switchings(all_switch_times, inhibits_starts, all_switch_types, settings, pulse_pos, max_time, max_nr)

#            print 'inhibits 3'
#            
#            print inhibits_starts
#            print 'sgkafjlsaög'
#                
#            print np.alen(inhibits_starts)

            inhibit_length,inhibits_starts= ensure_last_inhibit(settings, inhibits_starts)
            
#            print 'all_switch_times'
#            
#            print all_switch_times
#            
#            print 'inhibits_starts'
#            
#            print inhibits_starts
            
                        
            times_relative=get_relative_times(all_switch_times, inhibits_starts)
            
#            print 'times_relative'
#            
#            print times_relative
            
#            print inhibit_length
#            
#            print all_switch_times
            
#            print 'length times'
#            
#            print np.alen(inhibits_starts)
#            
#            print 'length times relative'
#            
#            print np.alen(inhibits_starts)
            


#            print inhibits_starts[-1]  # should now be the same for all files
#            print len(all_switch_times)
#           #save files finally
            save_files(settings, times_relative, all_switch_times, all_switch_types, inhibits_starts, inhibit_length, pulse_pos, ini_inhibit_counter)
    else:    #all files but not same params
        for pfilename in glob(os.path.join(settings["path"],'*_pulses.txt')):
            ini_inhibit_counter=0
            print "starting with file: %s" % os.path.split(pfilename)[1]
#                 extract base-filename from pfilename and update settings
            filenameend = os.path.split(pfilename)[1]
            filename = os.path.splitext(filenameend)[0]
            settings["myfile"] = pfilename
            settings["outfile"] = settings["path"] + "\\" + filename + \
                "_out" + ".fpm"
            settings["outfile2"] = settings["path"] + "\\" + filename + \
                "_human_readable" + ".txt"

            lines = tc.read_pulses(settings)
            switch_times, switch_types = compile_pulse_list(lines, settings)
            switch_times+= offset   #shift switch_times by the same offset
            all_switch_times, all_switch_types = add_empty_switchings(switch_times, switch_types, settings, pulse_pos)

            # group switchings together in bunches of 2 and create corresp. inhibits and include empties if needed
            inhibit_length, inhibits_starts = get_inhibits(all_switch_times, settings)
            
            if settings['rearrange_inhibits']==1:

                times_relative, inhibits_starts, inhibit_length, all_switch_types, all_switch_times, ini_inhibit_counter = get_relative_times_within_limits(all_switch_times, inhibits_starts, all_switch_types, ini_inhibit_counter)
        
                times_relative, inhibits_starts, inhibit_length, all_switch_types, all_switch_times, ini_inhibit_counter = get_relative_times_within_limits(all_switch_times, inhibits_starts, all_switch_types, ini_inhibit_counter)

            # save files
            save_files(settings, times_relative, all_switch_times, all_switch_types, inhibits_starts, inhibit_length, pulse_pos, ini_inhibit_counter)



    logging.shutdown()

if __name__ == '__main__':
    main()

