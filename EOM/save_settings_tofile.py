# -*- coding: utf-8 -*-
"""
Created on Wed Aug 07 15:33:09 2013

@author: Katzschmann
"""

import os
import sys
import ast
sys.path.append("..\Theory")

#import matplotlib.pyplot as plt
#from glob import glob
#from mpl_toolkits.mplot3d import axes3d
#from matplotlib.colors import Normalize
#import numpy as np
#from matplotlib import cm
#
#import theory_1D_dtqw as theo
import pprint
#import peakdetect as pd

import logging
import logging.config
import logging.handlers

logging.config.fileConfig('loggingConfig.txt')
log = logging.getLogger("myLogger.SafeSettingsToFile")

class FuncFilter(logging.Filter):
    def __init__(self, name=None):
        pass

    def filter(self, rec):
        if rec.funcName in []:
            return False
        else:
            return True

funcfilter = FuncFilter()
log.addFilter(funcfilter)

############################################################
# Variable declaration part
############################################################

lines = []
setting_label = '# Settings for DataAnalysisScript: '
save_settings = False
ignore_file_settings = False
settings = dict(
    tdc='',  # 'qutau',      # 'qutau' for the qutau, just something
             # else for the old one
    path="D:\Elster\Data\DiscreteTimeQuantumWalk\\130805",
    praefix="test.txt",
    Ha_offset=5,
    Va_offset=0,
    Hb_offset=230,
    Vb_offset=0,
    peak_width=18,

    max_steps=13,
    tau_offset=-1160,
    tau1=4150,
    tau2=287.5,
    tau3=23.5,
    tau_offset2=-1160,
    timeconst=160e-12,

    detection_eff_Ha=1,
    detection_eff_Va=1,
    detection_eff_Hb=1,
    detection_eff_Vb=1,

    steps_to_plot=5,  # i.e. Step number to start plotting from to
                      # max_steps
    display_grid=1,
    walk_2d=0,

    pol='V',
    alpha=20.7,
    loss_factor=0.92,

    alpha_blue_grid=0.3,
)
if settings['walk_2d'] > 0:
    settings['alpha_green_grid'] = 0.3
else:
    settings['alpha_green_grid'] = 0.0
settings['myfile'] = os.path.join(settings['path'], settings['praefix'])


def check_settings(settings, setting_label):
    with open(settings['myfile'], 'r') as myfile:
        for line in myfile.readlines():
            """find the settings in the file"""
            index = line.find(setting_label)
            if index > -1:
                log.debug("Settings found in File!")
                return True
            else:
                pass
    log.debug("no settings found!")
    return False


def get_settings(settings, setting_label):
    if check_settings(settings, setting_label):
        with open(settings['myfile'], 'r') as myfile:
            for line in myfile.readlines():
                """find the settings in the file"""
                index = line.find(setting_label)
                if index > -1:
                    log.debug("\nSettings read from file!")
                    s = line[index+len(setting_label):]
                    settings2 = ast.literal_eval(s)
                    log.debug(pprint.pformat(settings2))
                    return settings2
    log.debug("\nSettings not changed!\nKeeping settings to script defaults!")
    return settings


def append_settings(settings, setting_label):
    if check_settings(settings, setting_label) is False:
        with open(settings['myfile'], 'a') as myfile:
            myfile.write('\n'+setting_label+str(settings))
            log.debug("Settings appended to File!")
    else:
        log.warn("There are already settings in the File! Can not append " +
                 "settings!")


def replace_settings(settings, setting_label):
    if check_settings(settings, setting_label) is True:
        try:
            source = open(settings['myfile'], 'r')
            target = open(os.path.dirname(settings['myfile'])+"/temp.txt", 'w')
            log.debug("Temporary file created!")
            for line in source.readlines():
                """copy all comment lines as they are and also catch comments
                which have leading whitespace"""
                index = line.find(setting_label)
                if index > -1:
                    target.write(setting_label+str(settings))
                else:
                    target.write(line)
                    pass
            source.close()
            target.close()
        except IOError:
            log.critical("could not open file %s or %s", settings['myfile'],
                         os.path.dirname(settings['myfile'])+"/temp.txt")
            sys.exit("could not open file")
        log.debug("Temporary file complete!")
        os.remove(settings['myfile'])
        log.debug("Original file removed!")
        os.rename(os.path.dirname(settings['myfile'])+"/temp.txt",
                  settings['myfile'])
        log.debug("Temporary file renamed!")
    else:
        log.debug("There are no settings to replace!")


def handle_settings(settings, ignore_file_settings, save_settings,
                    setting_label):
    try:
        if ignore_file_settings is False:
            settings2 = get_settings(settings, setting_label)
            for key in settings2.keys():
                if (key == 'path' or key == 'myfile' or key == 'max_steps' or
                        key == 'steps_to_plot' or key == 'savedata2file' or
                        key == 'savetheo2file' or key == 'verbose' or
                        key == 'plot' or key == 'outfile' or key == 'outfile2'
                        or key == 'measured_file' or
                        key == 'reflect_list_name'):
#                    print "Ignored Path and Myfile"
                    continue
                settings[key] = settings2[key]
        else:
            log.debug("File settings ignored.")
        if save_settings:
            if check_settings(settings, setting_label) is False:
                log.debug("Appending settings to file!")
                append_settings(settings, setting_label)
            else:
                log.debug("Replacing settings in file!")
                replace_settings(settings, setting_label)
#        print settings
        return settings
    except IOError:
        log.warn("could not open file: %s", settings['myfile'])
        sys.exit("could not open file: "+settings['myfile'])


def main(argv, settings):
    try:
        if ignore_file_settings is False:
            settings = get_settings(settings, setting_label)
        else:
            log.debug("File settings ignored.")
        if save_settings:
            if check_settings(settings, setting_label) is False:
                log.debug("Appending settings to file!")
                append_settings(settings, setting_label)
            else:
                log.debug("Replacing settings in file!")
                replace_settings(settings, setting_label)
#        print settings

    except IOError:
        log.warn("could not open file: %s", settings['myfile'])
        sys.exit("could not open file: "+settings['myfile'])
    logging.shutdown()

if __name__ == "__main__":
    main(sys.argv[1:], settings)
