  # -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os
import sys
#sys.path.append("..\DataAnalysis")
#sys.path.append("..\TopologicalPhases")
from glob import glob
import numpy as np
#import scitools.pprint2 as pp
#import timing_calculation_new_experiment as tc
##import pprint as pp
##from collections import deque
#from itertools import izip
#import matplotlib.pyplot as plt


settings = dict(
            path = "Z:\Sampling",
            name="test",
            out_positions = "all",     # Positions within step in which walker is coupled out, type "all" for all positons
            out_steps = "all",         # Steps in which walker is coupled out, type "all" for all steps
            in_positions = [-1, 1],  #Positions which are switched into the walk
            in_step = 1,                #Step in which walker is coupled into the setup
            sink_positions= [0],       #Positions at which sink is placed, type "all" for all positons
            sink_steps = "all",     # Steps in which sink is place, type "all" for all steps
            step_number = 50,       #Number of steps taken into account
            )

def save_file(pattern, filename):
    """
    Function to create each file and save it
    """
#    string_rt = construct_string_rt(settings, pattern)
#    print string_rt
    
    

    try:
        with open(filename, 'w') as f:
            for lines in pattern:
                f.write(lines)
       
       #f = open(filename, 'w')
        
        #f.write(pattern)
        print 'yap'
        f.close
    except:
        print "Could not open file "+filename+" for writing."
        sys.exit(1)
        
        
def sample_patterns(settings):
    
    
    # + 'outcoupling in step'
    
    
    
    
    for out_steps in np.arange(2, settings['step_number']+1):
        
        
        
        pattern=construct_pattern(settings, out_steps)
        
        
        filename=os.path.join(settings['path'],'outcoupling in step' + str(out_steps) + '_pulses.txt')
        
        
        
#        np.savetxt(filename,pattern)
        
        save_file(pattern, filename)
        
    
        
def construct_pattern(settings, out_steps):
    
    pattern=[]
    
    for step in np.arange(0, settings['step_number']+1):
        

        
        if (step==out_steps):
            #or (step==settings['in_step']):
            
          
            
            
            for positions in np.arange(-step+(np.min(settings['in_positions'])+1), step+(np.max(settings['in_positions'])-1)+1, 2):
                
                
                if 'all' in settings['out_positions']:
                    
                    pattern.append(str(positions) + ' ' + 't'+ '     ')
                
                
                
                elif positions in settings['out_positions']:
                    
                     pattern.append(str(positions) + ' ' + 't'+ '     ')
                     
                    
                if positions==(step+(np.max(settings['in_positions'])-1)):
                    
                    pattern.append('\n')
                    
#            print 'step 1'
#                    
#            print step
                    
                    
        elif step==settings['in_step']:
            
            g=0
            
            for positions in settings['in_positions']:
                
                pattern.append(str(positions) + ' ' + 't'+ '     ')
                
                g+=1
                
                if g==np.size(settings['in_positions']):
                    
                    pattern.append('\n')
                    

                    
                    
            
                    
        
        elif (settings['sink_steps']=="all") and (step<=out_steps) and step>0:
            
 
                
                h=0
                
#                print 'positions'
#                
#                print np.arange(-step, step+1, 2)
                
                for positions in np.arange(-step, step+1, 2):
                    
#                    if (settings['sink_positions'])==str:
                
                    if 'all' in settings['sink_positions']:
                    
                            pattern.append(str(positions) + ' ' + 't'+ '     ')
                            
                            h+=1
                
                                
                    elif positions in settings['sink_positions']:
                    
                         pattern.append(str(positions) + ' ' + 't'+ '     ')
                         
                         h+=1
                         
#                    else:
#                        
#                        print 'I am dicky mdickface'
#                        
#                        pattern.append('s' + '\n')
                        
                        
                         
                        
                     
                    
                    if (positions==step):
                        
                        if h==0:
                            
                            
                            
                            pattern.append('s')
                            
                    
                        pattern.append('\n')
                        
                    
        elif type(settings['sink_steps'])==list:
                    

                    
                        if step in settings['sink_steps']:
                                
        
                        
                            h=0
                            
                            if type(step)==str:
                                step=0
                        
                
                        
                            for positions in np.arange(-step, step+1, 2):
                            
                            
                                if 'all' in settings['sink_positions']:
                                
                                    pattern.append(str(positions) + ' ' + 't'+ '     ')
                                    
                                    h+=1
                            
                            
                            
                                elif positions in settings['sink_positions']:
                                
                                     pattern.append(str(positions) + ' ' + 't'+ '     ')
                                     
                                     h+=1
                                 
                                
            
                                if positions==step:
                                    
                                    if h==0:
                                        
                                        
                                        
                                        pattern.append('s')
                                        
                                
                                    pattern.append('\n')
                
                        else:   
                            
                            pattern.append('s' + '\n')
                        
        else:    
                    

                
                    pattern.append('s' + '\n')
        
#                    result_lines.append('{0:.4f}'.format(item[0] * 1e6) + ' ' +
#                                '{0:.4f}'.format(item[1] * 1e6) + ' ' +
#                                '{0:.4f}'.format(item[2] * 1e6) + ' ' +
#                                '{0:.4f}'.format(item[3] * 1e6) + ' ' +
#                                '{0:.4f}'.format(item[4] * 1e6) + '\n')
        
#            print 'step 2'
#        
#            print step
        
        
    return pattern
        
        
        
#def construct_filename(settings):
#    
#    glob(os.path.join(settings["path"],'*_pulses.txt')
    
    
        
        
        
#########################################################################
# Main function
#########################################################################


def main(settings, argv):
    
    print type(settings['sink_steps'])
    
    if settings['out_steps']=="all":
        

        
        sample_patterns(settings)
        
    else:
        
        for out_steps in settings['out_steps']:
        
            pattern=construct_pattern(settings, out_steps)
            
            filename=os.path.join(settings['path'],'outcoupling in step' + str(out_steps) + '_pulses.txt')
        
#            print 'filename'
#            
#            print filename
            
            save_file(pattern, filename)

        
#    print 'stopped'


if __name__ == "__main__":
   main(settings, sys.argv[1:])