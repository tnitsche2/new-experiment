# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 15:11:51 2015

@author: Sonja
"""
import ctypes as C
import numpy as np
#import BME_DG_dll_script as BME_dll
import sys

class DelayList(object):
    """
    Class to store the DelayList Values in a "per channel" scheme, since the
    Delay Generator DLL expects it in this format. You will probably create
    four instances of this class in your main program, one for each channel
    used and fill them from the PercolationPulseFiles.
    """
    def __init__(self, Channel, delaytime=[], DG_Number=0):
        self.Channel = Channel
        self.DG_Number = DG_Number
        self.delaytime = np.asarray(delaytime)
        self.listlength = np.alen(self.delaytime)
        self.pulsewidth = np.ones(self.listlength)*0.05
        self.stepback = np.arange(1, self.listlength+1)

    def printargs(self):
        print "Channel: "+str(self.Channel)
        print "DG_Number: "+str(self.DG_Number)
        print "delaytimes: "+str(self.delaytime)
        print "pulsewidth: "+str(self.pulsewidth)
        print "stepback: "+str(self.stepback)
        print "listlength: "+str(self.listlength)+'\n'

    def printtimes(self):
        print "Channel: "+str(self.Channel)
        print "delaytimes: "+str(self.delaytime)+'\n'

    def set_DelayList(self, delaytime):
        self.delaytime = np.asarray(delaytime)*1.
        self.listlength = np.alen(self.delaytime)
        self.pulsewidth = np.ones(self.listlength)*0.05
        self.stepback = np.arange(1, self.listlength+1)


class InhibitList(object):
    """
    Class to store the InhibitList Values, since the
    Delay Generator DLL expects it in this format.
    See also class Delaylist
    but this has 2 less class fields since they are not needed for the call of set_InhibitList()
    included TriggerSignal field (see DG_DLL.h) in 151116
    """
    def __init__(self, delaytime=[], DG_Number=0, TriggerSignal = 0x400):
        self.DG_Number = DG_Number
        self.delaytime = np.asarray(delaytime)
        self.listlength = np.alen(self.delaytime)
        self.stepback = np.arange(1, self.listlength+1)
        self.TriggerSignal = TriggerSignal

    def printargs(self):
        print "Inhibit:"
        print "TriggerSignal: "+str(self.TriggerSignal)
        print "DG_Number: "+str(self.DG_Number)
        print "delaytimes: "+str(self.delaytime)
        print "stepback: "+str(self.stepback)
        print "listlength: "+str(self.listlength)+'\n'

    def set_List(self, delaytime):
        self.delaytime = np.asarray(delaytime)*1.
        self.listlength = np.alen(self.delaytime)
        self.stepback = np.arange(1, self.listlength+1)



def initialize_DG(DG_Number=0):
    """
    Function to initialize the DG Library and also set the expected data types
    for the called C-Functions
    new path for libraries/cal-file by Sonja Nov 2015
    """
    _mydll = C.cdll.LoadLibrary("C:\\Users\\IQO-User\\Documents\\BME_delayGenerator\\Update_151124_inhibitList4\\bmeW7\\PlxApi641.dll")
    _mydll = C.cdll.LoadLibrary("C:\\Users\\IQO-User\\Documents\\BME_delayGenerator\\Update_151124_inhibitList4\\bmeW7\\DelayGenerator.dll")

    _mydll.Test_BME_G08.restype = C.c_ulong
    _mydll.Test_BME_G08.argtypes = [C.c_ulong]

    _mydll.CalibratePathGroup.restype = C.c_ulong
    _mydll.CalibratePathGroup.argtypes = [C.POINTER(C.c_ulong), C.c_char_p]

    _mydll.CalibrateModule.restype = C.c_ulong
    _mydll.CalibrateModule.argtypes = [C.c_ulong]

    _mydll.LoadAllCardParameters.restype = C.c_ulong
    _mydll.LoadAllCardParameters.argtypes = [C.c_bool, C.c_bool, C.c_bool]

    _mydll.Set_CalibrationLevel.restype = C.c_ulong
    _mydll.Set_CalibrationLevel.argtypes = [C.c_ulong, C.c_ulong]

    _mydll.Set_G08_ClockParameters.restype = C.c_long
    _mydll.Set_G08_ClockParameters.argtypes = [C.c_bool, C.c_ulong, C.c_ulong,
                                            C.c_ulong, C.c_ulong, C.c_long]

    _mydll.Set_TriggerParameters.restype = C.c_long
    _mydll.Set_TriggerParameters.argtypes = [C.c_bool, C.c_double, C.c_double,
				C.c_ulong, C.c_ulong, C.c_bool, C.c_bool, C.c_bool,
                       C.c_bool, C.c_bool, C.c_bool, C.c_bool, C.c_bool, C.c_long]

    _mydll.Set_G08_TriggerParameters.restype = C.c_long
    _mydll.Set_G08_TriggerParameters.argtypes=[C.c_bool, C.c_double, C.c_double,
                                               C.c_bool, C.c_bool, C.c_double,
                                               C.c_double, C.c_ulong, C.c_ulong, C.c_long]

    _mydll.Set_GateFunction.restype = C.c_long
    _mydll.Set_GateFunction.argtypes=[C.c_ulong, C.c_long]

    _mydll.Set_G08_Delay.restype = C.c_ulong
    _mydll.Set_G08_Delay.argtypes = [C.c_ulong, C.c_double, C.c_double, C.c_ulong,
                                     C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                     C.c_bool, C.c_bool, C.c_bool, C.c_long]

    _mydll.Set_DelayList.restype = C.c_ulong
    _mydll.Set_DelayList.argtypes = [C.c_ulong, C.POINTER(C.c_double),
                                     C.POINTER(C.c_double),
                                     C.POINTER(C.c_ulong),
                                     C.c_long, C.c_long]

    _mydll.Set_InhibitList.restype = C.c_ulong
    _mydll.Set_InhibitList.argtypes = [C.POINTER(C.c_double),
                                     C.POINTER(C.c_ulong),
                                     C.c_long, C.c_ulong, C.c_long]

    _mydll.Set_BME_G08.restype = C.c_ulong
    _mydll.Set_BME_G08.argtypes = [C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_bool, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_bool, C.c_bool, C.c_bool,
                                   C.c_ulong, C.c_bool, C.c_ulong, C.c_ulong,
                                   C.c_ulong,
                                   C.c_ulong, C.c_bool, C.c_bool, C.c_double,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_double, C.c_ulong, C.c_ulong,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_bool, C.c_bool, C.c_bool, C.c_bool,
                                   C.c_bool, C.c_bool, C.c_bool, C.c_bool,
                                   C.c_ulong]
    result = _mydll.Reserve_DG_Data(1)
    if result != 0:
        sys.exit(result)
    result = _mydll.DetectPciDelayGenerators()
    result = _mydll.Initialize_DG_BME(0, C.c_ulong(46), 0)
    # C.c_ulong(46) means Type BME_SG08P3	/ Value taken from DG_Data.h
    if result != 0:
        sys.exit(result)
    result = Set_CalibrationLevel(_mydll)
    if result != 0:
        sys.exit(result)
    result = ReadCalibrationConstants(_mydll)
    if result != 0:
        sys.exit(result)
#    CalibrateModule(mydll)
    result = CalibratePathGroup(_mydll, np.asarray([1]),
                                r"C:\\Users\\IQO-User\\Documents\\BME_delayGenerator\\Update_151124_inhibitList4\\bmeW7\\")
    if result != 0:
        sys.exit(result)
    return _mydll


def Set_CalibrationLevel(_mydll, DG_Number=0):
    result = _mydll.Set_CalibrationLevel(1, DG_Number)
    print ("Output of CalibrationLevel: %s", str(result))
    return result


def ReadCalibrationConstants(_mydll):
    result = _mydll.ReadCalibrationConstants()
    print ("Output of ReadCalibrationConstants: %s", str(result))
    return result


def CalibratePathGroup(_mydll, p_module, Path):
    result = _mydll.CalibratePathGroup(p_module.ctypes.data_as(
        C.POINTER(C.c_ulong)), Path)
    print ("Output of CalibratePathGroup: %s", str(result))
    return result

def set_DG(_mydll,
           FireFirst_A=0.0, PulseWidth_A=0.5, OutputModulo_A=1,
           OutputOffset_A=0, GoSignal_A=0x810, Positive_A=True,
           Terminate_A=False,
           FireFirst_B=0.0, PulseWidth_B=0.5, OutputModulo_B=1,
           OutputOffset_B=0, GoSignal_B=0x810, Positive_B=True,
           Terminate_B=False,
           FireFirst_C=0.0, PulseWidth_C=0.5, OutputModulo_C=1,
           OutputOffset_C=0, GoSignal_C=0x810, Positive_C=True,
           Terminate_C=False,
           FireFirst_D=0.0, PulseWidth_D=0.5, OutputModulo_D=1,
           OutputOffset_D=0, GoSignal_D=0x20, Positive_D=True,
           Terminate_D=False,
           FireFirst_E=0.0, PulseWidth_E=0.5, OutputModulo_E=1,
           OutputOffset_E=0, GoSignal_E=0x810, Positive_E=True,
           Terminate_E=False,
           Disconnect_E=False, OntoMsBus_E=False, InputPositive_E=True,
           FireFirst_F=0.0, PulseWidth_F=0.0625, OutputModulo_F=5,
           OutputOffset_F=0, GoSignal_F=0x10, Positive_F=True,
           Terminate_F=False,
           Disconnect_F=False, OntoMsBus_F=True, InputPositive_F=True,
           GateFunction=0x0, ClockEnable=True, OscillatorDivider=2,
           TriggerDivider=1, TriggerMultiplier=1, ClockSource=1,
           TriggerTerminate=True, GateTerminate=True, TriggerLevel=0.0,
           GateLevel=1.0, GateDelay=-1.0, MS_Bus=0x1,
           InternalClock=2, PresetValue=0x0, GateDivider=1, ForceTrigger=-1.0,
           StepBackTime=-1.0, BurstCounter=1,
           PositiveGate=True, InternalTrigger=True, IgnoreGate=True,
           SynchronizeGate=False, RisingEdge=True, StopOnPreset=False,
           ResetWhenDone=True, TriggerEnable=True, DG_Number=0):
    result = _mydll.Set_BME_G08(FireFirst_A, PulseWidth_A, OutputModulo_A,
                                OutputOffset_A, GoSignal_A, Positive_A,
                                Terminate_A,

                                FireFirst_B, PulseWidth_B, OutputModulo_B,
                                OutputOffset_B, GoSignal_B, Positive_B,
                                Terminate_B,

                                FireFirst_C, PulseWidth_C, OutputModulo_C,
                                OutputOffset_C, GoSignal_C, Positive_C,
                                Terminate_C,

                                FireFirst_D, PulseWidth_D, OutputModulo_D,
                                OutputOffset_D, GoSignal_D, Positive_D,
                                Terminate_D,

                                FireFirst_E, PulseWidth_E, OutputModulo_E,
                                OutputOffset_E, GoSignal_E, Positive_E,
                                Terminate_E, Disconnect_E, OntoMsBus_E,
                                InputPositive_E,

                                FireFirst_F, PulseWidth_F, OutputModulo_F,
                                OutputOffset_F, GoSignal_F, Positive_F,
                                Terminate_F, Disconnect_F, OntoMsBus_F,
                                InputPositive_F,

                                GateFunction, ClockEnable, OscillatorDivider,
                                TriggerDivider, TriggerMultiplier, ClockSource,
                                TriggerTerminate, GateTerminate, TriggerLevel,
                                GateLevel, GateDelay, MS_Bus, InternalClock,
                                PresetValue, GateDivider, ForceTrigger,
                                StepBackTime, BurstCounter, PositiveGate,
                                InternalTrigger, IgnoreGate, SynchronizeGate,
                                RisingEdge, StopOnPreset, ResetWhenDone,
                                TriggerEnable, DG_Number)
    print ("Result of Set command: %s", str(result))
    return result

def initialize_DelayLists():
    """
    Function to creat four delay list objects, that are returned from this
    function.

    The hardcoded numbers are the channel selector values taken from DG_Data.h
    """
    ch1 = DelayList(2)  # corresponds to A channel
    ch2 = DelayList(3)  # corresponds to B channel
    ch3 = DelayList(4)  # corresponds to C channel
    ch4 = DelayList(5)  # corresponds to D channel
    return ch1, ch2, ch3, ch4


def initialize_InhibitList(TriggerSignal=0x400):
    """
    Function to creat one inhibitlist object, that are returned from this
    function.

    here no channel number expected
    """
    ch = InhibitList()
#    ch.printargs()
    return ch


def set_DelayList(_mydll, ch):
    """
    long CDG_DLL::Set_DelayList(unsigned long Channel, double* p_DelayTime, double* p_PulseWidth,
    			unsigned long* p_StepBack, signed long ListLength, long DG_Number)
    """
    result = _mydll.Set_DelayList(
        ch.Channel, ch.delaytime.ctypes.data_as(C.POINTER(C.c_double)),
        ch.pulsewidth.ctypes.data_as(C.POINTER(C.c_double)),
        ch.stepback.ctypes.data_as(C.POINTER(C.c_ulong)),
        ch.listlength, ch.DG_Number)
    print ("Result of Set_DelayList: %s", str(result))
    return result


def set_InhibitList(_mydll, ch):
    """
    new function similar to set_DelayList for the setting of the inhibits
    (new feature in 2015)
    TriggerSignal new in 151116
    long CDG_DLL::Set_InhibitList(double* p_InhibitTime, unsigned long* p_StepBack,
                                  signed long ListLength, unsigned long TriggerSignal, long DG_Number)
    """
    result = _mydll.Set_InhibitList(
        ch.delaytime.ctypes.data_as(C.POINTER(C.c_double)),
        ch.stepback.ctypes.data_as(C.POINTER(C.c_ulong)), ch.TriggerSignal,
        ch.listlength, ch.DG_Number)
    print ("Result of Set_InhibitList: %s", str(result))
    return result

def Set_G08_ClockParameters(_mydll, ClockEnable=True, OscillatorDivider=2,
                            TriggerDivider=1, TriggerMultiplier=1,
                            ClockSource=1, DG_number=0):
    """
    Call this routine to set the clock parameters of a delay generator
    long Set_G08_ClockParameters(BOOL ClockEnable, unsigned long OscillatorDivider,
				unsigned long TriggerDivider, unsigned long TriggerMultiplier,
				unsigned long ClockSource, long DG_Number);
    """
    result = _mydll.Set_G08_ClockParameters(ClockEnable, OscillatorDivider,
                                            TriggerDivider, TriggerMultiplier,
                                            ClockSource, DG_number)
    return result


def Set_TriggerParameters(_mydll, TriggerTerminate=True, InternalClock=1, TriggerLevel=0.5,
				PresetValue=0x0, GateDivider=1, PositiveGate=True,
                       InternalTrigger=True, InternalArm=False, SoftwareTrigger=False,
                       RisingEdge=True, StopOnPreset=False, ResetWhenDone=True,
                       TriggerEnable=True, DG_Number=0):
    """
    Call this routine to set the trigger parameters of a delay generator
    long Set_TriggerParameters(BOOL TriggerTerminate, double InternalClock, double TriggerLevel,
				unsigned long PresetValue, unsigned long GateDivider
				BOOL PositiveGate, BOOL InternalTrigger, BOOL InternalArm,
                       BOOL SoftwareTrigger, BOOL RisingEdge, BOOL StopOnPreset,
				BOOL ResetWhenDone, BOOL TriggerEnable, long DG_Number)
    """
    result = _mydll.Set_TriggerParameters(TriggerTerminate, InternalClock,
                                          TriggerLevel, PresetValue, GateDivider,
                                          PositiveGate, InternalTrigger,
                                          InternalArm, SoftwareTrigger, RisingEdge,
                                          StopOnPreset, ResetWhenDone,
                                          TriggerEnable, DG_Number)
    return result


def Set_G08_TriggerParameters(_mydll, GateTerminate=True, GateLevel=1.0, GateDelay=-1,
                                              IgnoreGate=True, SynchronizeGate=False,
                                              ForceTrigger=-1, StepBackTime=-1,
                                              BurstCounter=1,
                                              MS_Bus=0x1, DG_Number=0):
    """
    Call this routine to set the additional trigger parameters of the BME_SG08p delay generator
    long Set_G08_TriggerParameters(BOOL GateTerminate, double GateLevel, double GateDelay
				BOOL IgnoreGate, BOOL SynchronizeGate, double ForceTrigger
				double StepBackTime, unsigned long BurstCounter,
				unsigned long MS_Bus, long DG_Number)
    """
    result = _mydll.Set_G08_TriggerParameters(GateTerminate, GateLevel, GateDelay,
                                              IgnoreGate, SynchronizeGate,
                                              ForceTrigger, StepBackTime, BurstCounter,
                                              MS_Bus, DG_Number)
    return result


def Set_GateFunction(_mydll, GateFunction=0, DG_Number=0):
    """
    long Set_GateFunction(unsigned long GateFunction, long DG_Number);
    """
    result = _mydll.Set_GateFunction(GateFunction, DG_Number)
    return result


def Set_G08_Delay(_mydll, Channel=1, FireFirst=0, PulseWidth=0.05, OutputModulo=1,
                                  OutputOffset=0, GoSignal=0x810, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0):
    """
    Call this routine to set the delay parameters of a BME_G08p module
    long Set_G08_Delay(unsigned long Channel, double FireFirst, double PulseWidth,
			unsigned long OutputModulo, unsigned long OutputOffset,
			unsigned long GoSignal, BOOL Positive, BOOL Terminate,
			BOOL Disconnect, BOOL OntoMsBus, BOOL InputPositive, long DG_Number)
    """
    result = _mydll.Set_G08_Delay(Channel, FireFirst, PulseWidth, OutputModulo,
                                  OutputOffset, GoSignal, Positive, Terminate,
                                  	Disconnect, OntoMsBus, InputPositive, DG_Number)
    return result


def load_params(_mydll):
    result = _mydll.LoadAllCardParameters(True, True, True)
    print ("Result of param load: %s", str(result))
    return result


def deactivate_DG(_mydll, DG_Number=0):
    result = _mydll.Deactivate_DG_BME(0)
    print ("Result of deactivate: %s", str(result))
    return result


def activate_DG(_mydll, DG_Number=0):
    print ("Result of activate: %s", str(_mydll.Activate_DG_BME(0)))


def shutdown_DG(_mydll):
    print _mydll.Release_DG_Data()

def main(argv):
    mydll = initialize_DG()
    res = Set_G08_ClockParameters(mydll, ClockEnable=True, OscillatorDivider=2,
                            TriggerDivider=1, TriggerMultiplier=1,
                            ClockSource=2, DG_number=0)
    res += Set_TriggerParameters(mydll,TriggerTerminate=True, InternalClock=2, TriggerLevel=0.5,
				PresetValue=0x0, GateDivider=1, PositiveGate=True,
                       InternalTrigger=True, InternalArm=False, SoftwareTrigger=False,
                       RisingEdge=False, StopOnPreset=False, ResetWhenDone=True,
                       TriggerEnable=False, DG_Number=0)
    # handle inhibits
    inhibit  = initialize_InhibitList(TriggerSignal=0x400)
    inhibit.set_List([2.0,.2,.4,.6,.8,1.])
    res += set_InhibitList(mydll, inhibit)

    res += Set_G08_TriggerParameters(mydll, GateTerminate=True, GateLevel=1.0, GateDelay=-1,
                                              IgnoreGate=True, SynchronizeGate=False,
                                              ForceTrigger=2000, StepBackTime=-1,
                                              BurstCounter=8,
                                              MS_Bus=0xf0, DG_Number=0)
    res += Set_GateFunction(mydll, GateFunction=0, DG_Number=0)

    # handle delay channels A-D
    res += Set_G08_Delay(mydll,Channel=1, FireFirst=0, PulseWidth=0.05, OutputModulo=1,
                                  OutputOffset=0, GoSignal=0x650, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0)
    res += Set_G08_Delay(mydll,Channel=2, FireFirst=0, PulseWidth=0.05, OutputModulo=1,
                                  OutputOffset=0, GoSignal=0x650, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0)
    res += Set_G08_Delay(mydll,Channel=3, FireFirst=0, PulseWidth=0.05, OutputModulo=1,
                                  OutputOffset=0, GoSignal=0x650, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0)
    res += Set_G08_Delay(mydll,Channel=4, FireFirst=0, PulseWidth=0.05, OutputModulo=1,
                                  OutputOffset=0, GoSignal=0x650, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0)
    ch1, ch2, ch3, ch4 = initialize_DelayLists()
    ch1.set_DelayList([0,0,0,0,0,0])
    ch2.set_DelayList([0,0,0,0,0,0])
    ch3.set_DelayList([0,0,0,0,0,0])
    ch4.set_DelayList([0,0,0,0,0,0])
    res += set_DelayList(mydll, ch1)
    res += set_DelayList(mydll, ch2)
    res += set_DelayList(mydll, ch3)
    res += set_DelayList(mydll, ch4)

    # handle E and F channels
    res += Set_G08_Delay(mydll,Channel=5, FireFirst=0, PulseWidth=0.05, OutputModulo=8,
                                  OutputOffset=0, GoSignal=0x50, Positive=True,
                                  Terminate=True,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0)
    res += Set_G08_Delay(mydll,Channel=6, FireFirst=0, PulseWidth=0.05, OutputModulo=1,
                                  OutputOffset=0, GoSignal=0x10, Positive=True,
                                  Terminate=False,	Disconnect=False, OntoMsBus=False,
                                  InputPositive=True, DG_Number=0)
    res += load_params(mydll)
    print res
    activate_DG(mydll, DG_Number=0)

    raw_input('done yet?')
    deactivate_DG(mydll)
    shutdown_DG(mydll)


if __name__ == "__main__":
    mydll = main(sys.argv[1:])
