# -*- coding: utf-8 -*-
"""
Created on Wed Jul 02 13:14:23 2014

@author: fkatzsch
"""

import ctypes as C
import numpy as np
import sys
from time import sleep

import logging
import logging.config

logging.config.fileConfig('..\\..\\loggingConfig.txt')
log = logging.getLogger("myLogger.BME_DG_dll_script")


class FuncFilter(logging.Filter):
    def __init__(self, name=None):
        pass

    def filter(self, rec):
        if rec.funcName in ['sum_over_grid', 'generate_errorbars']:
            return False
        else:
            return True

funcfilter = FuncFilter()
log.addFilter(funcfilter)


class DelayList(object):
    """
    Class to store the DelayList Values in a "per channel" scheme, since the
    Delay Generator DLL expects it in this format. You will probably create
    four instances of this class in your main program, one for each channel
    used and fill them from the PercolationPulseFiles.
    """
    def __init__(self, Channel, delaytime=[], DG_Number=0):
        self.Channel = Channel
        self.DG_Number = DG_Number
        self.delaytime = np.asarray(delaytime)
        self.listlength = np.alen(self.delaytime)
        self.pulsewidth = np.ones(self.listlength)*0.05
        self.stepback = np.arange(1, self.listlength+1)

    def printargs(self):
        print "Channel: "+str(self.Channel)
        print "DG_Number: "+str(self.DG_Number)
        print "delaytimes: "+str(self.delaytime)
        print "pulsewidth: "+str(self.pulsewidth)
        print "stepback: "+str(self.stepback)
        print "listlength: "+str(self.listlength)+'\n'

    def printtimes(self):
        print "Channel: "+str(self.Channel)
        print "delaytimes: "+str(self.delaytime)+'\n'

    def set_DelayList(self, delaytime):
        self.delaytime = np.asarray(delaytime)*1.
        self.listlength = np.alen(self.delaytime)
        self.pulsewidth = np.ones(self.listlength)*0.05
        self.stepback = np.arange(1, self.listlength+1)


def readin_pattern(filename, ch1, ch2, ch3, ch4):
    """
    Function to read a PulsePatternFile and load the values into DelayList
    Objects
    Returns four DelayList objects
    """
    log.debug("Reading in Pattern")
    data = np.genfromtxt(filename, comments='#', invalid_raise=True)
#    print data
    ch1.set_DelayList(data[:, 0])
    ch2.set_DelayList(data[:, 1])
    ch3.set_DelayList(data[:, 2])
    ch4.set_DelayList(data[:, 3])

    ch1.printargs()
    ch2.printargs()
    ch3.printargs()
    ch4.printargs()

    return ch1, ch2, ch3, ch4


def ReadCalibrationConstants(_mydll):
    result = _mydll.ReadCalibrationConstants()
    log.debug("Output of ReadCalibrationConstants: %s", str(result))
    return result


def Set_CalibrationLevel(_mydll, DG_Number=0):
    result = _mydll.Set_CalibrationLevel(1, DG_Number)
    log.debug("Output of CalibrationLevel: %s", str(result))
    return result


def CalibrateModule(_mydll, DG_Number=0):
    result = _mydll.CalibrateModule(DG_Number)
    log.debug("Output of CalibrateModule: %s", str(result))
    return result


def CalibratePathGroup(_mydll, p_module, Path):
    result = _mydll.CalibratePathGroup(p_module.ctypes.data_as(
        C.POINTER(C.c_ulong)), Path)
    log.debug("Output of CalibratePathGroup: %s", str(result))
    return result


def initialize_DelayLists():
    """
    Function to creat four delay list objects, that are returned from this
    function.

    The hardcoded numbers are the channel selector values taken from DG_Data.h
    """
    ch1 = DelayList(2)  # corresponds to A channel
    ch2 = DelayList(3)  # corresponds to B channel
    ch3 = DelayList(4)  # corresponds to C channel
    ch4 = DelayList(6)  # corresponds to E channel, D not used because it is
                        # noisy
#    ch1.printargs()
#    ch2.printargs()
#    ch3.printargs()
#    ch4.printargs()

    return ch1, ch2, ch3, ch4


def initialize_DG(DG_Number=0):
    """
    Function to initialize the DG Library and also set the expected data types
    for the called C-Functions
    """
    _mydll = C.cdll.LoadLibrary('C:\\Users\\IQO-User\\Documents\\BME_delayGenerator\\Update_151106_inhibitList\\bmeW7\\PlxApi641.dll')
    _mydll = C.cdll.LoadLibrary('C:\\Users\\IQO-User\\Documents\\BME_delayGenerator\\Update_151106_inhibitList\\bmeW7\\DelayGenerator.dll')

    _mydll.Test_BME_G08.restype = C.c_ulong
    _mydll.Test_BME_G08.argtypes = [C.c_ulong]

    _mydll.CalibratePathGroup.restype = C.c_ulong
    _mydll.CalibratePathGroup.argtypes = [C.POINTER(C.c_ulong), C.c_char_p]

    _mydll.CalibrateModule.restype = C.c_ulong
    _mydll.CalibrateModule.argtypes = [C.c_ulong]

    _mydll.LoadAllCardParameters.restype = C.c_ulong
    _mydll.LoadAllCardParameters.argtypes = [C.c_bool, C.c_bool, C.c_bool]

    _mydll.Set_CalibrationLevel.restype = C.c_ulong
    _mydll.Set_CalibrationLevel.argtypes = [C.c_ulong, C.c_ulong]

    _mydll.Set_DelayList.restype = C.c_ulong
    _mydll.Set_DelayList.argtypes = [C.c_ulong, C.POINTER(C.c_double),
                                     C.POINTER(C.c_double),
                                     C.POINTER(C.c_ulong),
                                     C.c_long, C.c_ulong]

    _mydll.Set_BME_G08.restype = C.c_ulong
    _mydll.Set_BME_G08.argtypes = [C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_bool, C.c_bool, C.c_bool,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_ulong, C.c_ulong, C.c_bool, C.c_bool,
                                   C.c_bool, C.c_bool, C.c_bool,
                                   C.c_ulong, C.c_bool, C.c_ulong, C.c_ulong,
                                   C.c_ulong,
                                   C.c_ulong, C.c_bool, C.c_bool, C.c_double,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_double, C.c_ulong, C.c_ulong,
                                   C.c_double, C.c_double, C.c_ulong,
                                   C.c_bool, C.c_bool, C.c_bool, C.c_bool,
                                   C.c_bool, C.c_bool, C.c_bool, C.c_bool,
                                   C.c_ulong]
    result = _mydll.Reserve_DG_Data(1)
    log.debug("Result of memory allocation: %s", str(result))
    if result != 0:
        log.critical("Initialization of DG failed... aborting Program!")
        sys.exit(result)
    result = _mydll.DetectPciDelayGenerators()
    log.debug('No. of PCI Generators detected: %s', str(result))
    result = _mydll.Initialize_DG_BME(0, C.c_ulong(46), 0)
    # C.c_ulong(45) means Type BME_SG08P2	/ Value taken from DG_Data.h
    log.debug("Result of initialization routine: %s", str(result))
    if result != 0:
        log.critical("Initialization of DG failed... aborting Program!")
        sys.exit(result)
    result = Set_CalibrationLevel(_mydll)
    if result != 0:
        log.critical("Initialization of DG failed... aborting Program!")
        sys.exit(result)
    result = ReadCalibrationConstants(_mydll)
    if result != 0:
        log.critical("Initialization of DG failed... aborting Program!")
        sys.exit(result)
#    CalibrateModule(mydll)
    result = CalibratePathGroup(_mydll, np.asarray([1]),
                                "C:\\Users\\IQO-User\\Documents\\BME_delayGenerator\\Update_151106_inhibitList\\bmeW7\\")
    if result != 0:
        log.critical("Initialization of DG failed... aborting Program!")
        sys.exit(result)
    return _mydll


def set_DelayList(_mydll, ch):
    result = _mydll.Set_DelayList(
        ch.Channel, ch.delaytime.ctypes.data_as(C.POINTER(C.c_double)),
        ch.pulsewidth.ctypes.data_as(C.POINTER(C.c_double)),
        ch.stepback.ctypes.data_as(C.POINTER(C.c_ulong)),
        ch.listlength, ch.DG_Number)
    log.debug("Result of Set_DelayList: %s", str(result))
    return result


def test_DG(_mydll, DG_Number=0):
    print "Result of selftest: "+str(_mydll.Test_BME_G08(DG_Number))


def activate_DG(_mydll, DG_Number=0):
    log.debug("Result of activate: %s", str(_mydll.Activate_DG_BME(0)))


def deactivate_DG(_mydll, DG_Number=0):
    result = _mydll.Deactivate_DG_BME(0)
    log.debug("Result of deactivate: %s", str(result))
    return result


def load_params(_mydll):
    result = _mydll.LoadAllCardParameters(True, True, True)
    log.debug("Result of param load: %s", str(result))
    return result


def set_DG(_mydll,
           FireFirst_A=0.0, PulseWidth_A=0.05, OutputModulo_A=1,
           OutputOffset_A=0, GoSignal_A=0x810, Positive_A=True,
           Terminate_A=False,
           FireFirst_B=0.0, PulseWidth_B=0.05, OutputModulo_B=1,
           OutputOffset_B=0, GoSignal_B=0x810, Positive_B=True,
           Terminate_B=False,
           FireFirst_C=0.0, PulseWidth_C=0.05, OutputModulo_C=1,
           OutputOffset_C=0, GoSignal_C=0x810, Positive_C=True,
           Terminate_C=False,
           FireFirst_D=0.0, PulseWidth_D=0.05, OutputModulo_D=1,
           OutputOffset_D=0, GoSignal_D=0x810, Positive_D=True,
           Terminate_D=False,
           FireFirst_E=0.0, PulseWidth_E=0.05, OutputModulo_E=1,
           OutputOffset_E=0, GoSignal_E=0x810, Positive_E=True,
           Terminate_E=False,
           Disconnect_E=False, OntoMsBus_E=False, InputPositive_E=True,
           FireFirst_F=0.0, PulseWidth_F=0.0625, OutputModulo_F=5,
           OutputOffset_F=0, GoSignal_F=0x10, Positive_F=True,
           Terminate_F=False,
           Disconnect_F=False, OntoMsBus_F=True, InputPositive_F=True,
           GateFunction=0x0, ClockEnable=True, OscillatorDivider=2,
           TriggerDivider=1, TriggerMultiplier=1, ClockSource=1,
           TriggerTerminate=True, GateTerminate=True, TriggerLevel=0.0,
           GateLevel=1.0, GateDelay=-1.0, MS_Bus=0x1,
           InternalClock=2, PresetValue=0x0, GateDivider=1, ForceTrigger=-1.0,
           StepBackTime=-1.0, BurstCounter=1,
           PositiveGate=True, InternalTrigger=True, IgnoreGate=True,
           SynchronizeGate=False, RisingEdge=True, StopOnPreset=False,
           ResetWhenDone=True, TriggerEnable=True, DG_Number=0):
    result = _mydll.Set_BME_G08(FireFirst_A, PulseWidth_A, OutputModulo_A,
                                OutputOffset_A, GoSignal_A, Positive_A,
                                Terminate_A,

                                FireFirst_B, PulseWidth_B, OutputModulo_B,
                                OutputOffset_B, GoSignal_B, Positive_B,
                                Terminate_B,

                                FireFirst_C, PulseWidth_C, OutputModulo_C,
                                OutputOffset_C, GoSignal_C, Positive_C,
                                Terminate_C,

                                FireFirst_D, PulseWidth_D, OutputModulo_D,
                                OutputOffset_D, GoSignal_D, Positive_D,
                                Terminate_D,

                                FireFirst_E, PulseWidth_E, OutputModulo_E,
                                OutputOffset_E, GoSignal_E, Positive_E,
                                Terminate_E, Disconnect_E, OntoMsBus_E,
                                InputPositive_E,

                                FireFirst_F, PulseWidth_F, OutputModulo_F,
                                OutputOffset_F, GoSignal_F, Positive_F,
                                Terminate_F, Disconnect_F, OntoMsBus_F,
                                InputPositive_F,

                                GateFunction, ClockEnable, OscillatorDivider,
                                TriggerDivider, TriggerMultiplier, ClockSource,
                                TriggerTerminate, GateTerminate, TriggerLevel,
                                GateLevel, GateDelay, MS_Bus, InternalClock,
                                PresetValue, GateDivider, ForceTrigger,
                                StepBackTime, BurstCounter, PositiveGate,
                                InternalTrigger, IgnoreGate, SynchronizeGate,
                                RisingEdge, StopOnPreset, ResetWhenDone,
                                TriggerEnable, DG_Number)
    log.debug("Result of Set command: %s", str(result))
    return result


def shutdown_DG(_mydll):
    print _mydll.Release_DG_Data()


def main(argv):
    mydll = initialize_DG()
    set_DG(mydll, InternalClock=1, OutputModulo_F=1, PulseWidth_A=0.05,
           PulseWidth_B=0.05, PulseWidth_C=0.05, PulseWidth_E=0.05,
           PulseWidth_F=0.05)
#    test_DG(mydll)   
#    filename =r'C:\Users\fkatzsch\Documents\Data\DiscreteTimeQuantumWalk\PercolationPulseFiles\20140514\64_pattern_7509466515219024133795007013273930_pulses_out.fpm'
    ch1, ch2, ch3, ch4 = initialize_DelayLists()
#    ch1, ch2, ch3, ch4 = readin_pattern(filename, ch1, ch2, ch3, ch4)
    ch1.set_DelayList(np.ones(5)*0)
    ch2.set_DelayList(np.ones(5)*0.5)
    ch3.set_DelayList(np.ones(5)*1)
    ch4.set_DelayList(np.ones(5)*0)
#    set_DelayList(mydll, ch1)
#    set_DelayList(mydll, ch2)
#    set_DelayList(mydll, ch3)
#    set_DelayList(mydll, ch4)
    load_params(mydll)
#    sleep_factor = 1
#    rounds_factor = 2
#    raw_input('Check output on Oszi... wait to continue')
#    for i in np.arange(rounds_factor):
#        log.debug("Loop No.: ", str(i))
#        set_DG(mydll, FireFirst_A=0.2*i, FireFirst_B=0.2*i,
#               FireFirst_C=0.2*i, FireFirst_E=0.2*i,
#               InternalClock=9, OutputModulo_F=5, PulseWidth_A=0.05,
#               PulseWidth_B=0.05, PulseWidth_C=0.05, PulseWidth_E=0.05,
#               PulseWidth_F=0.05)
##        ch1.set_DelayList(np.ones(5)*(0.01 + (i*0.04)))
##        ch1.printtimes()
##        ch2.set_DelayList(np.ones(5)*(0.02 + (i*0.04)))
##        ch2.printtimes()
##        ch3.set_DelayList(np.ones(5)*(0.03 + (i*0.04)))
##        ch3.printtimes()
##        ch4.set_DelayList(np.ones(5)*(0.04 + (i*0.04)))
##        ch4.printtimes()
##        set_DelayList(mydll, ch1)
##        set_DelayList(mydll, ch2)
##        set_DelayList(mydll, ch3)
##        set_DelayList(mydll, ch4)
##        load_params(mydll)
#        print "Begin sleep"
#        sleep(sleep_factor)
#        print "Sleep ended"
##    filename=r'C:\Users\fkatzsch\Documents\Data\DiscreteTimeQuantumWalk\PercolationPulseFiles\20140516\2_pattern_22528399545657072401385021039821784_pulses_out.fpm'
##    ch1, ch2, ch3, ch4 = readin_pattern(filename, ch1, ch2, ch3, ch4)
##    set_DelayList(mydll, ch1)
##    set_DelayList(mydll, ch2)
##    set_DelayList(mydll, ch3)
##    set_DelayList(mydll, ch4)
##    load_params(mydll)
##    path_new = raw_input('Check output on Oszi... wait to continue')
#
##    for pfilename in glob(os.path.join(settings["path"],
##                                               '*_pulses.txt')):
##                print "starting with file: " + os.path.split(pfilename)[1]
    raw_input('done yet?')
    deactivate_DG(mydll)
    shutdown_DG(mydll)
    logging.shutdown()

if __name__ == "__main__":
    mydll = main(sys.argv[1:])
