# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 15:51:07 2015

File for Timing Calculations with our new 805nm Fibers with 448m and 470m and
APC couplers based on the timing_calculation.py from Percolation

@author: felster
"""
import os
import sys
sys.path.append("..\DataAnalysis")
from glob import glob
import numpy as np
#import scitools.pprint2 as pp
#import pprint as pp
from collections import deque
from itertools import izip
import matplotlib.pyplot as plt

import save_settings_tofile as sstf

import logging
import logging.config
import logging.handlers

logging.config.fileConfig('loggingConfig.txt')
log = logging.getLogger("myLogger.timing_calculation")


class FuncFilter(logging.Filter):
    def __init__(self, name=None):
        pass

    def filter(self, rec):
        if rec.funcName in ['sum_over_grid']:
            return False
        else:
            return True

funcfilter = FuncFilter()
log.addFilter(funcfilter)

setting_label = '# Settings for Timing_calculation: '

settings = dict(            #step_delta and pulse_delta extracted by Sonja from
#150519_152032_iV_22.5_0_0_0_EOM_finiteWalk_off_0th run_60sec_calcfrombin.txt

    step_delta=2.2940e-6,  # 2.19863e-6 Round trip time in the experiment for the shortest
                           # possible route
    step2_delta=678.73e-9,  # possibility to have different RTT's for splitstep
                            # versions of the walk
    split_step=0,           # if 0 step2_delta has no effect, if 1 step2_delta
                            # will be used
    pulse_delta=0.1049e-6,   # =0.113676e-6 time distance created by the fiber length
                            # difference
    max_steps=11,   # Number of steps to simulate for checking for
                   # possible overlaps between empty switchings and
                   # actual pulse positions
#    max_ini_pos=2, #Max number of initially occupied positions
    window_width=28e-9,  # 31e-9 defines how wide the EOM = on window is
    empty_switch_window=10e-9,  # how wide the distance between the on and off
                                # switchings of an empty switching are
    inhibit=1e-6,  # Obsolete ATM, cause value is scanned by script to find
                   # optimum starting with biggest values first
    start_inh=2.275,   # largest inhibit to start from scanning down to find
                       # a working one [in µs] (nice if you want to have the
                       # same inhibit for several files but one gives you
                       # always a too large inhibit... with this you can force
                       # a smaller one)
    scan_speed=1e1,    # 1e3 = ns, 1e2 = 10 ns, 1e1 = 100 ns steps for inhibit
                       # scan
    single_offset=1,   # toggle if  every possible offset should be tried until
                       # one that works is found (0) or only the offset
                       # 'offset' should be tried (1).
                       # this will also disable inhibit-scanning. Only
                       # 'start_inh' will be tested
    offset=1.0e-6,   # used to test a specific offset or to save the offset
                       # calculated by a scanning run with single_offset=0
    min_distance_to_inhibit=1e-20,  # minimum distance that all switchings have
                                   # to have before the next inhibit
#    max_inhibit_length=5e-6,        #Maximum length of inhibit
    safety_distance=1e-8,  # region around actual pulses, that may not contain
                           # empty switchings
    single_switch_min_dist=50e-9,  # hard limit of EOM is 50e-9, so we use
                                   # 55e-9 to be on the safe side, since the
                                   # first - fourth delays have an insecurity
                                   # of 1-2 ns each
    delay_gen_modulo=16,  # defines the "length" of the period that is created
    delay_gen_offset=0,  # Obsolete ATM, cause value is scanned by script
    allfiles=0,       # set this flag to load all the pulse-files in path
    path=r"\\131.234.227.48\QW_Data\Pump_modulation",
    myfile="2_pulses_incoupling_outcoupling_step_5_pulses",
    outfile="",  # "_out",
    outfile2="",  # "_human_readable",
    std_pulse_length=0.01,
    fpga_step=12.5e-9,  # Internal Speed of the FPGA (usually 12,5ns = 80 MHz)
    modulo_on=0,  # Switch to toggle that all calculated times are integer
                  # multiples of fpga_step
    measured_times=0,  # Switch to toggle plotting of measured files
    measured_file="",  # "_measured",

    first_delay= 12e-9,   # since the DelayGen produces different offsets per
    second_delay= 12e-9,  # channel usually pairwise, we need an option to
    third_delay=-0e-9,   # already include this in the file generation
    fourth_delay=-0e-9,  # Delay values could be + or - and have to be in s
    # typical values are 3 ns and -9 ns (usually sums up to 12)
    # The numbering refers to the colum in the resulting files
)

settings["outfile"] = settings["path"] + "\\" + settings["myfile"] + "_out" + \
    ".fpm"
settings["outfile2"] = settings["path"] + "\\" + settings["myfile"] + \
    "_human_readable" + ".txt"
settings["measured_file"] = settings["path"] + "\\" + settings["myfile"] + \
    "_measured" + ".txt"
settings["myfile"] = settings["path"] + "\\" + settings["myfile"] + ".txt"
settings['start_inh'] = settings['start_inh'] * settings['scan_speed']


def calculate_pulse_time(settings, step, pulse, max_ini_pos):
    """
    Function to calculate the time of "pulse" in step "step", when each odd
    step takes "step_delta" and even steps take "step2_delta" and pulse
    separation is "pulse_delta". Pulses have to be labled according to their
    real position inside the walk, i.e. in even steps can only be even
    positions and vice versa. (Counting convention for steps is, you start with
    step 1 after the fist separation = step 1 has two positions)
    """
    if (abs(pulse) > step + max_ini_pos) or (pulse % 2 != step % 2):
        log.warn("Step %s does not contain position %s", str(step), str(pulse))
    else:
        if settings['split_step'] == 1:
            step_time = (step % 2 + int(step)/2) * settings['step_delta'] +  \
                int(step)/2 * settings['step2_delta']
        else:
            step_time = step * settings['step_delta']
        log.debug("Step_Time for Step %s = %s", str(step), str(step_time))
        pulse_time = (step + pulse)/2. * settings['pulse_delta']
        time = step_time + pulse_time
        log.debug("Time for Step %s and Pulse %s = %s", str(step), str(pulse),
                  str(time))
        return time


def read_pulses(settings):
    """
    Function to read a pulse pattern file. Line one describes first stepTopPhase_case_3_4_pulses
    (the one with two positions), two the second etc. "S" means skip, so there
    is no switching in this step.
    """
    with open(settings['myfile'], 'r') as f:
        lines = f.readlines()
        f.closed
    return lines
#    params = lines[7]
#    params = params.split()
#    timeconst = float(params[4])   # usually 82 ps = 82e-12 seconds
#
#    data = np.genfromtxt(myfile, comments='#', invalid_raise=True)


def check_pulses(step, pulse, max_ini_pos):
    """
    Function to check if a combination of step and pulse are valid or not.
    """
    if pulse == 's' or pulse == 'S':
        return False
    else:
        try:
#            if (int(step) % 2 == int(pulse) % 2):
#                and \
#                    int(step) >= abs(int(pulse)):
#            print 'int(step)'
#            print int(step)
#            print 'abs(int(pulse))'
#            print abs(int(pulse))
#            print 'int(step) % 2'
#            print  int(step) % 2
#            print 'int(pulse) % 2'
#            print int(pulse) % 2
            if ((int(step) + max_ini_pos) >= abs(int(pulse))):
                #and ((int(step) % 2 +1) == int(pulse) % 2):
                return True
            else:
                
                log.warn("There are invalid entries in line %s in the pulse" +
                         "file!", str(step))
                sys.exit(1)
                return False
        except ValueError:
            log.warn("There are invalid characters in line %s in the pulse" +
                     "file!", str(step))
            sys.exit(1)
            return False


def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def compile_pulse_time_list(lines, settings):
    """
    Function to calculate when the pulses will happen after the trigger event
    and also to extract and define what kind of switching has to happen
    """
    pulse_times = []
    pulse_type = []
    for step, pulse_list in enumerate(lines):
        step = step+1            # Because steps start at 1 and not at 0
        pulse_list = pulse_list.split()
        for item in pulse_list:
            if RepresentsInt(item):
                if check_pulses(step, item, settings['max_ini_pos']):
                    time = calculate_pulse_time(settings, step, int(item))
                    log.debug("Step %s Pulse %s has time %s", str(step),
                              str(item), str(time))
                    pulse_times.append([step, int(item), time, 0])
            else:
                if (item == 's' or item == 'S'):  # in this step nothing has
                                                   # to happen ->skip
                    pass
                else:
                    pulse_type.append(item)
    for index in np.arange(np.alen(pulse_times)):
        try:
            pulse_times[index].append(pulse_type[index])
        except:
            log.warn("\nError in list combination of times and switch type\n" +
                     "There is probably an error in the pulse file!\n")
            sys.exit(1)

    pulse_times = set_pulse_switching_mode(pulse_times)
#    log.debug(pp.pformat(pulse_times))
    result, result_type = create_switching_times(settings, pulse_times)
    return result, result_type


def get_all_pulse_positions(settings):
    """
    Function to get a complete list of all possible pulse positions to check if
    problems with empty switchings could arise
    """
    pulse_times = []
    for step in np.arange(1, settings['max_steps']+0.1):
        for pulse in np.arange(-(step), (step+0.1), 2):
            if check_pulses(step, pulse, settings['max_ini_pos']):
                time = calculate_pulse_time(settings, step, pulse, settings['max_ini_pos'])
                pulse_times.append([step, pulse, time])
#    log.debug(pp.pformat(pulse_times))
    pulse_times = np.array(pulse_times)
    return pulse_times[:, 2]


def assign_switching_state(idx, start):
    """
    set switching mode to 0 for no neighbor, to 1 for the first neigbor, to 2
    for last neigbor and to 3 for intermediate neigbor. Also detects adjacent
    pulses, where we have to switch from r to t or vice versa
    """
    if (start[idx, 0] == start[idx-1, 0] and    # same step? (left neighbour)
        start[idx, 4] == start[idx-1, 4] and    # same type?
        start[idx, 0] == start[idx+1, 0] and    # same step? (right neighbour)
        start[idx, 4] == start[idx+1, 4] and    # same type?
        abs(int(start[idx, 1]) - int(start[idx-1, 1])) == 2 and     # neighboured positions? e.g. -3 and -1 and 1?
            abs(int(start[idx, 1]) - int(start[idx+1, 1])) == 2):
        start[idx, 3] = 3
    elif (start[idx, 0] == start[idx-1, 0] and  # same step? (left neighbour)
          start[idx, 4] == start[idx-1, 4] and  # same type?
          abs(int(start[idx, 1]) - int(start[idx-1, 1])) == 2): # neighboured positions? e.g. -3 and -1?
        start[idx, 3] = 2
    elif (start[idx, 0] == start[idx+1, 0] and  # same step? (right neighbour)
          start[idx, 4] == start[idx+1, 4] and  # same type?
          abs(int(start[idx, 1]) - int(start[idx+1, 1])) == 2):  # neighboured positions? e.g. -1 and 1?
        start[idx, 3] = 1


def set_pulse_switching_mode(pulse_times):
    """
    Function to determine if a real switching has to occur or if you
    "can leave the EOM on" because of the previous pulse. The pulse switching
    mode is stored in the 4th column in pulse times.
    0 = switch on and off, 1 = switch only on, 2 = switch only off
    """
    #generate only a list of the timings
    result_list = []
    start = [[0, 0, 0, 0, 0]]
    for item in pulse_times:
        start.append(item)
    start.append([0, 0, 0, 0, 0])
    start = np.array(start)

    for idx in np.arange(1, np.alen(start)-1):
        assign_switching_state(idx, start)
    for item in start:
        if item[0] != '0':
            result_list.append(item)
    log.debug("Result_list: \n %s", pp.pformat(result_list))
    return result_list


def generate_output_file(filename, result_lines):
    """
    Function to generate the outputfile, which can be read by the delay
    generator
    """
    
    with open(filename, 'w') as f:
        for lines in result_lines:
            f.write(lines)
        #write pulses to file
        f.closed


def scan_trough_timeslots(settings, new_switch_times, switch_types):
    """
    Function to distribute the switchings between the timeslots defined by the
    Inhibit of the DelayGenerator. (Always 4 per Inhibit)
    """
    time_list_pos = 0
    switches_per_pos = 0
    empty_switchings = []
    result_times = []
    result_types = []
    proximity = False
    offset_ok = True
    for idx, counter in enumerate(np.arange(0, settings['delay_gen_modulo']) *
                                  settings['inhibit']):
        if (switches_per_pos == 0 or switches_per_pos == 4) and \
                proximity is False:
            pass
        else:
#            print "Warning, distribution over Inhibit pos or close!"
            switches_per_pos = 0
            time_list_pos = 0
            proximity = False
            del result_times[:]
            del empty_switchings[:]
            del result_types[:]
            offset_ok = False
            return result_times, result_types, empty_switchings, offset_ok
#        print "Timeslot: " + str(counter)
        while time_list_pos < np.alen(new_switch_times):
#            print "Timelist_pos: "+str(time_list_pos)
            if (counter <= new_switch_times[time_list_pos] <
                    counter+settings['inhibit']) and switches_per_pos < 4:
                result_times.append(new_switch_times[time_list_pos] -
                                    counter)
                result_types.append(switch_types[time_list_pos])
#                print "time appended!"
                if (((new_switch_times[time_list_pos]-counter) <
                    settings['min_distance_to_inhibit']) or
                    (settings['inhibit'] + counter -
                    new_switch_times[time_list_pos] <
                        settings['min_distance_to_inhibit'])):
#                            or counter > settings['max_inhibit_length']:
#                    print "Proximity warning!"
                    proximity = True
                    break
#                print "Switch no.: "+str(time_list_pos)
                switches_per_pos += 1
                time_list_pos += 1
#                print "Switches so far: "+str(switches_per_pos)
            else:
                if counter > new_switch_times[time_list_pos]:
#                    print "More than four switchings for the last inhibit!"
                    proximity = True
                    break
                elif switches_per_pos == 2 and proximity is False:
                    if idx % 2 == 0:
                        empty_switchings.append(counter)
    #                    print "Include only one empty switching"
                        result_times.append(0.0)
                        result_times.append(0.0)
                        result_types.append('e')
                        result_types.append('e')
    #                    print "Resetting switches per pos!"
                        switches_per_pos = 0
                        break
                    else:
                        empty_switchings.append(counter+settings['inhibit']
                                                - 0.07e-6)
    #                    print "Include only one empty switching"
                        result_times.append(settings['inhibit'] - 0.07e-6)
                        result_times.append(settings['inhibit'] - 0.07e-6)
                        result_types.append('e')
                        result_types.append('e')
    #                    print "Resetting switches per pos!"
                        switches_per_pos = 0
                        break
                elif switches_per_pos == 0 and proximity is False:
                    if idx % 2 == 0:
                        empty_switchings.append(counter)
                        empty_switchings.append(counter+0.1e-6)
    #                    print "Include full empty switching!"
                        result_times.append(0.0)
                        result_times.append(0.0)
                        result_times.append(0.1e-6)
                        result_times.append(0.1e-6)
                        result_types.append('e')
                        result_types.append('e')
                        result_types.append('e')
                        result_types.append('e')
    #                    print "Resetting switches per pos!"
                        switches_per_pos = 0
                        break
                    else:
                        empty_switchings.append(counter+settings['inhibit']
                                                - 0.07e-6)
                        empty_switchings.append(counter+settings['inhibit']
                                                - 0.14e-6)
    #                    print "Include full empty switching!"
                        result_times.append(settings['inhibit'] - 0.07e-6)
                        result_times.append(settings['inhibit'] - 0.07e-6)
                        result_times.append(settings['inhibit'] - 0.14e-6)
                        result_times.append(settings['inhibit'] - 0.14e-6)
                        result_types.append('e')
                        result_types.append('e')
                        result_types.append('e')
                        result_types.append('e')
    #                    print "Resetting switches per pos!"
                        switches_per_pos = 0
                        break
                elif switches_per_pos == 4 and proximity is False:
#                    print "Seems good - ressetting switches per pos!"
                    switches_per_pos = 0
                    break
                else:
#                    print "The mega error occured!"
#                    proximity = True
#                    print switches_per_pos
#                    print proximity
                    break
        if switches_per_pos == 2 and proximity is False:
            if idx % 2 == 0:
                empty_switchings.append(counter)
#                    print "Include only one empty switching"
                result_times.append(0.0)
                result_times.append(0.0)
                result_types.append('e')
                result_types.append('e')
#                    print "Resetting switches per pos!"
                switches_per_pos = 0
            else:
                empty_switchings.append(counter+settings['inhibit']
                                        - 0.07e-6)
#                    print "Include only one empty switching"
                result_times.append(settings['inhibit'] - 0.07e-6)
                result_times.append(settings['inhibit'] - 0.07e-6)
                result_types.append('e')
                result_types.append('e')
#                    print "Resetting switches per pos!"
                switches_per_pos = 0
    if proximity:
        offset_ok = False
#    pp.pprint(result_times)
    return result_times, result_types, empty_switchings, offset_ok


def create_delay_gen_timings(settings, pulse_times, switch_times,
                             switch_types, last_offset):
    """
    Function to create the string to save to the delay gen file, as well as all
    empty switchings to check for collisions and the offset needed to fit
    everything between inhibits
    """
    loops = 0
    success = False
    help_offset = 0
    if settings['single_offset'] == 1:
        new_switch_times = switch_times + settings['offset']
        result_times, result_types, empty_switchings, offset_ok = \
            scan_trough_timeslots(settings, new_switch_times, switch_types)
        if offset_ok is False:
            log.info("Offset of %s is not working with Inhibit %s",
                     settings['offset'], settings['inhibit'])
            generate_plot(settings, settings['offset'], pulse_times,
                          switch_times, empty_switchings)
            sys.exit(0)
        log.info("Testing offset and collisions!")
        if test_for_collisions(empty_switchings, pulse_times,
                                              settings['offset']):
            log.info("No collisions found for offset!")
            help_offset = settings['offset']
            success = True

        else:
            log.info("Collision at desired offset %s found!",
                     settings['offset'])
            sys.exit(0)
    else:
#    pp.pprint(switch_times)
        for offset in np.arange(last_offset, settings['inhibit'],
                                1e-10):
#            print "Offset is now: "+str(offset)
            loops += 1
            new_switch_times = switch_times + offset
    #        pp.pprint(new_switch_times)
            result_times, result_types, empty_switchings, offset_ok = \
                scan_trough_timeslots(settings, new_switch_times, switch_types)
            if offset_ok is False:
                continue
            log.info("Testing for collisions!")
            if test_for_collisions(empty_switchings, pulse_times, offset):
                log.info("No collisions found for offset: %s!", offset)
                help_offset = offset
                success = True
                break
            log.debug("Offset (in_loop): %s", str(offset))
#    pp.pprint(result_times)
#    pp.pprint(empty_switchings)
    log.debug("Inhibit: %s", str(settings['inhibit']))
    log.debug("Offset: %s", str(help_offset))
    log.debug("Loops: %s", str(loops))
#    print np.shape(result_times)
#    for item in izip(result_types[::4], result_types[1::4],
#                     result_types[2::4], result_types[3::4]):
#        print item
#    print np.shape(result_types)
    return empty_switchings, result_times, result_types, help_offset, success


def modulo_check(settings, time):
    """
    Function to make sure "time" is always set to the nearest integer multiple
    of "fpga_step", functionality disabled if "modulo_on" == 0
    """
    if settings['modulo_on'] == 1:
        div_result, mod_result = divmod(time, settings['fpga_step'])
        if mod_result < settings['fpga_step']/2.:
            return div_result * settings['fpga_step']
        else:
            return (div_result + 1) * settings['fpga_step']
    else:
        return time


def create_switching_times(settings, switch_times):
    """
    Function to generate a single array containing the exact times to switch.
    for three (or more) neighboring pulses where item[3] == 3 no extra times
    are appended to the queue
    """
    queue = deque([])
    type_queue = deque([])
    for item in switch_times:
#        print item
        if item[3] == '0':
            #print "an und aus"
            queue.append(modulo_check(settings,
                                      np.float64(item[2]) -
                                      settings['window_width']/2.))
            type_queue.append(item[4])
            type_queue.append(item[4])
            queue.append(modulo_check(settings,
                                      np.float64(item[2]) +
                                      settings['window_width']/2.))
        elif item[3] == '1':
           # print "nur an"
            queue.append(modulo_check(settings,
                                      np.float64(item[2]) -
                                      settings['window_width']/2.))
            type_queue.append(item[4])
        elif item[3] == '2':
           # print "nur aus"
            queue.append(modulo_check(settings,
                                      np.float64(item[2]) +
                                      settings['window_width']/2.))
            type_queue.append(item[4])
#    print np.shape(np.array(queue))
#    print np.shape(np.array(type_queue))
    return np.array(queue), np.array(type_queue)


def test_for_collisions(empty_switchings, pulse_times, offset):
    """
    Function to test if there are collisions between the empty switchings
    and actual pulse positions
    """
    new_pulse_times = pulse_times + offset
    for pulse in new_pulse_times:
        for switching in empty_switchings:
            if switching-settings['safety_distance']/2. < pulse < \
                    switching+settings['safety_distance']/2.:
                log.warn("Pulse collision with empty switching at: %s",
                         str(pulse))
                return False
    return True


def security_check(settings, quartett):
    """
    Function to make sure, that a tuple of four switch times are not to close
    together to damage the EOM electronics
    """
#    print 'checking this quartett'
#    
#    print quartett
    
    if (abs(quartett[2]-quartett[0]) < settings['single_switch_min_dist'] or
            abs(quartett[3]-quartett[1]) < settings['single_switch_min_dist']):
        return False
    else:
        return True


def order_switch_times(settings, switch_times, switch_types):
    """
    Function to make sure only positive switchings happen and empty switchings
    are always positioned at the beginning of an inhibit window
    ordering of h's results from switching scheme:
    B_on, A_on, A_off, B_off, since starting with A gives us identity
    switchings instead of reflections'
    """
    result_array = []
    result_lines = []
    counter=0
    for x1, x2, x3, x4, t1, t2, t3, t4 in izip(switch_times[::4],
                                               switch_times[1::4],
                                               switch_times[2::4],
                                               switch_times[3::4],
                                               switch_types[::4],
                                               switch_types[1::4],
                                               switch_types[2::4],
                                               switch_types[3::4]):
#        x1, x2, x3, x4 = np.sort([x1, x2, x3, x4])
        counter += 1
        quartett = []
        if x1 > x3:
            first_switch_times = [x3, x4]
            first_switch_type = [t3, t4]
            second_switch_times = [x1, x2]
            second_switch_type = [t1, t2]
        else:
            first_switch_times = [x1, x2]
            first_switch_type = [t1, t2]
            second_switch_times = [x3, x4]
            second_switch_type = [t3, t4]
        if (not(first_switch_type[0] == first_switch_type[1]) or
                not(second_switch_type[0] == second_switch_type[1])):
            log.warn("Something very terrible happened!" +
                     " (Probably invalid char)")
            sys.exit(1)
        if (first_switch_type[0] == 'r' or first_switch_type[0] == 'e'):
            quartett.append(first_switch_times[1] + settings['first_delay'])
            quartett.append(first_switch_times[0] + settings['second_delay'])
            # 21
        elif first_switch_type[0] == 't':
            quartett.append(first_switch_times[0] + settings['first_delay'])
            quartett.append(first_switch_times[1] + settings['second_delay'])
            # 12
        if (second_switch_type[0] == 'r' or second_switch_type[0] == 'e'):
            quartett.append(second_switch_times[0] + settings['third_delay'])
            quartett.append(second_switch_times[1] + settings['fourth_delay'])
            # 34
        elif second_switch_type[0] == 't':
            quartett.append(second_switch_times[1] + settings['third_delay'])
            quartett.append(second_switch_times[0] + settings['fourth_delay'])
            # 43
        if security_check(settings, quartett):
            result_array.append(quartett)
           # del quartett[:]
        else:
            log.warn(pp.pformat("There was a switch error (to fast " +
                                "switching of a single switch)"))
            sys.exit(1)
    for item in result_array[::-1]:
#        print str(item)
        result_lines.append('{0:.3f}'.format(item[0] * 1e6) + ' ' +
                            '{0:.3f}'.format(item[1] * 1e6) + ' ' +
                            '{0:.3f}'.format(item[2] * 1e6) + ' ' +
                            '{0:.3f}'.format(item[3] * 1e6) + '\n')
#    print result_lines
    return result_lines


def display_calculated_times(empty_switchings, old_switch_times, offset):
    pp.float_format = '%.3E'
    print "\nold Switchtimes (without / with offset): "
    pp.pprint([(i, i + offset) for i in old_switch_times])
#            print "\nnew Switchtimes: "
#            pp.pprint(switch_times)
    print "\nTime difference between switchings: "
    pp.pprint([j-i for i, j in zip(old_switch_times[:-1],
                                   old_switch_times[1:])],
              width=60)
    print "\nEmpty switchings (will not be printed double for " + \
          "coinciding channels): "
    pp.pprint(empty_switchings, width=50)


def read_measured_times(settings):
    """
    Function to read timings to plot with the calculated timings
    """
    with open(settings['measured_file'], 'r') as f:
        lines = f.readlines()
        f.closed
    measured_times = []
    for line in lines:
        line = line.split()
        for time in line:
            measured_times.append(float(time)*1e-6-1.6e-6+6e-9)
#    print measured_times
    return measured_times


def generate_plot(settings, offset, pulse_times, switch_times,
                  empty_switchings):
    """
    Function to plot the timings
    """
    old_switch_times = switch_times

    time = np.arange(0, settings['inhibit']*settings['delay_gen_modulo'])
    plt.plot(time, ':', label='Sum', linewidth=0.5)
#            pp.pprint(pulse_times)
    for pos in np.arange(0, settings['delay_gen_modulo']) * \
            settings['inhibit']:  # plot inhibits (green)
        plt.axvline(pos, color='g', alpha=1, linewidth=3)
    for pos in pulse_times:  # plot actual pulse positions (blue area)
        pos = pos + offset
        plt.axvspan(pos-(settings['safety_distance']),
                    pos+(settings['safety_distance']),
                    color='b', alpha=0.1)
    for pos in old_switch_times:  # times where reguar switches occur (red)
        pos = pos + offset
        plt.axvline(pos, color='r', alpha=1)
    for pos in empty_switchings:
        plt.axvline(pos, color='b', alpha=1, linewidth=2, linestyle = '--')
    if settings['measured_times'] == 1:
        measured_times = read_measured_times(settings)
        for pos in measured_times:
            plt.axvline(pos, color='r', alpha=1, linewidth=2, ymax=0.75)


def generate_human_readable(settings, switch_times):
    """
    """
    result = []
    for idx, [x1, x2, x3, x4] in enumerate(izip(switch_times[::4],
                                                switch_times[1::4],
                                                switch_times[2::4],
                                                switch_times[3::4])):
        x1 = x1 + (idx + 1) * settings['inhibit']
        x2 = x2 + (idx + 1) * settings['inhibit']
        x3 = x3 + (idx + 1) * settings['inhibit']
        x4 = x4 + (idx + 1) * settings['inhibit']
        result.append(x1)
        result.append(x2)
        result.append(x3)
        result.append(x4)
#    pp.pprint(result)
    return result


def test_inhibit(settings, pulse_times, switch_times, switch_types,
                 last_offset):
    """
    Function to test if a choosen inhibit is working with the choosen pulse
    pattern
    """
    old_switch_times = switch_times.tolist()
    # .tolist needed for pretty printing
    empty_switchings, switch_times, switch_types, offset, success = \
        create_delay_gen_timings(settings, pulse_times, switch_times,
                                 switch_types, last_offset)
    if success is True:
#            display_calculated_times(empty_switchings, old_switch_times,
#                                     offset)
        if settings['allfiles'] == 0:
            generate_plot(settings, offset, pulse_times, old_switch_times,
                          empty_switchings)
        human_readable = generate_human_readable(settings, switch_times)
#        pp.pprint(human_readable)
#        pp.pprint(np.sort(human_readable))
#        pp.pprint(switch_times)

        temp1 = settings['first_delay']
        temp2 = settings['second_delay']
        temp3 = settings['third_delay']
        temp4 = settings['fourth_delay']
        generate_output_file(settings['outfile'],
                             order_switch_times(settings, switch_times,
                                                switch_types))
        # for the human readable output we want the actual times, when a
        # switch has to occur. Therefore the delays are set to zero before
        # the human readable file is created
        settings['first_delay'] = 0
        settings['second_delay'] = 0
        settings['third_delay'] = 0
        settings['fourth_delay'] = 0
        generate_output_file(settings['outfile2'],
                             order_switch_times(settings,
                                                human_readable,
                                                switch_types))
        help_filename = settings['myfile']
        settings['myfile'] = settings['outfile2']
        settings['offset'] = offset
        sstf.handle_settings(settings, True, True, setting_label)
        settings['myfile'] = help_filename
        settings['first_delay'] = temp1
        settings['second_delay'] = temp2
        settings['third_delay'] = temp3
        settings['fourth_delay'] = temp4
        return True, offset
    return False, offset


def main():
    last_offset = 0
    counter = 0
    should_restart = True
    pulse_times = get_all_pulse_positions(settings)
    # work with only one file (-> myfile)
    if settings["allfiles"] == 0:
        log.info("starting with file: %s",
                 os.path.split(settings["myfile"])[1])
        lines = read_pulses(settings)
        switch_times, switch_types = compile_pulse_time_list(lines, settings)
    #    pp.pprint(pulse_times)
        log.debug('Start_inh is: %s', settings['start_inh'])
        for i in np.arange(settings['start_inh'], 5, -1) * 1e-6 / \
                settings['scan_speed']:
                                                 # scan inhibits from 50µs to
                                                 # 0.2µs and stop, if somethig
                                                 # working has been found
                settings['inhibit'] = i
                log.info("Inhibit is now: %s", str(settings['inhibit']))
                result, new_offset = test_inhibit(settings, pulse_times,
                                                  switch_times, switch_types,
                                                  last_offset=0)
                if result is True:
                    log.info("Solution found!")
                    log.info("Inhibit is %s", i)
                    log.info("Offset is %s", new_offset)
                    break
        if result is False:
            log.info("No suitable inhibit found!")
    else:   # load all files in path with ending _pulses.txt if allfiles == 1
        while(should_restart):
            should_restart = False
            for pfilename in glob(os.path.join(settings["path"],
                                               '*_pattern_??.txt')):
                log.info("starting with file: %s", os.path.split(pfilename)[1])
                # extract base-filename from pfilename and update settings
                filenameend = os.path.split(pfilename)[1]
                filename = os.path.splitext(filenameend)[0]
                settings["myfile"] = pfilename
                settings["outfile"] = settings["path"] + "\\" + filename + \
                    "_out" + ".fpm"
                settings["outfile2"] = settings["path"] + "\\" + filename + \
                    "_human_readable" + ".txt"
                settings["measured_file"] = settings["path"] + "\\" + \
                    filename + "_measured" + ".txt"
                # search and save solution for every file
                lines = read_pulses(settings)
                switch_times, switch_types = compile_pulse_time_list(lines,
                                                                     settings)
                for i in np.arange(settings['start_inh'], 5, -1) * 1e-6 / \
                        settings['scan_speed']:
                    settings['inhibit'] = i
                    log.debug("Inhibit is now: %s", str(settings['inhibit']))
                    result, new_offset = test_inhibit(settings, pulse_times,
                                                      switch_times,
                                                      switch_types,
                                                      last_offset)
                    counter += 1
                    if new_offset == last_offset:
                        pass
                    else:
                        last_offset = new_offset
                        should_restart = True
#                        counter += 1
                        break
                    if result is True:
                        log.info("Solution found!")
                        log.info("Counter = %s \n", str(counter))
                        log.info("Inhibit is %s", i)
                        break
                if should_restart is True:
                    break
    logging.shutdown()

if __name__ == '__main__':
    main()

