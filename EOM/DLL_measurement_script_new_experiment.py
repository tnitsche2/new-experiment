# -*- coding: utf-8 -*-
"""
Created on Nov 02 2015

@author: Sonja
based on Dll_qutau_measurmentscript.py by Fabian, but now includes 5 columns in
the time_list (includes a variable inhibit) and is supposed to work for the new
EOMs and the new working scheme (function generator as external clock and gate)
is working with wrapper class BME_DG_dll_script_newFirmware
"""

import subprocess
import numpy as np

import os
import sys
sys.path.append("..\DataAnalysis")
from time import gmtime, strftime, sleep
import BME_DG_dll_script_newFirmware as dll
import BME_DG_dll_script as BME_dll
from glob import glob
import shutil
import bin2histo as b2h
import logging
import logging.config
import DLL_qutau_measurment_script as meas

logging.config.fileConfig('..\\..\\loggingConfig.txt')
log = logging.getLogger("myLogger.DLL_measurement_script_TopPhase")


class FuncFilter(logging.Filter):
    def __init__(self, name=None):
        pass

    def filter(self, rec):
        if rec.funcName in ['sum_over_grid', 'generate_errorbars']:
            return False
        else:
            return True

funcfilter = FuncFilter()
log.addFilter(funcfilter)

def construct_filename(path, time, filemask, pattern, no=1):
    """
    Function to construct the filename and the parameterfilename given to the
    qutau communication
    """
    # extract info from pattern for the complete Meas-filename
    patternname = os.path.basename(pattern).split('_')[1:3]
    # call binary datatype for saving the data
    filename = path + '/' + strftime("%y%m%d") + '_' + strftime("%H%M%S") + \
        '_' + filemask + '_' + patternname[0] + '_' + patternname[1] + '_run' \
        + str(no) + '_' + time + 'sec.bin'
    paramfilename = "-t " + time + ' -F "' + filename
    return paramfilename, filename


def read_pulsefilepath():
    """
    Function to read where the Path to the Pulsepatternfiles is located.
    """
    while(True):
            f = open('pulsepath.ini', 'r')
            path1 = f.readline()[:-1]
            print 'Previous path was : ' + path1
            f.closed
            result = 1
            path_new = raw_input('Type new path for ulseFiles or leave ' +
                                 'empty for last path (Type "full" for full' +
                                 'set of measurements - otherwise only' +
                                 'pattern 1 will be done): ')
            if os.path.isdir(path_new):
                f = open('pulsepath.ini', 'w')
                f.write(path_new+'\n')
                f.write(path1+'\n')
                f.closed
                path1 = path_new
                log.info("Pulsefile Path changed" +
                         " successfully to: %s", path1)
                result = 100
                break
            if path_new == 'full':
                log.info("Keeping Path: %s for Single File measurement", path1)
                result = 100
                break
            else:
                log.info("Keeping previous path: %s",  path1)
                break
    meas.ensure_dir(path1)
    return path1, result


def set_newLists(mydll, filename, ch1, ch2, ch3, ch4, inhibit):
    '''
    included a inhibit_channel
    2.11.2015 by Sonja
    '''
     # fill class fields with switch times from pattern
    ch1, ch2, ch3, ch4, inhibit = dll.readin_pattern(filename, ch1, ch2, ch3, ch4, inhibit)
    result = dll.set_DelayList(mydll, ch1)
    if result != 0:
        log.critical("Error setting DelayList 1... aborting Program!")
        sys.exit(result)
    result = dll.set_DelayList(mydll, ch2)
    if result != 0:
        log.critical("Error setting DelayList 2... aborting Program!")
        sys.exit(result)
    result = dll.set_DelayList(mydll, ch3)
    if result != 0:
        log.critical("Error setting DelayList 3... aborting Program!")
        sys.exit(result)
    result = dll.set_DelayList(mydll, ch4)
    if result != 0:
        log.critical("Error setting DelayList 4... aborting Program!")
        sys.exit(result)
    result = dll.set_InhibitList(mydll, inhibit)
    if result != 0:
        log.critical("Error setting Inhibit List... aborting Program!")
        sys.exit(result)
    # load all parameters to the card
    result = BME_dll.load_params(mydll)
    if result != 0:
        log.critical("Error loading Settings to FPGA... aborting Program!")
        sys.exit(result)


def main(argv):
    movepath = 'E:\Data'
    print ''
    print '===== Welcome to measurement script ====='
    print ''
    pulsefilepath, filenumbers = read_pulsefilepath()
    no = meas.read_number()   # = number of measurements per Pulselist
    if filenumbers == 1:   # load only first file (as a test, hard coded)
        pulsefilelist = glob(os.path.join(pulsefilepath, '*_out.fpm'))[:1]
    else: # filenumbers == 100  -> load all files in pulsefilepath in the list
        pulsefilelist = glob(os.path.join(pulsefilepath, '*_out.fpm'))

    mydll = dll.initialize_DG()
    result = dll.init_allParams(mydll)

    if result != 0 or len(pulsefilelist) == 0:   # if DG does not answer or pulsefilelist empty...
        result = BME_dll.deactivate_DG(mydll)  # deactivate DG if something went wrong
        log.critical("Static setting for DG not working... aborting Program!")
        sys.exit(result)

    # initialization of class instances
    ch1, ch2, ch3, ch4 = dll.initialize_DelayLists()
    inhibit  = dll.initialize_InhibitList()
    # fill class fields with switch times from pattern and load params to card for first pattern
    set_newLists(mydll, pulsefilelist[0], ch1, ch2, ch3, ch4, inhibit)
    result = BME_dll.activate_DG(mydll, DG_Number=0)
    log.info("DelayGenerator running with first pattern, please switch on"
             " EOM Power Supply and turn up the HV to desired level. ")

     # communication with qutau (standard meas script)-----------------------
    path = meas.read_path()
    filemask = meas.read_filemask()
    comment = ''
    if filenumbers == 1:
        comment = meas.read_comment()
    else:
        meas.copy_comment(path)
    time = meas.read_time()
    raw_input("Preparation finished. DelayGenerator running with first pattern,\
              please switch on EOM Power Supply and turn up the HV.\
              Press Enter to start the measurement.")

    for pattern in pulsefilelist:   # go through all pulsefiles
        print "Starting with Pattern:"+str(os.path.basename(pattern))
        # load a list from the pulsefiles
        set_newLists(mydll, pattern, ch1, ch2, ch3, ch4, inhibit)
        if no > 1:  # for more than 1 file create a subfolder!
            path_new = path + '/' + strftime("%y%m%d") + '_' + \
                strftime("%H%M%S") + '_' + filemask + '_' + str(no) + 'x' + \
                time + 's'
            os.makedirs(path_new)

            for i in np.arange(0, no):   #measure files according to number no
                paramfilename, filename = construct_filename(path_new, time, filemask,
                                                             pattern, i)
                print paramfilename
                sleep(1)
                try:   # console communication with qutau
                    log.debug("Starting Subprocess")
                    proc = subprocess.Popen("cmd", shell=True, bufsize=-1,
                                            stdin=subprocess.PIPE)
                    proc.communicate(input="qutau.exe "+paramfilename+"\n")
                    proc.wait()
                    log.debug("Subprocess terminated.")
                    #if retcode < 0:
                    #    print >>sys.stderr, "Child was terminated by signal", -retcode
                    #else:
                    #    print >>sys.stderr, "Child returned", retcode
                except OSError:
                    print >>sys.stderr, "Execution failed:", e
                log.debug("Finished. Moving on to next Pattern.")
                sleep(1)
            try:
                # move folder to another folder (external harddrive) for large data sets
                filepath_only = os.path.split(filename)[0]
#                print filepath_only
#                print movepath
                shutil.move(filepath_only, movepath)
            except:
                log.info('Folder could not be moved!!!')
        elif no == 1:
            paramfilename, filename = construct_filename(path, time, filemask,
                                                             pattern)
            sleep(1)
            try:   # console communication with qutau
                log.debug("Starting Subprocess")
                proc = subprocess.Popen("cmd", shell=True, bufsize=-1,
                                        stdin=subprocess.PIPE)
                proc.communicate(input="qutau.exe "+paramfilename+"\n")
                proc.wait()
                log.debug("Subprocess terminated.")
                #if retcode < 0:
                #    print >>sys.stderr, "Child was terminated by signal", -retcode
                #else:
                #    print >>sys.stderr, "Child returned", retcode
            except OSError:
                print >>sys.stderr, "Execution failed:", e
            log.debug("Finished. Moving on to next Pattern.")
            sleep(1)
            try:
                # move file to another folder (external harddrive) for large data sets
                name_only = os.path.basename(filename)
                filename_new = os.path.join(movepath, name_only)
                shutil.move(filename, filename_new)
            except:
                log.info('File could not be moved!!!')
          

        # create histogram-txt-file from binary if only 1 file################
#    if filenumbers == 1:
#        log.info('starting to convert saved bindata to histogram txt.file')
#        data_time, data_chan = b2h.readbin2memory(filename, count=-1)
#        histo, av_counts_per_frame = b2h.data2histo(data_time, data_chan, 2)
#        txtfilename = os.path.splitext(filename)[0] + "_calcfrombin.txt"
#        b2h.savehisto2txt(txtfilename, histo, comment)
#        log.info('conversion into histogram txt.file ' + txtfilename + ' done')

        # remove binary aftern conversion?########################
#        while (True):
#            if os.access(filename, os.W_OK):
#                os.remove(filename)
#                break
#            print "No file access to binary file yet!"
#            time.sleep(0.5)
#        print "Binary file removed!"

    print "All Measurements done. Turn down HV and switch off EOM."
    raw_input("Press enter do deactivate DG and close programm?")
    result = BME_dll.deactivate_DG(mydll)
    log.info("Deactivating DG")
    logging.shutdown()

if __name__ == "__main__":
    main(sys.argv[1:])
