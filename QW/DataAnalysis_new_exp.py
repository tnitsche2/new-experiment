# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 14:42:00 2013

@author: Katzschmann
"""

import os
import sys
sys.path.append("..\Theory")

import matplotlib.pyplot as plt
from glob import glob
from mpl_toolkits.mplot3d import axes3d
from matplotlib.colors import Normalize
import numpy as np
from matplotlib import cm
import pprint as pp

import theory_1D_dtqw_new_exp as theo
import save_settings_tofile as sstf
#import peakdetect as pd

from time import *
import logging
import logging.config
import logging.handlers

logging.config.fileConfig('loggingConfig.txt')
log = logging.getLogger("myLogger.DataAnalysis")


class FuncFilter(logging.Filter):
    def __init__(self, name=None):
        pass

    def filter(self, rec):
        if rec.funcName in ['sum_over_grid']:
            return False
        else:
            return True

funcfilter = FuncFilter()
log.addFilter(funcfilter)
############################################################
# Variable declaration part
############################################################

lines = []

setting_label = '# Settings for DataAnalysisScript: '
save_settings = False
ignore_file_settings = False


settings = dict(
    tdc='AIT',    # 'bin2histo' for bin2histo function in the new qutau
                        # 'qutau' for the qutau and its -H histogram function,
                        # just something else for old one
                        #AIT for the TDC used with the new experiment
    new_exp=1,      #respecting pecularities of new exp? 1 => yes, 0 => no
    sampling=1,     #Is each step sampled in a separate measurement?  1 => yes, 0 => no
    recurrence_restart=0,    #Is the recurrence to be extracted in a protocol assuming restart after the measurement?
                            #1 => yes, 0 => no
    recurrence_continue=1,   #Is the recurrence to be extracted in a protocol assuming continuation after the measurement?
                            #1 => yes, 0 => no
    sinks=1,                #Are sinks to be included in the theory? In this case a 't' in the reflect list is a sink
                            #Works via implementing hungry EOM coin; 1 => yes, 0 => no
    sink_transmission=0.1,    #Residual transmission of sinks due to imperfect extinction
    timeconst=82.3045e-12,#161.91e-12,  # change to 80e-12 for the old tdc and 161.91e-12
                           # for the new one and for bin2histo
    path = r"\\131.234.203.7\QW_Data\161024\sink",
    path_no_sink = r"\\131.234.203.7\QW_Data\161024\no_sink",
    praefix="outstep02_161024_145423_10sec_calcfrombin.txt",
    Ha_offset=33,
    Va_offset=0,
    Hb_offset=0,
    Vb_offset=23,
    peak_width=60,

    max_steps=11,    # calculates only steps up to max_steps -1
    second_peak_pos=33362,#33421,#33362,  # 5050 for iH, 4767 for iV
                           # for the "even" grid, so the second step should
                           # fit / all should fit if not split_step walk
    tau1=27881,  # 4208,4220    # round trip time from step to step
    tau2=1273,    # time between positions in a step
                   # (big time diff due to fiber loop)
    tau3=12,     # time between positions in a step
                   # (small time diff due to freespace difference)
#    tau_offset2= -2614, # for the "odd" grid, so the first step should fit
                    # (with two peaks)

    detection_eff_Ha=0.5,
    detection_eff_Va=1,
    detection_eff_Hb=1,
    detection_eff_Vb=1,
    background_sample_size=400,
    
    numerical_error_bars=0,  # is it desired to generated error bars for the numerics via Monte Carlo Sim

    steps_to_plot=1,  # i.e. Step number to start plotting from to max_steps
    plot=1,           # toggle if acutal plots are desired or only console
                      # output
    verbose=1,        # the bigger the number, the more output on the console
    display_grid=1,
    walk_2d=0,
    split_step=0,  # set to 1 for a split step QW
    percolation=0,  # toggle to switch the percolation results to show

    alpha_blue_grid=0.3,
    savedata2file=0,  # should the analysed data (= barcharts) be saved 2 file?
                      # works only for steps_to_plot > 0
    savetheo2file=0,  # should the correspondC:\Users\User\Desktop\Meas_temp\try_6ing theory be saved 2 file?
                      # works only for steps_to_plot > 0

    savedistance2file=1,  # is the distance between theo and exp data to be saved to a file?

    HHa=np.sqrt(0.84),
    HVa=np.sqrt(0.84),
    VHa=np.sqrt(0.84),
    VVa=np.sqrt(0.84),
    HHb=np.sqrt(0.46),
    HVb=np.sqrt(0.52),
    VHb=np.sqrt(0.54),
    VVb=np.sqrt(0.56),


    # assumed errors for error bars-----------------------------------------
    coin_error = 0.1,
    coupl_error = 0.04,
    trans_error = 0.00,
    sink_error = 0.02,
    alpha = 22.5,


    ###################################
    # Theory settings
    ###################################
    HWP_alpha=22.5,  # in  Degree - angle for the HWP if used
                     # 22.5 for Hadamard Coin
    QWP_alpha=0,    # in Degree - angle the QWP in front of the EOM is
                     # aligned for the no HWP scheme
                     # To use this, set phi to -pi/2, 0 and pi/2 and observe
                     # the coins identity, hadamard and not realized
    beta=45,    # in Degree - angle the EOM is oriented for the normal coin
                # (i.e. applies to all positions) default: 45
    phiT=90./360.*2*np.pi,   # in rad - phase the EOM applies at positions from
                            # 'reflect_list' - for Transmissions
    phiR = 90./360.*2*np.pi,   #  for Reflections

    loss_factor=1,   # to simulate more loss in H then in V

    sbc=-0.0*np.pi,  # phase the sbc introduces to H light in the V fiber
                     # should be 0 if the measurement is ideal
    reflect_list=[[], 
                  [],
                  [],
                  ['0 t'],                            
                  [],   
                  ['0 t'],
                  [],
                  ['0 t'],
                  [],
                  ['0 t'],
                  [],
                  ['0 t'],
                  [],
                  ['0 t'],
                  [],
                  ['0 t'],
                  [],
                  ['0 t'],
                  [],
                  ['0 t'],
                  [],
                  ['0 t']],

#    reflect_list=[[],    # mind that theory labels the positions inverse
#                  [],         # to how the data analysis does! So -1 in this
#                  ['-2 r', '2 r'],   # list actually means 1 and vice versa
#                  [],
#                  ['-2 r', '2 r'],
#                  [],
#                  ['-2 r', '2 r'],
#                  [],
#                  ['-2 r', '2 r'],
#                  [],
#                  ['-2 r', '2 r'],
#                  [],
#                  ['-2 r', '2 r'],
#                  [],
#                  ['-2 r', '2 r'],
#                  [],
#                  ['-2 r', '2 r'],
#                  [],
#                  ['-2 r', '2 r'],
#                  [],
#                  ['-2 r', '2 r'],
#                  [],
#                  ['-2 r', '2 r'],
#                  [],
#                  ['-2 r', '2 r'],],C:\Users\User\Desktop\Meas_temp\try_7
    r_coef=np.sqrt(1),        # how well does a reflection work (1 = perfect)
    t_coef=np.sqrt(0.981),    # how well does a transmission work (1 = perfect)
    EOM_active=0,             # toggle to either activate the EOM (1) or just
                              # do a passive walk (0)
    sbc_mode=0,
    qwp_mode=0,  # toggle to switch if there is a HWP in front of the EOM as
                 # coin 1 (0) or a QWP (1)
                 # Attention: qwp_mode=1 with split_step=1 will lead to the
                 # assumption that in the b mode there is also a QWP present
                 # at angle 'qwp_alpha'

    pol='D',     # input polarisation: H,V,D,A,RHC,LHC
    tomography='HV'    # which tomography basis is used: 'HV', 'DA', 'RL'?
)
settings['myfile'] = os.path.join(settings['path'], settings['praefix'])
log.debug("Settings: %s", pp.pformat(settings))
#########################################################################
# Begin of function declarations
#########################################################################


def generate_grid(settings):
    """
    Function to generate the grid that defines the bins
    """
    grid = []
    for n in np.arange(1, settings['max_steps']+0.1):
        for l in np.arange(0, n):
            if not(n % 2 == 0):
                result = settings['tau_offset'] + l*settings['tau2'] + \
                    (n-1)*settings['tau1']
            else:
                result = settings['tau_offset2'] + l*settings['tau2'] + \
                    (n-1)*settings['tau1']
            grid.append(result)
    grid = np.asarray(grid)
    return grid


def generate_2D_grid(settings):
    """
    Function to generate the grid that defines the bins for 2D data
    """
    grid = []
    for n in np.arange(1, settings['max_steps']+0.1):
        for l in np.arange(0, n):
            for k in np.arange(0, n):
                result = settings['tau_offset'] + k*settings['tau3'] + \
                    l*settings['tau2'] + (n-1)*settings['tau1']
                grid.append(result)
    grid = np.asarray(grid)
    return grid


def sum_over_grid(grid, settings, data):
    """
    Function to sum up all counts in one bin and return the bins and their
    cumulative counts
    """
    bins = []
    for pos in grid:
        bins.append(np.sum(data[pos-(settings['peak_width']/2.):
                                pos+(settings['peak_width']/2.)]))
        log.debug("Position: %i", pos)
        log.debug("%s", str(data[pos-(settings['peak_width']/2.):
                  pos+(settings['peak_width']/2.)]))
        log.debug('Summe: %s', str(np.sum(data[pos-(settings['peak_width']/2.):
                                   pos+(settings['peak_width']/2.)])))
    bins = np.asarray(bins)
    return bins


def generate_steps(bins, settings):
    """
    Function to generate single steps out of the complete array of timebins
    """
    step = []
    for i in np.arange(0, settings['max_steps']):
        step.append([bins[i*(i+1)/2:i*(i+1)/2+i+1]])
    return step


def generate_2D_steps(bins, settings):
    """
    Function to generate single steps out of the complete array of timebins for
    2D data
    """
    step = []
    for i in np.arange(0, settings['max_steps']):
        step.append([bins[(2 * i + 1) * (i**2 + i) / 6:(2*i + 1) *
                    (i**2 + i) / 6 + (i + 1)**2]])
       # print "i= "+str(i)+" start= "+str((2*i +1 ) * (i**2 + i)/6 )+\
#                    " end= "+str((2*i +1 ) * (i**2 + i)/6 +(i +1)**2)
    return step


def get_peak_area(start, end, data, detector):
    """
    Function to get an certain area under a peak - not used at the moment
    """
    print np.sum(data[start:end, detector])


def parse_qutau_tdc_file(settings):
    """
    Function to parse histogram files from the qutau tdc
    """
    log.debug("Begin Parsing")
    if ((settings['Ha_offset'] >= settings['Va_offset']) and
        (settings['Ha_offset'] >= settings['Hb_offset']) and
            (settings['Ha_offset'] >= settings['Vb_offset'])):
        big_offset = settings['Ha_offset']
    elif ((settings['Va_offset'] >= settings['Hb_offset']) and
            settings['Va_offset'] >= settings['Vb_offset']):
        big_offset = settings['Va_offset']
    elif (settings['Hb_offset'] >= settings['Vb_offset']):
        big_offset = settings['Hb_offset']
    else:
        big_offset = settings['Vb_offset']

    data = np.genfromtxt(settings['myfile'], comments='#', invalid_raise=True)

    """
    Sample the background of each channel and get the mean as a background to
    subtract from each channel
    """
    background_Ha = np.sum(data[(np.alen(data[:, 3])/2):
                                (np.alen(data[:, 3])/2) +
                                settings['background_sample_size'], 3]) / \
        settings['background_sample_size']
    background_Va = np.sum(data[(np.alen(data[:, 3])/2):
                                (np.alen(data[:, 3])/2) +
                                settings['background_sample_size'], 4]) / \
        settings['background_sample_size']
#    background_Hb = np.sum(data[(np.alen(data[:, 3])/2):
#                                (np.alen(data[:, 3])/2) +
#                                settings['background_sample_size'], 6]) / \
#        settings['background_sample_size']
#    background_Vb = np.sum(data[(np.alen(data[:, 3])/2):
#                                (np.alen(data[:, 3])/2) +
#                                settings['background_sample_size'], 5]) / \
#        settings['background_sample_size']

    log.debug("Subtracted Background Ha: %s", str(background_Ha))
    log.debug("Subtracted Background Va: %s", str(background_Va))
#    log.debug("Subtracted Background Hb: %s", str(background_Hb))
#    log.debug("Subtracted Background Vb: %s", str(background_Vb))

    """
    The APD's are connected to the tdc in the following order: Ha, Va, Vb, Hb
    -> thats why the data assignment is 3, 4, 6, 5
    """
    Ha = np.append(np.append(np.zeros(settings['Ha_offset']), data[(
        np.alen(data[:, 3])/2):, 3] - background_Ha),
        np.zeros(big_offset - settings['Ha_offset']))
    Va = np.append(np.append(np.zeros(settings['Va_offset']), data[(
        np.alen(data[:, 3])/2):, 4] - background_Va),
        np.zeros(big_offset - settings['Va_offset']))
#    Hb = np.append(np.append(np.zeros(settings['Hb_offset']), data[(
#        np.alen(data[:, 3])/2):, 6] - background_Hb),
#        np.zeros(big_offset - settings['Hb_offset']))
#    Vb = np.append(np.append(np.zeros(settings['Vb_offset']), data[(
#        np.alen(data[:, 3])/2):, 5] - background_Vb),
#        np.zeros(big_offset - settings['Vb_offset']))

    log.debug("Data shape: %s", np.shape(data))

    timeline = np.zeros((np.alen(Ha), 5))

    summe = (Ha/settings['detection_eff_Ha'] +
             Va/settings['detection_eff_Va'] +
             Hb/settings['detection_eff_Hb'] +
             Vb/settings['detection_eff_Vb'])

#print settings['timeconst']
#print np.alen(Ha)

    time = np.arange(0, np.alen(Ha))
    timeline[:, 0] = time * settings['timeconst']
    timeline[:, 1] = Ha/settings['detection_eff_Ha']
    timeline[:, 2] = Va/settings['detection_eff_Va']
    timeline[:, 3] = Hb/settings['detection_eff_Hb']
    timeline[:, 4] = Vb/settings['detection_eff_Vb']

    return Ha/settings['detection_eff_Ha'], Va/settings['detection_eff_Va'], \
        Hb/settings['detection_eff_Hb'], Vb/settings['detection_eff_Vb'], \
        summe, time, timeline


def parse_new_exp(settings):
    """
    Function to parse the histogram files created from a binary file
    with bin2histo.py or directly via the qutau_measurement_script.py
    """
    log.debug("Begin Parsing")
    if ((settings['Ha_offset'] >= settings['Va_offset']) and
        (settings['Ha_offset'] >= settings['Hb_offset']) and
            (settings['Ha_offset'] >= settings['Vb_offset'])):
        big_offset = settings['Ha_offset']
    elif ((settings['Va_offset'] >= settings['Hb_offset']) and
            settings['Va_offset'] >= settings['Vb_offset']):
        big_offset = settings['Va_offset']
    elif (settings['Hb_offset'] >= settings['Vb_offset']):
        big_offset = settings['Hb_offset']
    else:
        big_offset = settings['Vb_offset']
        
    print 'Opening this file'
    
    print settings['myfile']
    
    

    data = np.genfromtxt(settings['myfile'], comments='#', invalid_raise=True)

    """
    Sample the background of each channel and get the mean as a background to
    subtract from each channel
    """
    background_Ha = np.sum(data[8000: settings['background_sample_size'], 0]) / \
        settings['background_sample_size']
    background_Va = np.sum(data[8000: settings['background_sample_size'], 1]) / \
        settings['background_sample_size']


    log.debug("Subtracted Background Ha: %s", str(background_Ha))
    log.debug("Subtracted Background Va: %s", str(background_Va))
    print("Subtracted Background Ha: %s", str(background_Ha))
    print("Subtracted Background Va: %s", str(background_Va)) 


    """
    The APD's are connected to the tdc in the following order: Ha, Va, Vb, Hb
    -> thats why the data assignment is 0, 1, 3, 2
    """
    Ha = np.append(np.append(np.zeros(settings['Ha_offset']),
                   data[:, 0] - background_Ha),
                   np.zeros(big_offset - settings['Ha_offset']))
    Va = np.append(np.append(np.zeros(settings['Va_offset']),
                   data[:, 1] - background_Va),
                   np.zeros(big_offset - settings['Va_offset']))


    log.debug("Data shape: %s", np.shape(data))

    timeline = np.zeros((np.alen(Ha), 5))

    summe = (Ha/settings['detection_eff_Ha'] +
             Va/settings['detection_eff_Va'])


    time = np.arange(0, np.alen(Ha))
    timeline[:, 0] = time * settings['timeconst']
    timeline[:, 1] = Ha/settings['detection_eff_Ha']
    timeline[:, 2] = Va/settings['detection_eff_Va']


    return Ha/settings['detection_eff_Ha'], Va/settings['detection_eff_Va'], \
        summe, time, timeline



def parse_bin2histo(settings):
    """
    Function to parse the histogram files created from a binary file
    with bin2histo.py or directly via the qutau_measurement_script.py
    """
    log.debug("Begin Parsing")
    if ((settings['Ha_offset'] >= settings['Va_offset']) and
        (settings['Ha_offset'] >= settings['Hb_offset']) and
            (settings['Ha_offset'] >= settings['Vb_offset'])):
        big_offset = settings['Ha_offset']
    elif ((settings['Va_offset'] >= settings['Hb_offset']) and
            settings['Va_offset'] >= settings['Vb_offset']):
        big_offset = settings['Va_offset']
    elif (settings['Hb_offset'] >= settings['Vb_offset']):
        big_offset = settings['Hb_offset']
    else:
        big_offset = settings['Vb_offset']

    data = np.genfromtxt(settings['myfile'], comments='#', invalid_raise=True)

    """
    Sample the background of each channel and get the mean as a background to
    subtract from each channel
    """
    background_Ha = np.sum(data[8000: settings['background_sample_size'], 0]) / \
        settings['background_sample_size']
    background_Va = np.sum(data[8000: settings['background_sample_size'], 1]) / \
        settings['background_sample_size']
#    background_Hb = np.sum(data[: settings['background_sample_size'], 3]) / \
#        settings['background_sample_size']
#    background_Vb = np.sum(data[: settings['background_sample_size'], 2]) / \
#        settings['background_sample_size']

    log.debug("Subtracted Background Ha: %s", str(background_Ha))
    log.debug("Subtracted Background Va: %s", str(background_Va))
#    log.debug("Subtracted Background Hb: %s", str(background_Hb))
#    log.debug("Subtracted Background Vb: %s", str(background_Vb))

    """
    The APD's are connected to the tdc in the following order: Ha, Va, Vb, Hb
    -> thats why the data assignment is 0, 1, 3, 2
    """
    Ha = np.append(np.append(np.zeros(settings['Ha_offset']),
                   data[:, 0] - background_Ha),
                   np.zeros(big_offset - settings['Ha_offset']))
    Va = np.append(np.append(np.zeros(settings['Va_offset']),
                   data[:, 1] - background_Va),
                   np.zeros(big_offset - settings['Va_offset']))
#    Hb = np.append(np.append(np.zeros(settings['Hb_offset']),
#                   data[:, 3] - background_Hb),
#                   np.zeros(big_offset - settings['Hb_offset']))
#    Vb = np.append(np.append(np.zeros(settings['Vb_offset']),
#                   data[:, 2] - background_Vb),
#                   np.zeros(big_offset - settings['Vb_offset']))

    log.debug("Data shape: %s", np.shape(data))

    timeline = np.zeros((np.alen(Ha), 5))

    summe = (Ha/settings['detection_eff_Ha'] +
             Va/settings['detection_eff_Va'])
#             +
#             Hb/settings['detection_eff_Hb'] +
#             Vb/settings['detection_eff_Vb'])

#print settings['timeconst']
#print np.alen(Ha)

    time = np.arange(0, np.alen(Ha))
    timeline[:, 0] = time * settings['timeconst']
    timeline[:, 1] = Ha/settings['detection_eff_Ha']
    timeline[:, 2] = Va/settings['detection_eff_Va']
#    timeline[:, 3] = Hb/settings['detection_eff_Hb']
#    timeline[:, 4] = Vb/settings['detection_eff_Vb']

    return Ha/settings['detection_eff_Ha'], Va/settings['detection_eff_Va'], \
        summe, time, timeline
#        Hb/settings['detection_eff_Hb'], Vb/settings['detection_eff_Vb'], \


def parse_old_tdc_file(settings):
    """
    Function to parse histogram files from the old tdc
    """
    log.debug("Begin Parsing")
    if ((settings['Ha_offset'] >= settings['Va_offset']) and
        (settings['Ha_offset'] >= settings['Hb_offset']) and
            (settings['Ha_offset'] >= settings['Vb_offset'])):
        big_offset = settings['Ha_offset']
    elif ((settings['Va_offset'] >= settings['Hb_offset']) and
            settings['Va_offset'] >= settings['Vb_offset']):
        big_offset = settings['Va_offset']
    elif (settings['Hb_offset'] >= settings['Vb_offset']):
        big_offset = settings['Hb_offset']
    else:
        big_offset = settings['Vb_offset']

    with open(settings['myfile'], 'r') as f:
        lines = f.readlines(10)
        f.close

    params = lines[7]
    params = params.split()
    settings['timeconst'] = float(params[4])   # usually 82 ps = 82e-12 seconds

    data = np.genfromtxt(settings['myfile'], comments='#', invalid_raise=True)
    log.debug("Data shape: %s", np.shape(data))

    """
    Sample the background of each channel and get the mean as a background to
    subtract from each channel
    """
    background_Ha = np.sum(data[:settings['background_sample_size'], 0]) / \
        settings['background_sample_size']
    background_Va = np.sum(data[:settings['background_sample_size'], 1]) / \
        settings['background_sample_size']
    background_Hb = np.sum(data[:settings['background_sample_size'], 2]) / \
        settings['background_sample_size']
    background_Vb = np.sum(data[:settings['background_sample_size'], 3]) / \
        settings['background_sample_size']

    log.debug("Subtracted Background Ha: %s", str(background_Ha))
    log.debug("Subtracted Background Va: %s", str(background_Va))
    log.debug("Subtracted Background Hb: %s", str(background_Hb))
    log.debug("Subtracted Background Vb: %s", str(background_Vb))

    H_a = np.append(np.append(np.zeros(settings['Ha_offset']),
                              data[:, 0]-background_Ha),
                    np.zeros(big_offset - settings['Ha_offset']))
    V_a = np.append(np.append(np.zeros(settings['Va_offset']),
                              data[:, 1]-background_Va),
                    np.zeros(big_offset - settings['Va_offset']))
    H_b = np.append(np.append(np.zeros(settings['Hb_offset']),
                              data[:, 2]-background_Hb),
                    np.zeros(big_offset - settings['Hb_offset']))
    V_b = np.append(np.append(np.zeros(settings['Vb_offset']),
                              data[:, 3]-background_Vb),
                    np.zeros(big_offset - settings['Vb_offset']))

    Ha = np.zeros(np.alen(H_a)/2+1)
    Va = np.zeros(np.alen(H_a)/2+1)
    Hb = np.zeros(np.alen(H_a)/2+1)
    Vb = np.zeros(np.alen(H_a)/2+1)
    for i in np.arange(0, np.alen(H_a)/2, 1):
    # summation of two neighboring bins to smooth the graph
        Hb[i] = (H_b[2*i]+H_b[2*i+1])/settings['detection_eff_Hb']
        Va[i] = (V_a[2*i]+V_a[2*i+1])/settings['detection_eff_Va']
        Ha[i] = (H_a[2*i]+H_a[2*i+1])/settings['detection_eff_Ha']
        Vb[i] = (V_b[2*i]+V_b[2*i+1])/settings['detection_eff_Vb']

    timeline = np.zeros((np.alen(Ha), 5))

    summe = (Ha/settings['detection_eff_Ha'] +
             Va/settings['detection_eff_Va'] +
             Hb/settings['detection_eff_Hb'] +
             Vb/settings['detection_eff_Vb'])

    log.debug("Timeconst: %s", settings['timeconst'])
    log.debug("Length of Ha: %s", np.alen(Ha))

    time = np.arange(0, np.alen(Ha))
    timeline[:, 0] = time * settings['timeconst'] * 2
    # double the time scaling due to graph smoothing
    timeline[:, 1] = Ha
    timeline[:, 2] = Va
    timeline[:, 3] = Hb
    timeline[:, 4] = Vb

    return Ha, Va, Hb, Vb, summe, time, timeline


def generate_grid_and_bins(settings, Ha, Va, summe):
    """
    Function to generate the grid and the bins from the data
    @ by Sonja (29.4.2014) don't allow negative values! take (max(val,0))
    """

    if settings['walk_2d'] > 0:
        grid = generate_2D_grid(settings)
    else:
        grid = generate_grid(settings)

#    bins = sum_over_grid(grid, settings, summe)
#    ind = np.where(bins < 0)[0]
#    bins[ind] = 0
#    print '-------------------------ha--------------------'
    Ha_bins = sum_over_grid(grid, settings, Ha)
    ind_Ha = np.where(Ha_bins < 0)[0]
    Ha_bins[ind_Ha] = 0
#    print '-------------------------Va--------------------'
    Va_bins = sum_over_grid(grid, settings, Va)
    ind_Va = np.where(Va_bins < 0)[0]
    Va_bins[ind_Va] = 0
#    print '-------------------------hb--------------------'
#    Hb_bins = sum_over_grid(grid, settings, Hb)
#    ind_Hb = np.where(Hb_bins < 0)[0]
#    Hb_bins[ind_Hb] = 0
##    print '-------------------------vb--------------------'
#    Vb_bins = sum_over_grid(grid, settings, Vb)
#    ind_Vb = np.where(Vb_bins < 0)[0]
#    Vb_bins[ind_Vb] = 0
    bins = Ha_bins + Va_bins 
#    bins = Ha_bins + Va_bins + Vb_bins + Hb_bins  # set bins as sum over all
#    plt.figure()
#    plt.plot(bins, '-x', color = 'k')
#    plt.plot(Ha_bins, '-x', color = 'r')
#    plt.plot(Va_bins, '-x', color = '#EEB422')
#    plt.plot(Hb_bins, '-x', color = 'b')
#    plt.plot(Vb_bins, '-x', color = 'g')
#    plt.show()
    return bins, Ha_bins, Va_bins, grid


def generate_2D_barplots(bins, settings):
    """
    Function to generate 2D barplots, if the setup was run in a true 2D quantum
    walk scheme
    """
    if settings['steps_to_plot'] > 0:

        step = generate_2D_steps(bins, settings)

        for plot_step_no in np.arange(settings['steps_to_plot'],
                                      settings['max_steps']):
            print "Plotting step: "+str(plot_step_no)

            # Finally, for all quantum walkers, Python does feature a 3d-bar
            # plotting tool. Let's see how it works.
            # First of all, we need a coarser grid...
            xn = np.linspace(-plot_step_no, plot_step_no, plot_step_no+1)
            yn = np.linspace(-plot_step_no, plot_step_no, plot_step_no+1)
            Xn, Yn = np.meshgrid(xn, yn)
            dxn = abs(xn[0] - xn[1])
            dyn = abs(yn[0] - yn[1])

            # Now the bar3d function is a bit particular, in that it takes only
            # 1d arrays as arguments, so we will generate them from the 2d
            # arrays
            xpos = Xn.flatten()
            ypos = Yn.flatten()
            dz = step[plot_step_no][0]

            norm_factor = np.sum(dz)
            z_values = dz/norm_factor
#            zmin = np.min(z_values)
#            zmax = np.max(z_values)

            # For the color coding, we need to compensate for negative values.
            # Again, this is a particular problem of bar3d -.-
            #offset = dz + np.abs(zmin)
            colors = cm.jet(z_values)
            # z_values/max(z_values) for independend colormaps,
            # only z_values for global
            fig = plt.figure(figsize=plt.figaspect(1.0) * 1.0)
            ax = fig.gca(projection='3d')  # the 3d-plot axes
            ax.bar3d(xpos, ypos, np.zeros(len(dz)), 0.5*dxn, 0.5*dyn, z_values,
                     color=colors)
                # Brr, this looks nasty! There are six (!) mandatory arguments.
                # The first three define the positions of the bottoms of the
                # bars. We want them all to be at the z=0 plane, so we give an
                # array of zeros as third argument. The first two are x- and
                # y-positions. Then, we need to define the width of the bars.
                # For this we use the calculated step-sizes of our grid and
                # make the bars a bit smaller. We don't want them to touch each
                # other. Finally, the sixth argument is the actual z-values.
                # Phew! Let's hope it works ;o)
            ax.set_xlabel('X position')
            ax.set_ylabel('Y position')
            ax.set_zlabel('Intensity [a.u.]')
            ax.set_title(r'Step' + str(plot_step_no))
          #  ax.set_zlim(zmin, zmax)
#            ftitle = 'fig_' + str(plot_step_no) + '_bars.png'
            plt.show()
           # plt.clf()


def generate_1D_barplots(bins, settings, Ha_bins, Va_bins, norm=1):
    """
    Function to generate the 1D bar charts (measurement and theory)
    """

    if settings['steps_to_plot'] > 0:
        step = generate_steps(bins, settings)
        Ha_step = generate_steps(Ha_bins, settings)
        Va_step = generate_steps(Va_bins, settings)


        HWP_alpha = settings['HWP_alpha']/360.*2*np.pi
                     # conversion to rad
        beta = settings['beta']/360.*2*np.pi
                 # conversion to rad
        QWP_alpha = settings['QWP_alpha']/360.*2*np.pi
                 # conversion to rad
        similarities = []
        distances = []
        hor_pol = []
#        theoh_pol = []
        ver_pol = []
#        theov_pol = []
        sum_pol = []
        hor_steps = []
        ver_steps = []
        theoh_steps = []
        theov_steps = []
        zero_array=[]       #Array containing the probability to find the walker at pos 0 for the steps investigated
        zero_array_theo=[]
        zero_array_theo_err=[]

        for plot_step_no in np.arange(settings['steps_to_plot'],
                                      settings['max_steps']):
            zero_pos=0          #Probability to find the walker at pos 0 in the current step
            zero_pos_theo=0
            
            if settings['plot'] == 1:
                fig = plt.figure()
    #            width = 0.5
                ax = fig.add_subplot(111)
                plt.title(r'Step' + str(plot_step_no) + " File " +
                          str(settings['praefix']))
                ax.title.set_y(1.02)
                plt.xlabel(r'Position')
                plt.ylabel(r'peak height [a.u.]')

            plot_step = step[plot_step_no][0]
            Ha_plot_step = Ha_step[plot_step_no][0]
            Va_plot_step = Va_step[plot_step_no][0]


            if settings['split_step'] == 1 and plot_step_no % 2 == 1:
                """
               we are in B-mode in the split step scheme -> count B-detectors
               and set plot_step as the sum over both
                """
                Ha_plot_step = Vb_plot_step
                Va_plot_step = Hb_plot_step
                plot_step = Ha_plot_step + Va_plot_step
            if settings['split_step'] == 1 and plot_step_no % 2 == 0:
                """
               we are in A-mode in the split step scheme -> count A-detectors
               and set plot_step as the sum over both
                """
                Ha_plot_step = Ha_plot_step
                Va_plot_step = Va_plot_step
                plot_step = Ha_plot_step + Va_plot_step
                

            log.debug("Now generating theory for step: %s", str(plot_step_no))
            print 'Now generating theory for step: %s', str(plot_step_no)
            psi = theo.generate_initial_state(settings, plot_step_no)
            psi_HV, psi_DA, psi_RL = theo.time_evolution(settings, psi,
                                                         plot_step_no, 0, 0)
            if settings['sinks']==1:
                     psi_HV_sink, psi_DA_sink, psi_RL_sink = theo.time_evolution(settings, psi,
                                                         plot_step_no, 1, 0)                                        
            print 'HWP alpha'
            print HWP_alpha

            if settings['tomography'] == 'HV':
                psi = psi_HV
                if settings['sinks']==1:
                    psi_sink=psi_HV_sink
            elif settings['tomography'] == 'DA':
                psi = psi_DA
                if settings['sinks']==1:
                    psi_sink=psi_DA_sink
            elif settings['tomography'] == 'RL':
                psi = psi_RL
                if settings['sinks']==1:
                    psi_sink=psi_RL_sink

            else:
                log.warn('something wrong with tomography?!')
                break
            if settings['split_step'] == 1 and plot_step_no % 2 == 0:
                psi = psi_HV    # tomography coins ONLY in B-mode (odd steps)
#            print "Settings after theory: "
#            pp.pprint(settings)
            vertical = (psi[2*plot_step_no+1:] *
                        psi[2*plot_step_no+1:].conj() /
                        np.sum(psi[:] * psi[:].conj()))
            if settings['sinks']==1:           
                vertical_sink = (psi_sink[2*plot_step_no+1:] *
                        psi_sink[2*plot_step_no+1:].conj() /
                        np.sum(psi[:] * psi[:].conj()))


#            print 'psi:' + str( np.around(psi,3))
            horizontal = (psi[:2*plot_step_no+1] *
                          psi[:2*plot_step_no+1].conj() / np.sum(psi[:] *
                          psi[:].conj()))
            if settings['sinks']==1:              
                horizontal_sink = (psi_sink[:2*plot_step_no+1] *
                          psi_sink[:2*plot_step_no+1].conj() / np.sum(psi[:] *
                          psi[:].conj()))
                          
            if (settings['recurrence_restart']==1 and plot_step_no % 2 ==0) or settings['recurrence_continue']==1:          #Determining the prob for pos 0 in the current step
#                print 'exp h'
#                print Ha_plot_step
#                print 'theo h'
#                print horizontal
#                print 'theo h with sink'
#                print horizontal_sink
##                print 'exp v'
##                print Va_plot_step
#                print 'theo v'
#                print vertical
#                print 'theo v with sink'
#                print vertical_sink
#                print 'plot_step_no'
#                print plot_step_no
#                print 'exp pos'
#                print Ha_plot_step[plot_step_no/2]
#                print 'theo pos'
#                print vertical[plot_step_no/2+1]
                zero_pos+=Ha_plot_step[plot_step_no/2]+Va_plot_step[plot_step_no/2]
                zero_pos=zero_pos/(np.sum(Ha_plot_step[:])+np.sum(Va_plot_step[:]))
                
                if settings['sinks']==1:
                    zero_pos_theo+=(horizontal_sink[plot_step_no]+vertical_sink[plot_step_no])
                    #/(np.sum(horizontal[:])+np.sum(vertical[:]))
                else:
                    zero_pos_theo+=(horizontal[plot_step_no]+vertical[plot_step_no])
                    #/(np.sum(horizontal[:])+np.sum(vertical[:]))
                
 #               zero_pos_theo=zero_pos_theo/(np.sum(horizontal[:])+np.sum(vertical[:]))
                
                print 'norm zero prob'
                
                print zero_pos_theo
                
                zero_array.append(zero_pos)
                zero_array_theo.append(zero_pos_theo)
            
# Using different normalization for bar charts
            
            if settings['sinks']==1:
                vertical_sink_ref=vertical_sink
                vertical_sink = (psi_sink[2*plot_step_no+1:] *
                        psi_sink[2*plot_step_no+1:].conj() /
                        np.sum(psi_sink[:] * psi_sink[:].conj()))


            if settings['sinks']==1: 
                horizontal_sink_ref=horizontal_sink
                horizontal_sink = (psi_sink[:2*plot_step_no+1] *
                          psi_sink[:2*plot_step_no+1].conj() / np.sum(psi_sink[:] *
                          psi_sink[:].conj()))


             # Generating errorbars for the numerical values
            if settings['numerical_error_bars'] == 1:


                h_err_theo = generate_monte_carlo_errorbars(settings, 0, plot_step_no, horizontal)

                v_err_theo = generate_monte_carlo_errorbars(settings, 1, plot_step_no, vertical)
                    
                
                if (settings['recurrence_restart']==1 and plot_step_no % 2 ==0) or settings['recurrence_continue']==1:
                
                    if settings['sinks']==1:
                        
                        h_err_theo_sink = generate_monte_carlo_errorbars(settings, 0, plot_step_no, horizontal_sink_ref)
    
                        v_err_theo_sink = generate_monte_carlo_errorbars(settings, 1, plot_step_no, vertical_sink_ref)
                        
                    if settings['sinks']==1:
                        zero_pos_theo_err=np.sqrt(h_err_theo_sink[plot_step_no]**2+v_err_theo_sink[plot_step_no]**2)
                        #/(np.sum(horizontal[:])+np.sum(vertical[:]))
                    else:
                        zero_pos_theo_err=np.sqrt(h_err_theo[plot_step_no]**2+v_err_theo[plot_step_no]**2)
                        
                    zero_array_theo_err.append(zero_pos_theo_err)
                    
            else:
                
                zero_array_theo_err.append(0)
                

#            sum_theory = horizontal + vertical


            if settings['plot'] == 1 :

                if settings['numerical_error_bars'] == 1:
                    
                    if settings['sinks']==1:
                        
                        ax.bar(np.arange(-(plot_step_no+0.5), (plot_step_no+0.1), 1),
                           (horizontal_sink[::-1]), width=0.5, color='#DF013A',
                           align='center', label='Hor_numeric with sinks',yerr=h_err_theo_sink, error_kw=dict(barsabove=True, capthick=1,
                            elinewidth=1), ecolor='#000000', capsize=10)
                        ax.bar(np.arange(-(plot_step_no+0.4), (plot_step_no+0.1), 1),
                           vertical_sink[::-1], width=0.5, color='#013ADF',
                           align='center', label='Ver_numeric with sinks', yerr=v_err_theo_sink, error_kw=dict(barsabove=True, capthick=1,
                           elinewidth=1), ecolor='#000000', capsize=10)
                           
                    else:
                    
                        ax.bar(np.arange(-(plot_step_no+0.5), (plot_step_no+0.1), 1),
                               (horizontal[::-1]), width=0.5, color='#DF013A',
                               align='center', label='Hor_numeric',yerr=h_err_theo, error_kw=dict(barsabove=True, capthick=1,
                                elinewidth=1), ecolor='#000000', capsize=10)
                        ax.bar(np.arange(-(plot_step_no+0.4), (plot_step_no+0.1), 1),
                               vertical[::-1], width=0.5, color='#013ADF',
                               align='center', label='Ver_numeric', yerr=v_err_theo, error_kw=dict(barsabove=True, capthick=1,
                               elinewidth=1), ecolor='#000000', capsize=10)
                           
                elif settings['sinks']==1:
                        ax.bar(np.arange(-(plot_step_no+0.5), (plot_step_no+0.1), 1),
                               (horizontal_sink[::-1]), width=0.5, color='#DF013A',
                               align='center', label='Hor_numeric with sinks')
                        ax.bar(np.arange(-(plot_step_no+0.4), (plot_step_no+0.1), 1),
                               vertical_sink[::-1], width=0.5, color='#013ADF',
                               align='center', label='Ver_numeric with sinks')

                else:

                    ax.bar(np.arange(-(plot_step_no+0.5), (plot_step_no+0.1), 1),
                           (horizontal[::-1]), width=0.5, color='#DF013A',
                           align='center', label='Hor_numeric')
                    ax.bar(np.arange(-(plot_step_no+0.4), (plot_step_no+0.1), 1),
                           vertical[::-1], width=0.5, color='#013ADF',
                           align='center', label='Ver_numeric')
                           




            h_err_exp = generate_errorbars(settings, Ha_plot_step[:],
                                       np.sum(plot_step[:]))

#            h_err_exp = 0.1


            v_err_exp = generate_errorbars(settings, Va_plot_step[:],
                                       np.sum(plot_step[:]))

#            v_err_exp = 0.1
            if settings['plot'] == 1:
                ax.bar(np.arange(-(plot_step_no+0.2), (plot_step_no+0.1), 2),
                       (Ha_plot_step[:])/np.sum(plot_step[:]), width=0.5,
                       color='#FF8000', align='center', label='Hor_exp',
                       yerr=h_err_exp, error_kw=dict(barsabove=True, capthick=1,
                                                 elinewidth=1),
                       ecolor='#000000', capsize=10)
                ax.bar(np.arange(-(plot_step_no+0.1), (plot_step_no+0.1), 2),
                       (Va_plot_step[:])/np.sum(plot_step[:]), width=0.5,
                       color='#00BFFF', align='center', label='Ver_exp',
                       yerr=v_err_exp, error_kw=dict(barsabove=True, capthick=1,
                                                 elinewidth=1),
                       ecolor='#000000', capsize=10)
    #            ax.bar(np.arange(-(plot_step_no),(plot_step_no+0.1),2),
    #                (plot_step[:])/np.sum(plot_step[:]), width=0.5, color='g',
    #                   align='center', label='Data')
                plt.legend()

            # output for percolation analysis
            hor_pol.append(np.sum(Ha_plot_step))
#            theoh_pol.append(np.sum(horizontal))
            ver_pol.append(np.sum(Va_plot_step))
#            theov_pol.append(np.sum(vertical))
            sum_pol.append(np.sum(plot_step[:]))
            if settings['percolation'] == 1:
                print "Percolation output for step " + str(plot_step_no)
                print "Horizontal Polarisation: " + str(np.sum(Ha_plot_step) /
                                                        np.sum(plot_step[:]))

                print "Horizontal Polarisation (theory): " + \
                    str(np.sum(horizontal))
                print "Vertical Polarisation: " + str(np.sum(Va_plot_step) /
                                                      np.sum(plot_step[:]))
                print "Vertical Polarisation (theory): " + \
                    str(np.sum(vertical))

            # calculates similarity between Exp and Theo
            sim = calc_Similarity(horizontal[::-1], vertical[::-1],
                                  (Ha_plot_step[:])/np.sum(plot_step[:]),
                                  (Va_plot_step[:])/np.sum(plot_step[:]))
            log.debug("Similarity in step %s is %s", str(plot_step_no),
                      str(sim))
            similarities.append(sim)
             # calculates distance between Exp and Theo
            dist = calc_distance(horizontal[::-1], vertical[::-1],
                                 (Ha_plot_step[:])/np.sum(plot_step[:]),
                                 (Va_plot_step[:])/np.sum(plot_step[:]))
            log.debug("Distance in step %s is %s\n", str(plot_step_no),
                      str(dist))
            distances.append(dist)

            hor_steps.append((Ha_plot_step[:])/np.sum(plot_step[:]))
            ver_steps.append((Va_plot_step[:])/np.sum(plot_step[:]))

            theoh_steps.append(horizontal[::-1])
            theov_steps.append(vertical[::-1])
        log.debug("Mean similarity from step %s to step %s is: %s",
                 str(settings['steps_to_plot']), str(settings['max_steps']-1),
                 str(np.mean(similarities)))
        log.info("Mean distance from step %s to %s is: %s",
                 str(settings['steps_to_plot']), str(settings['max_steps']-1),
                 str(np.mean(distances)))

#        save_distance=[]
#
#        save_distance.append(np.mean(distances))
#
#        print save_distance
#
#        distance_name = settings['path'] + '/'  + 'distance' \
#        + '.txt'
#        try:
#            np.savetxt(distance_name, save_distance, delimiter=',  ', fmt='%1.5f')
#            log.info("Successfully written the data into %s", distance_name)
#        except:
#            log.warn("Could not open file %s for writing.", distance_name)
#            sys.exit(1)

        if settings['savedata2file'] == 1:
            log.info("Saving data to file")
            save2file(settings, hor_steps, ver_steps)
        if settings['savetheo2file'] == 1:
            log.info("Saving theory to file")
            savetheo2file(settings, theoh_steps, theov_steps,
                          append='_sim')
        print 'raw zero_array_theo_err'
        print zero_array_theo_err                 
        return hor_pol, ver_pol, sum_pol, hor_steps, ver_steps, zero_array, zero_array_theo, zero_array_theo_err


def generate_errorbars(settings, counts, total_counts):
    """
    Function to generate the errorbars for a bar chart
    """
    error = []
    log.debug("Total counts: %s", str(total_counts))
    for idx, C_x0 in enumerate(counts):
#        print "Index: "+str(idx)+" Counts: "+str(C_x0)
        result = []
        for idx2, bar in enumerate(counts):
#            print "Inner Index: "+str(idx2)+" Counts: "+str(bar)
            if idx == idx2:
                derivative = (total_counts - bar) / \
                    total_counts**2
            else:
                derivative = bar / total_counts**2
#            print "Derivative: "+str(derivative)
            result.append((derivative * np.sqrt(abs(bar)))**2)
        error.append(1/2. * np.sqrt(np.sum(result)))
    log.debug("Error: %s", str(error))

#    kater_name = settings['path'] + '/'  + 'kater' \
#        + '.txt'
#    try:
#        np.savetxt(kater_name, error, delimiter=',  ', fmt='%1.5f')
#        log.info("Successfully written the data into %s", kater_name)
#    except:
#        log.warn("Could not open file %s for writing.", kater_name)
#        sys.exit(1)

#    print error
    return error


def generate_monte_carlo_errorbars(settings, pol, plot_step_no, reference):
    """
    Function to generate the errorbars for a bar chart via a Monte Carlo Simulation

    """

    # initializing parameters and arrays


    carlo_error = np.zeros(reference.size)
#    print reference
#    print reference.size


    for a in [-1, 1]:   # loop over HHa coupl
        for b in [-1, 1]:  # loop over HVa coupl
            for c in [-1, 1]:  # loop over VVa coupl
                for d in [-1, 1]:  # loop over VHa coupl
#                    log.info("Progress : %s %s %s %s", a, b, c, d)
                    for e in [-1, 1]:  # loop over HHb coupl
                        for f in [-1, 1]:  # loop over HVb coupl
                            for g in [-1, 1]:  # loop over VVb coupl
                                for h in [-1, 1]:  # loop over VHb coupl
                                    for i in [-1, 1]:  # loop over Coin
                                        for j in [-1, 1]:  # loop over Transm
                                            for k in [-1, 1]:  # loop over Sink Transm
                                                t0 = clock()
#                                                # actualize settings
                                                tmp_HHa = settings['HHa']
                                                settings['HHa'] = tmp_HHa + a * settings['coupl_error']
                                                tmp_HVa = settings['HVa']
                                                settings['HVa'] = tmp_HVa + b * settings['coupl_error']
                                                tmp_VVa = settings['VVa']
                                                settings['VVa'] = tmp_VVa + c * settings['coupl_error']
                                                tmp_VHa = settings['VHa']
                                                settings['VHa'] = tmp_VHa + d * settings['coupl_error']
                                                tmp_HHb = settings['HHb']
                                                settings['HHb'] = tmp_HHb + e * settings['coupl_error']
                                                tmp_HVb = settings['HVb']
                                                settings['HVb'] = tmp_HVb + f * settings['coupl_error']
                                                tmp_VVb = settings['VVb']
                                                settings['VVb'] = tmp_VVb + g * settings['coupl_error']
                                                tmp_VHb = settings['VHb']
                                                settings['VHb'] = tmp_VHb + h * settings['coupl_error']
                                                tmp_Coin = settings['alpha']
                                                settings['alpha'] = tmp_Coin + i * settings['coin_error']
                                                tmp_t_coef = settings['t_coef']
                                                settings['t_coef'] = tmp_t_coef + j * settings['trans_error']
                                                tmp_s_trans = settings['sink_transmission'] 
                                                settings['sink_transmission'] = tmp_s_trans + k * settings['sink_error']
                                                if settings['t_coef'] > 1:   # don't allow irrealistic values
                                                    settings['t_coef'] = 1

                                                # calculate theory values with new Settings

                                                psi = theo.generate_initial_state(settings, plot_step_no)
                                                psi_HV, psi_DA, psi_RL = theo.time_evolution(settings, psi,
                                                                                             plot_step_no, 0, 0)
                                                                                             
                                                                                             
                                                if settings['sinks']==1:
                                                         psi_HV_sink, psi_DA_sink, psi_RL_sink = theo.time_evolution(settings, psi,
                                                                                             plot_step_no, 1, 0)                                                                                                                  
                                                
                                                if settings['tomography'] == 'HV':
                                                    psi = psi_HV
                                                    if settings['sinks']==1:
                                                        psi_sink=psi_HV_sink
                                                elif settings['tomography'] == 'DA':
                                                    psi = psi_DA
                                                    if settings['sinks']==1:
                                                         psi_sink=psi_DA_sink
                                                elif settings['tomography'] == 'RL':
                                                    psi = psi_RL
                                                    if settings['sinks']==1:
                                                        psi_sink=psi_RL_sink 
                                                else:
                                                    log.warn('something wrong with tomography?!')
                                                    break
                                                if settings['split_step'] == 1 and plot_step_num % 2 == 0:
                                                    psi = psi_HV    # tomography coins ONLY in B-mode (odd steps)
                                    #            print "Settings after theory: "
                                    #            pp.pprint(settings)

                                                            
                                                if settings['sinks']==1:           
                                                    vertical = (psi_sink[2*plot_step_no+1:] *
                                                            psi_sink[2*plot_step_no+1:].conj() /
                                                            np.sum(psi[:] * psi[:].conj()))
                                                else:                     
                                                    vertical = (psi[2*plot_step_no+1:] *
                                                            psi[2*plot_step_no+1:].conj() /
                                                            np.sum(psi[:] * psi[:].conj()))
                                                                                                                       

                                                              
                                                if settings['sinks']==1:              
                                                    horizontal = (psi_sink[:2*plot_step_no+1] *
                                                              psi_sink[:2*plot_step_no+1].conj() / np.sum(psi[:] *
                                                              psi[:].conj()))
                                                else:
                                                    horizontal = (psi[:2*plot_step_no+1] *
                                                              psi[:2*plot_step_no+1].conj() / np.sum(psi[:] *
                                                              psi[:].conj()))


                                                # calculate difference between reference and new theory values

                                                if pol == 1:

                                                     for i, ref in enumerate(reference):
                                                         diff_ver = abs(ref - vertical[i])

                                                         if diff_ver > carlo_error[i]:

                                                             carlo_error[i] = diff_ver



                                                if pol == 0:

                                                   for i, ref in enumerate(reference):
                                                       diff_hor = abs(ref - horizontal[i])


                                                       if diff_hor > carlo_error[i]:

                                                             carlo_error[i] = diff_hor


                                                # reset on original value in settings
                                                settings['HHa'] = tmp_HHa
                                                settings['HVa'] = tmp_HVa
                                                settings['VVa'] = tmp_VVa
                                                settings['VHa'] = tmp_VHa
                                                settings['HHb'] = tmp_HHb
                                                settings['HVb'] = tmp_HVb
                                                settings['VVb'] = tmp_VVb
                                                settings['VHb'] = tmp_VHb
                                                settings['alpha'] = tmp_Coin
                                                settings['t_coef'] = tmp_t_coef
                                                settings['sink_transmission'] = tmp_s_trans 
                                                
#                                                print str(clock()-t0)


#    carlo_name = settings['path'] + '/'  + 'carlo' \
#        + '.txt'
#    try:
#        np.savetxt(carlo_name, carlo_error, delimiter=',  ', fmt='%1.5f')
#        log.info("Successfully written the data into %s", carlo_name)
#    except:
#        log.warn("Could not open file %s for writing.", carlo_name)
#        sys.exit(1)
#
    carlo_error = carlo_error[::-1]             # Reverse array to adapt it to loony theory conventions
    return carlo_error



def plot_timelines(time, summe, timeline, settings, grid):
    """
    Function to plot the timeline histograms of the four detectors
    """

    fig = plt.figure()
    ax = fig.add_subplot(111)

    yfm = ax.yaxis.get_major_formatter()
    yfm.set_powerlimits([-3, 3])
    xfm = ax.xaxis.get_major_formatter()
    xfm.set_powerlimits([-3, 3])

    if settings['display_grid'] == 1:
        plt.title(r'File ' + str(settings['praefix']))
        ax.title.set_y(1.02)
        #ax.set_yscale('log')
        plt.plot(time, summe, ':', label='Sum', linewidth=0.5)
        # time, H_a, '#EEB422'  , time, Hb,'b', time, Vb, 'g')
        #a[:-1,:] to subtract the first row of zeros to initialize a
        plt.plot(time, timeline[:, 1], 'r', label='Ha')
        plt.plot(time, timeline[:, 2], 'k', label='Va')
#        plt.plot(time, timeline[:, 3], 'b', label='Hb')
#        plt.plot(time, timeline[:, 4], 'g', label='Vb')
        plt.xlabel(r'time [timebins] ')
        for pos in grid:
#            print "Printing even blue grid"
            plt.axvspan(pos-(settings['peak_width'] / 2.),
                        pos+(settings['peak_width'] / 2.),
                        color='b', alpha=settings['alpha_blue_grid'])

    else:
        plt.title(r'File ' + str(settings['praefix']))
        ax.title.set_y(1.02)
        #ax.set_yscale('log')
        plt.plot(timeline[:, 0], summe, ':', label='Sum', linewidth=0.5)
        # time, H_a, '#EEB422'  , time, Hb,'b', time, Vb, 'g')
        #a[:-1,:] to subtract the first row of zeros to initialize a
        plt.plot(timeline[:, 0], timeline[:, 1], 'r', label='Ha')
        plt.plot(timeline[:, 0], timeline[:, 2], 'k', label='Va')
#        plt.plot(timeline[:, 0], timeline[:, 3], 'b', label='Hb')
#        plt.plot(timeline[:, 0], timeline[:, 4], 'g', label='Vb')
        plt.xlabel(r'time [seconds] ')
    plt.legend()
    plt.show()


def calc_Similarity(HTheo, VTheo, HExp, VExp):
    """
    Function to calculate the Similarity between Experimental Data and Theory
    """
    HTheo_val = HTheo[::2]      # consider only populated positions of the step
    VTheo_val = VTheo[::2]
    minus_indH = np.where(HExp < 0)     # set negative values to zero
    HExp[minus_indH] = 0
    minus_indV = np.where(VExp < 0)
    VExp[minus_indV] = 0
    HExpn = HExp/np.sum([HExp, VExp])
    VExpn = VExp/np.sum([HExp, VExp])    # new normalization due to zero values
    Sim = (abs(np.sum(np.sqrt(HTheo_val*HExpn)+np.sqrt(VTheo_val*VExpn))))**2
    return Sim


def calc_distance(HTheo, VTheo, HExp, VExp):
    """
    Function to calculate the Distance between Experimental Data and Theory
    """
    HTheo_val = HTheo[::2]      # consider only populated positions of the step
    VTheo_val = VTheo[::2]
    minus_indH = np.where(HExp < 0)     # set negative values to zero
    HExp[minus_indH] = 0
    minus_indV = np.where(VExp < 0)
    VExp[minus_indV] = 0
    HExpn = HExp/np.sum([HExp, VExp])
    VExpn = VExp/np.sum([HExp, VExp])    # new normalization due to zero values
    dist = 1/2. * np.sum(np.abs(HTheo_val-HExpn) + np.abs(VTheo_val - VExpn))
    return dist


def save2file(settings, HExp, VExp, append=''):
    """
    Function to save evaluated Data into a file for later plotting
    """
    # construct array for saving from bar chart data
    arr0 = np.zeros((settings['max_steps'], (settings['max_steps']-1)*2+1))
    arr0[0, settings['max_steps'] - 1] = 1   # fill with initial pulse
    for ind in np.arange(1, settings['max_steps']):  # fill other steps
        arr0[ind, settings['max_steps'] - 1 - ind:
             settings['max_steps'] + ind:2] = HExp[ind - 1] + VExp[ind - 1]
   # construct filename from path and präfix
    name, dum = os.path.splitext(settings['praefix'])
    filename = settings['path'] + '/' + name + '_barchartdata_steps0to' \
        + str(settings['max_steps']-1) + append + '.txt'
    try:
        np.savetxt(filename, arr0, delimiter=',  ', fmt='%1.5f')
        log.info("Successfully written the data into %s", filename)
    except:
        log.warn("Could not open file %s for writing.", filename)
        sys.exit(1)


def savetheo2file(settings, HTheo, VTheo, append=''):
    """
    Function to save theory Data corresponding to experimental settings into
    file for later plotting
    """
    # construct array for saving from bar chart data
    arr0 = np.zeros((settings['max_steps'], (settings['max_steps']-1)*2+1))
    arr0[0, settings['max_steps'] - 1] = 1   # fill with initial pulse
    for ind in np.arange(1, settings['max_steps']):  # fill other steps
        arr0[ind, settings['max_steps'] - 1 - ind:
             settings['max_steps'] + ind:] = HTheo[ind - 1] + VTheo[ind - 1]
    # construct filename from path and präfix
    name, dum = os.path.splitext(settings['praefix'])
    filename = settings['path'] + '/' + name + '_barchartdata_steps0to' \
        + str(settings['max_steps']-1) + append + '.txt'
    try:
        np.savetxt(filename, arr0, delimiter=',  ', fmt='%1.5f')
        log.info("Successfully written the data into %s", filename)
    except:
        log.warn("Could not open file %s for writing.", filename)
        sys.exit(1)
        
def generate_sampling_plot(settings):
    
    count = 0
    zero_array=[]
    zero_array_theo=[]
    zero_array_theo_err=[]
    step_array=[]
    
#    print sorted(glob(os.path.join(settings['path'], '*.txt')))
    
    for plotfile in sorted(glob(os.path.join(settings['path'], '*.txt'))):
        
#        print 'plotfile'
#        
#        print plotfile

        settings['myfile']=plotfile
        
        settings['praefix']='Sampling Step' + str(count+2)
        
        settings['steps_to_plot']=count+2
        
        settings['max_steps']=count+3
        
        
        if settings['tdc'] == 'qutau':
                Ha, Va, summe, time, timeline = parse_qutau_tdc_file(settings)
        elif settings['tdc'] == 'bin2histo':
            Ha, Va, summe, time, timeline = parse_bin2histo(settings)
        elif settings['tdc'] == 'AIT':
            Ha, Va, summe, time, timeline = parse_new_exp(settings)
            if settings['recurrence_continue']==1:
                print 'this file'
                print sorted(glob(os.path.join(settings['path_no_sink'], '*.txt')))[count]
                settings['myfile']=sorted(glob(os.path.join(settings['path_no_sink'], '*.txt')))[count]
                Ha_no_sinks, Va_no_sinks, summe_no_sinks = parse_new_exp(settings)[0:3]
                Ha_bins_no_sinks, Va_bins_no_sinks=generate_grid_and_bins(settings, Ha_no_sinks, Va_no_sinks, summe_no_sinks)[1:3]
                        
        bins, Ha_bins, Va_bins, grid = \
            generate_grid_and_bins(settings, Ha, Va, summe)
        
    #        generate_grid_and_bins(settings, Ha, Va, Hb, Vb, summe)
        if settings['walk_2d'] > 0:
            if settings['recurrence_restart']==0 and settings['recurrence_continue']==0:
                generate_2D_barplots(bins, settings)
            else:
                print 'Not yet implemented, coming soon'
#        if settings['recurrence_restart']==1:
            
        elif settings['recurrence_restart']==1 or settings['recurrence_continue']==1:
            
            zero_pos, zero_pos_theo, zero_pos_theo_err=generate_1D_barplots(bins, settings, Ha_bins, Va_bins)[5:8]    
            print 'this'
            print zero_pos_theo_err   
            if settings['recurrence_continue']==1:
                Ha_step_no_sinks = generate_steps(Ha_bins_no_sinks, settings)
                Va_step_no_sinks = generate_steps(Va_bins_no_sinks, settings)
                Ha_plot_step_no_sinks = Ha_step_no_sinks[settings['steps_to_plot']][0]
                Va_plot_step_no_sinks = Va_step_no_sinks[settings['steps_to_plot']][0]
                Ha_step = generate_steps(Ha_bins, settings)
                Va_step = generate_steps(Va_bins, settings)
                Ha_plot_step = Ha_step[settings['steps_to_plot']][0]
                Va_plot_step = Va_step[settings['steps_to_plot']][0]
                zero_pos+=Ha_plot_step[settings['steps_to_plot']/2]+Va_plot_step[settings['steps_to_plot']/2]
                zero_pos=zero_pos/(np.sum(Ha_plot_step_no_sinks[:])+np.sum(Va_plot_step_no_sinks[:]))
            print 'zero_pos'
            print zero_pos
            print 'zero_pos_theo'
            print zero_pos_theo
            zero_array=np.concatenate((zero_array, zero_pos))
            zero_array_theo=np.concatenate((zero_array_theo, zero_pos_theo))
            zero_array_theo_err=np.concatenate((zero_array_theo_err, zero_pos_theo_err))
            
            if settings['steps_to_plot'] % 2==0 or settings['recurrence_continue']==1:
                step_array.append(settings['steps_to_plot'])

            
        else:
            generate_1D_barplots(bins, settings, Ha_bins, Va_bins)

            
        count+=1

        
    if settings['plot'] == 1:
        plot_timelines(time, summe, timeline, settings, grid)
        
    if settings['recurrence_restart']==1:
        
        zero_prob =[]
        zero_prob_theo=[]
        zero_prob_theo_err=[]
        diff_zero=1-zero_array
        diff_zero_theo=1-zero_array_theo
        #diff_zero_theo_err=1-zero_array_theo
        

        
        for i in np.arange(0, np.size(zero_array)):

            
            temp_zero=diff_zero[0:i+1]
            temp_zero_theo=diff_zero_theo[0:i+1]
            temp_zero_theo_err=diff_zero_theo[0:i+1]

            zero_prob.append(1-np.prod(temp_zero)) 
            zero_prob_theo.append(1-np.prod(temp_zero_theo))
            if i==0:
                error_sum_element=0
            else:
                error_sum_element=zero_prob_theo_err[i-1]
                
            temp_zero_theo_err=zero_prob_theo_err
            zero_prob_theo_err.append(np.sqrt(error_sum_element**2+zero_array_theo_err[i]**2*np.prod(np.square(diff_zero_theo[0:i]))))
            print 'final array'
            print zero_prob_theo_err
            
#        fig = plt.figure()
#        plt.plot(step_array, zero_prob, color='b', label='Recurrence prob')
#        plt.plot(step_array, zero_prob_theo, color='r', label='Recurrence prob theo')
#        plt.legend(loc=1)      
#        plt.xlabel(r'Step', fontsize=20)
#        plt.ylabel(r'Recurrence prob', fontsize=20)
#        plt.show()
        
    if settings['recurrence_continue']==1:
        
        zero_prob =[]
        zero_prob_theo=[]
        zero_prob_theo_err=[]
        
        temp_zero=0
        temp_zero_theo=0
        temp_zero_theo_err=0
        print 'zero_array_theo_err'
        print zero_array_theo_err
  
        for i in np.arange(0, np.size(zero_array)):

            temp_zero+=zero_array[i]
            temp_zero_theo+=zero_array_theo[i]
            temp_zero_theo_err=temp_zero_theo_err+zero_array_theo_err[i]**2
            

            zero_prob.append(temp_zero)  
            zero_prob_theo.append(temp_zero_theo) 
            zero_prob_theo_err.append(np.sqrt(temp_zero_theo_err))

            
            
            
            
    if settings['numerical_error_bars'] == 1:

#        ax.bar(np.arange(-(plot_step_no+0.5), (plot_step_no+0.1), 1),
#               (horizontal[::-1]), width=0.5, color='#DF013A',
#               align='center', label='Hor_numeric',yerr=h_err_theo, error_kw=dict(barsabove=True, capthick=1,
#                elinewidth=1), ecolor='#000000', capsize=10)
#        ax.bar(np.arange(-(plot_step_no+0.4), (plot_step_no+0.1), 1),
#               vertical[::-1], width=0.5, color='#013ADF',
#               align='center', label='Ver_numeric', yerr=v_err_theo, error_kw=dict(barsabove=True, capthick=1,
#               elinewidth=1), ecolor='#000000', capsize=10)
        print 'zero prob'
        print zero_prob
        print 'zero prob theo'
        print zero_prob_theo
        print 'zero prob theo error'
        print zero_prob_theo_err

        fig = plt.figure()
        plt.plot(step_array, zero_prob, color='b', label='Recurrence prob')
#        plt.errorbar(step_array, zero_prob_theo, yerr=zero_prob_theo_err,)
        plt.errorbar(step_array, zero_prob_theo, yerr=zero_prob_theo_err, color='r', label='Recurrence prob theo')
        plt.legend(loc=1)      
        plt.xlabel(r'Step', fontsize=20)
        plt.ylabel(r'Recurrence prob', fontsize=20)
        plt.show()



               
#    elif settings['sinks']==1:
#            ax.bar(np.arange(-(plot_step_no+0.5), (plot_step_no+0.1), 1),
#                   (horizontal_sink[::-1]), width=0.5, color='#DF013A',
#                   align='center', label='Hor_numeric with sinks')
#            ax.bar(np.arange(-(plot_step_no+0.4), (plot_step_no+0.1), 1),
#                   vertical_sink[::-1], width=0.5, color='#013ADF',
#                   align='center', label='Ver_numeric with sinks')

    else:

#        ax.bar(np.arange(-(plot_step_no+0.5), (plot_step_no+0.1), 1),
#               (horizontal[::-1]), width=0.5, color='#DF013A',
#               align='center', label='Hor_numeric')
#        ax.bar(np.arange(-(plot_step_no+0.4), (plot_step_no+0.1), 1),
#               vertical[::-1], width=0.5, color='#013ADF',
#               align='center', label='Ver_numeric')
                           
        print 'zero prob'
        print zero_prob
        print 'zero prob theo'
        print zero_prob_theo
                                 
        fig = plt.figure()
        plt.plot(step_array, zero_prob, color='b', label='Recurrence prob')
        plt.plot(step_array, zero_prob_theo, color='r', label='Recurrence prob theo')
        plt.legend(loc=1)      
        plt.xlabel(r'Step', fontsize=20)
        plt.ylabel(r'Recurrence prob', fontsize=20)
        plt.show()
        


 


def main(settings, argv):
    settings = sstf.handle_settings(settings, ignore_file_settings,
                                    save_settings, setting_label)
    settings['tau_offset'] = settings['second_peak_pos'] - settings['tau1']
    settings['tau_offset2'] = settings['tau_offset']
    if settings['split_step'] == 1:
        settings['tau_offset2'] = settings['tau_offset'] + settings['tau3']
    """
    choose which tdc and which kind of measurement has been used

    """
    if settings['sampling']==1:
        generate_sampling_plot(settings)
    else:
        if settings['tdc'] == 'qutau':
    #        Ha, Va, Hb, Vb, summe, time, timeline = parse_qutau_tdc_file(settings)
                Ha, Va, summe, time, timeline = parse_qutau_tdc_file(settings)
        elif settings['tdc'] == 'bin2histo':
    #        Ha, Va, Hb, Vb, summe, time, timeline = parse_bin2histo(settings)
            Ha, Va, summe, time, timeline = parse_bin2histo(settings)
        elif settings['tdc'] == 'AIT':
            Ha, Va, summe, time, timeline = parse_new_exp(settings)
        else:
    #        Ha, Va, Hb, Vb, summe, time, timeline = parse_old_tdc_file(settings)
            Ha, Va, summe, time, timeline = parse_old_tdc_file(settings)
    #    bins, Ha_bins, Va_bins, Hb_bins, Vb_bins, grid = \
#        if new_exp==1:     #Swapping channels to assign right pol to detectors
#            tmp=Ha
#            Ha=Va
#            Va=tmp
        bins, Ha_bins, Va_bins, grid = \
            generate_grid_and_bins(settings, Ha, Va, summe)
    #        generate_grid_and_bins(settings, Ha, Va, Hb, Vb, summe)
        if settings['walk_2d'] > 0:
            generate_2D_barplots(bins, settings)        
        else:
            generate_1D_barplots(bins, settings, Ha_bins, Va_bins)
        if settings['plot'] == 1:
            plot_timelines(time, summe, timeline, settings, grid)


    logging.shutdown()

if __name__ == "__main__":
    main(settings, sys.argv[1:])
