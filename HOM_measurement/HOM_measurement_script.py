# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 13:40:34 2016

@author: Thomas
"""
import sys, os, time
import signal
import struct
#import SIM900,SIM928
import bin2histo_new_exp as b2h
import numpy as np
import subprocess
import numpy as np
import shutil
import logging
import logging.config
from glob import glob
from time import gmtime, strftime, sleep

#import numpy as np, scipy as sp
#from scipy import special, optimize
#import matplotlib.pyplot as plt
Working_folder= 'Y:/iqo/data/Students/tnitsche/HOM_measurement' 
sys.path.append(Working_folder)
sys.path.append('C:\Users\User\Desktop\TTM8000')
from LinearStage import LinearStage


def signal_handler(signal, frame):
    global exit_flag
    print '*** Exiting ***'
    exit_flag = True

def ensure_dir(f):
    if not os.path.exists(f):
        os.makedirs(f)
#        log.info("directory for today created!")
        

#def set_bias(volt):
#    signal.signal(signal.SIGINT, signal_handler)    
#    if volt > maxv:
#        print "Error Voltage too high"
#        volt = maxv
#    sim = SIM900.device('dev2')
#    vsrc1 = SIM928.device('dev2',3)
#    vsrc2 = SIM928.device('dev2',4)
#    vbias1 = volt
#    vbias2 = volt
#    
#    stat=sim.getstatus(channel=5)
#    if stat:
#        print 'reset: '+time.strftime('%H:%M:%S')
#        vsrc1.setV(0)
#        time.sleep(0.2)
#        vsrc1.setV(vbias1)
#        vsrc1.enable()
#    else: 
#        vsrc1.setV(vbias1)
#        vsrc1.enable()
#    stat=sim.getstatus(channel=6)
#    if stat:
#        print 'reset: '+time.strftime('%H:%M:%S')
#        vsrc2.setV(0)
#        time.sleep(0.2)
#        vsrc2.setV(vbias2)
#        vsrc2.enable()
#    else:
#        vsrc2.setV(vbias2)
#        vsrc2.enable()


def read_path():
    
#    if not os.path.exists(Working_folder  + '/path.ini'):
#        
#        print 'WRRRRRRRRRROOOOOOOOONNG'
#        
#        f = open('path.ini', 'a')
#        f.write('This is a formidable example' '\n')
#        f.write('Of bad programming style')
    
    
    while(True):
            f = open('path.ini', 'r')
            path1 = f.readline()[:-1]
            path2 = f.readline()[:-1]
            print 'Previous path was : ' + path1
            print 'Second to last path was: ' + path2
            f.closed
            path_new = raw_input('Type new path for saving data to change ' +
                                 'path, type "2" for second to last path, ' +
                                 'leave empty for last path: ')
            if os.path.isdir(path_new):
                f = open('path.ini', 'w')
                f.write(path_new+'\n')
                f.write(path1+'\n')
                f.closed
                path1 = path_new
#                log.info("Path changed successfully to: %s", path1)
                
                break
            elif path_new == '2':
                f = open('path.ini', 'w')
                f.write(path2 + '\n')
                f.write(path1 + '\n')
                f.closed
                path1 = path2
#                log.info("Path changed to: %s", path1)
                break
            else:
                print 'Keeping previous path'
#                log.info("Keeping previous path: %s", path1)
                break
    path1 = path1 + "\\" + strftime("%y%m%d", gmtime())
    ensure_dir(path1)
    return path1


def read_number():
    no = raw_input('Number of measurements to take (for n > 1)')
    try:
        no = int(no)
    except ValueError:
#        log.info("Using default. (1)")
        print 'Using default (1)'
        no = 1
    return no

def read_steps():
    maxStart = 0
    maxStop = 0
#    maxStart = -10000
#    maxStop = 10000
    no = raw_input('Number of measurements to take (for n > 1)')
    try:
        no = int(no)
    except ValueError:
#        log.info("Using default. (1)")
        print 'Using default (2)'
        no = 2
    start = raw_input('Start Pos in Steps')
    try:
        start = float(start)
        if np.abs(start) > np.abs(maxStart):
            start = maxStart
            print 'Error start Pos too high, Using default (- 1 000 000)'
    except ValueError:
#        log.info("Using default. (1)")
        print 'Using default (- 1 800 000)'
        start = maxStart
    stop = raw_input('Stop Pos in Steps')
    try:
        stop= float(stop)
        if np.abs(stop) > np.abs(maxStop):
            stop = maxStop
            print 'Error start Pos too high, Using default (+ 1 000 000)'
    except ValueError:
#        log.info("Using default. (1)")
        print 'Using default (+ 1 800 000)'
        stop = maxStop
    return no,start,stop


    
#def read_convert():
#    co = raw_input('Automatic Conversion: ' + '(Type 1 for yes and 0 for no):' 
#                  )
#    try:
#        co = int(co)
#    except ValueError:
#        log.info("Using default. (1)")
#        co = 1
#    return co


def read_time():
    time = raw_input('Time of each measurement (in seconds): ')
    try:
        time = int(time)
    except ValueError:
#        log.info("Using default. (120 Seconds)")
        time = 10
        print 'Using default (10 seconds)'
    return str(time)


def read_filemask():
    
    
#    if not os.path.exists(Working_folder + '/filemask.ini'):
#    
#        print 'WRRRRRRRRRROOOOOOOOONNG'
#    
#        f = open('filemask.ini', 'a')
#        f.write('This is yet a formidable example' '\n')
#        f.write('Of bad programming style')
    
    while(True):
        f = open('filemask.ini', 'r')
        filemask = f.readline()[:-1]
        fileending = f.readline()[:-1]
        f.closed
        print "Previous Filenamemask was: " + filemask
        print "Previous Fileending was: " + fileending
        fileending_new = raw_input('Type new fileending mask, ' +
                                   'leave empty for reuse or type # for new' +
                                   ' Filenamemask : ')
        if fileending_new == '':
#            log.info("Keeping fileending.")
            print 'Keeping fileending'
            break
        if fileending_new == '#':
            filemask_new = raw_input('Type new filename mask: ')
            fileending_new = raw_input('Type new fileending: ')
            f = open('filemask.ini', 'w')
            f.write(filemask_new+'\n')
            f.write(fileending_new+'\n')
            f.closed
#            log.info("Filename changed successfully to: %s %s", filemask_new,
#                     fileending_new)
            filemask = filemask_new
            fileending = fileending_new
            break
        else:
            f = open('filemask.ini', 'w')
            f.write(filemask+'\n')
            f.write(fileending_new+'\n')
            f.closed
            fileending = fileending_new
#            log.info("Filenamemask changed.")
            break
    return filemask+'_'+fileending


def read_comment():
    
#    if not os.path.exists(Working_folder  + '/comment.ini'):
#    
#            print 'WRRRRRRRRRROOOOOOOOONNG'
#        
#            f = open('comment.ini', 'a')
#            f.write('I am getting tired of this')
#        
#        
    while(True):
        f = open('comment.ini', 'r')
        comment = f.readline()
        print 'Previous comment was: ' + comment
        f.closed
        comment_new = raw_input('Type new comment to append to measurement ' +
                                'file or leave empty for last comment: ')
        if comment_new == '':
            print "Keeping previous comment: " + comment
            break
        else:
            f = open('comment.ini', 'w')
            f.write(comment_new)
            f.closed
            comment = comment_new
            break

    return comment


def append_comment(comment, filename):
    if comment == '':
#        log.info("No comment appended.")
         print 'No comment appended'
    else:
        f = open(filename, 'a')
        f.write('#####################################\n# ' + comment +
                '\n###################################')
        f.close
        sleep(1)
#        log.info("Comment appended.")
        
#        + str(counter)


def construct_filename(path, time, filemask, counter):
    # call binary datatype for saving the data
    filename = path  + '/' + strftime("%y%m%d") + '_' + strftime("%H%M%S") +  '_' + filemask   +  '_' + time + 'sec.bin'
# 
    paramfilename = " -n -u " + time + ' -oB -w ' + filename#filemask + '_' + str(counter) + 'th_run' + '_' + time + 'sec.bin'
    # for histogram function, attention: start-stop-measurement!
#     filename = path + '/' + strftime("%y%m%d") + '_' + strftime("%H%M%S") + \
#        '_' + filemask + '_' + time + 'sec_b2_B100000.txt'
#    paramfilename = "-t " + time + ' -H "' + filename + '" -b2 -B100000'
   # filename = "-t "+ time+' -H "'+ filemask+'.txt" -b2 -B100000'
	#args = shlex.split(filename)
    #f = open(os.path.basename(path)+'\\'+strftime("%H%M%S", gmtime())+'_'+ filemask+'.txt' ,'w')
#    log.info("Binary file created!")
#    log.debug("qutau.exe %s", paramfilename)
    return paramfilename, filename

def read_convert():
    co = raw_input('Automatic Conversion: ' + '(Type 1 for yes and 0 for no):'
                  )
    try:
        co = int(co)
    except ValueError:
        log.info("Using default. (1)")
        co = 1
    return co


def main(argv):
    
    movepath = 'D:\Data\new_hom'
    
    a  = LinearStage()
    
    
    print ''
    print '===== Welcome to HOM measurement script====='
    print ''
    signal.signal(signal.SIGINT, signal_handler) 
    path = read_path()
    no,start1,stop1 = read_steps()
    #print no
    #print start1
    #print stop1
    time1 = read_time()
    
#    time1 = 1
#    
#    print 'Zeit'
#    
#    print time1
#    
    #co = read_convert()
    filemask = read_filemask()
    comment = read_comment()
    
    TTMCmd = r"C:\Users\User\Desktop\TTM8000\win32"
    steps =  np.linspace(start1, stop1, no) 
    np.savetxt('steps.txt',steps)
    
        
#    if no > 1:
#        path =  path + '/' + strftime("%y%m%d") + '_' + strftime("%H%M%S") + '_' + filemask + '_' + str(no) + 'x' + time1 + 's'
#
#        os.makedirs(path)
    
    poslist = np.array([])
    actual_poslist = np.array([])
    a.MoveAbsolute(0)
    a.WaitUntilFinished()
    time.sleep(1)
    pos = a.GetPosition()
    actual_poslist = np.append(actual_poslist, pos)
    for i in np.arange(no):
        
        path1 =  path + '/' + strftime("%y%m%d") + '_' + strftime("%H%M%S") + '_' + filemask + '_' + time1 + 's' + '_' + str(i)

#        print 'path'
#        
#        print path
        
        os.makedirs(path1)
        
        try:
            #signal.signal(signal.SIGINT, signal_handler) 
            
            #print i
            #print "volts " + str(steps[i])            
            pos=int(steps[i])
            poslist = np.append(poslist,pos)
            a.MoveAbsolute(pos)
#            a.MoveRelative(1000)
#            time.sleep(1)
            a.WaitUntilFinished()            
            pos = a.GetPosition()
#            print 'position'
#            print pos
            actual_poslist = np.append(actual_poslist, pos)
            
            print 'i was here'
            
            for j in np.arange(0, int(time1), 5):
                
#                print 'but not here'
#                
#                print j
                
                paramfile, filename = construct_filename(path1, str(5), filemask, i)
                
#                print 'paramfile'
#                
#                print paramfile
                
                
                
        
            
#            log.debug("Starting Subprocess")
                proc = subprocess.Popen("cmd", shell=True, bufsize=-1,
                                        stdin=subprocess.PIPE)
                #proc.wait()
                #print paramfile                        
                proc.communicate(input="TTMCnvt_32.exe" +paramfile+"\n")
                proc.wait()
                #print 'Successfully contacted TTM'
                time.sleep(1)
            
            try:
                # move folder to another folder (external harddrive) for large data sets
                filepath_only = os.path.split(filename)[0]
                print 'Filename'
                print filepath_only
#                print filepath_only
#                print movepath
                shutil.move(filepath_only, movepath)
            except:
                log.info('Folder could not be moved!!!')
#            log.debug("Subprocess terminated.")
            #if retcode < 0:
            #    print >>sys.stderr, "Child was terminated by signal", -retcode
            #else:
            #    print >>sys.stderr, "Child returned", retcode
        except:
            print >>sys.stderr, "Execution failed:"
            no = read_number()
            
#    if co == 1:
#         
#            # create histogram-txt-file from binary##############################
############# see main in bin2histo for details################################
#        if no == 1:
#        # convert a single bin-file to a histogram-file#######################
#            print 'starting to convert saved bindata to histogram txt.file'
#            name = os.path.split(filename)[1]
#            histo = b2h.readData_createHisto(path, name)
#            print histo
#            #----------save histo--------------------------------------------
#            txtfilename = os.path.join(path, os.path.splitext(name)[0] +
#                                       "_calcfrombin.txt")
#            comment = 'used function data2histo_arrayOp'
#            b2h.savehisto2txt(txtfilename, histo, comment)
#            del histo
#            print 'conversion into histogram txt.file ' + txtfilename + ' done'
#        else:
#        # loads all the single files in a path and makes only one histogram out of them
#            count = 0
#            filename_list_comment = ''
#            filelist = []
#            for pfilename in glob(os.path.join(path, '*.bin'))[::-1]:
#                name = os.path.split(pfilename)[1]
#                filename_list_comment += '# ' + name + '\n'
#                filelist.append(name)
#                log.info("starting with file: %s", name)
#                if count == 0:   # first file
#                    histo_sum = b2h.readData_createHisto(path, name)                    
#                else:    # sum up for all other file
#                    histo_sum += b2h.readData_createHisto(path, name)
#                count += 1
##            histo_sum_peak = b2h.sum_over_peak(histo_sum,40,6)
#            print histo_sum
##            global test 
##            test = histo_sum
##            print histo_sum_peak
#            
#            
#    #        ----------save histo-------------------------------------------------
#            listname = b2h.create_filename_fromList(filelist)
#            txtfilename = os.path.join(path, listname + "_calcfrombin.txt")
#            comment = 'Used function data2histo_arrayOp \n# Included the following files:\n'
#            comment = comment + filename_list_comment + '#'
#            b2h.savehisto2txt(txtfilename, histo_sum, comment)
#
#        #change.shrink_filesize(filename)       #  not needed anymore
            
    poslist = np.append(poslist,0)
    a.MoveAbsolute(0)
    a.WaitUntilFinished()
    time.sleep(1)
    pos = a.GetPosition()
    actual_poslist = np.append(actual_poslist, pos)
    np.savetxt(os.path.join(movepath, 'positions.txt'),poslist)  
    np.savetxt(os.path.join(movepath, 'actual_positions.txt'),actual_poslist)
#    set_bias(0.2)     
    no = read_number()
#    logging.shutdown()

if __name__ == "__main__":
    main(sys.argv[1:])



