# -*- coding: utf-8 -*-
"""
Created on Wed Jun 27 14:59:49 2012

@author: Katzschmann
"""

import numpy as np
import sys
#import scipy.linalg
#from mpl_toolkits.mplot3d import axes3d
#from matplotlib.colors import Normalize
import matplotlib.pyplot as plt
import pprint as pp
import logging
import logging.config
import logging.handlers

logging.config.fileConfig('loggingConfig.txt')
log = logging.getLogger("myLogger.theory_1D_dtqw")


class FuncFilter(logging.Filter):
    def __init__(self, name=None):
        pass

    def filter(self, rec):
        if rec.funcName in ['create_coin_list_h', 'create_coin_list_v']:
            return False
        else:
            return True

funcfilter = FuncFilter()
log.addFilter(funcfilter)

settings = dict(
    max_steps=6,
    HWP_alpha=45,  # in  Degree - angle for the HWP if used
                     # 22.5 for Hadamard Coin
    QWP_alpha=45,    # in Degree - angle the QWP in front of the EOM is
                     # aligned for the no HWP scheme
                     # To use this, set phi to -pi/2, 0 and pi/2 and observe
                     # the coins identity, hadamard and not realized
    beta=45,    # in Degree - angle the EOM is oriented for the normal coin
                # (i.e. applies to all positions) default: 45
    phiT=-90/360.*2*np.pi,   # in rad - phase the EOM applies at positions from
                            # 'reflect_list' - for Transmissions
    phiR = 90/360.*2*np.pi,   #  for Reflections
    loss_factor=1,   # to simulate more loss in H then in V

    HHa=np.sqrt(1),  # 0.98,
    HVa=np.sqrt(1),  # 1,
    VHa=np.sqrt(1),  # 0.85,
    VVa=np.sqrt(1),  # 0.88,
    HHb=np.sqrt(1),  # 0.85,
    HVb=np.sqrt(1),  # 0.87,
    VHb=np.sqrt(1),  # 1,
    VVb=np.sqrt(1),  # 1,

    sbc=0.0*np.pi,  # phase the sbc introduces to H light in the V fiber
                    # should be 0 if the measurement is ideal

    reflect_list=[],
    reflect_list_name = r"d:\Data\DTQW_Data\TopologicalPulseFiles\Fidelity_TestWalk_5Steps.txt",
    r_coef=np.sqrt(1),     # how well does a reflection work (1 = perfect)
    t_coef=np.sqrt(1),     # how well does a transmission work (1 = perfect)

    split_step=0,  # set to 1 for a split step QW, important for the couplings
    EOM_active=1,             # toggle to either activate the EOM (1) or just
                           # do a passive walk (0)
    qwp_mode=1,  # toggle to switch if there is a HWP in front of the EOM as
                 # coin 1 (0) or a QWP (1)
                 # Attention: qwp_mode=1 with split_step=1 will lead to the
                 # assumption that in the b mode there is also a QWP present
                 # at angle 'qwp_alpha'
    sbc_mode=1,  # toggle to switch if there is an SBC in front of EOM,
                 # new by Sonja 150615
    pol='V'      # input polarisation: H,V,D,A,RHC,LHC
)
settings['sbc_phi'] = 90/360.*2*np.pi  # settings['phiR']

def turning_matrix(alpha, settings):
    """
    Function to declare an turning matrix with full dimensionality determinded
    by 'max_steps'.
    """
    x = turning_matrix_2d(alpha)
    return np.kron(x, np.eye(2*settings['max_steps']+1, dtype="complex"))


def turning_matrix_2d(alpha):
    """
    Function to declare an turning matrix as 2x2 Matrix.
    """
    x = np.array([[np.cos(alpha), -np.sin(alpha)],
                  [np.sin(alpha), np.cos(alpha)]],
                 dtype="complex")
    return x


def phase_shift(alpha, settings):
    """
    Function to declare an phase-shift induced by an EOM with full
    dimensionality determinded by 'max_steps'.
    """
    x = phase_shift_2d(alpha, settings)
    return np.kron(x, np.eye(2*settings['max_steps']+1, dtype="complex"))


def phase_shift_2d(alpha, settings):
    """
    Function to declare an phase-shift induced by an EOM as 2x2 Matrix.
    """
    x = np.array([[np.exp(-1j*alpha), 0.], [0., 1.]], dtype="complex").dot(
        np.array([[1., 0], [0., np.exp(1j*alpha)]]))
    return x


def EOM_2d(phi, settings):
    """
    Function to declare the EOM including a possibly by beta rotated axis
    as 2x2 Matrix.
    """
    x = turning_matrix_2d(settings['beta']).dot(phase_shift_2d(
        phi, settings).dot(
            turning_matrix_2d(-settings['beta'])))
    return x


def HWP(alpha, settings):
    """
    Function to declare an Half-Wave-Plate with full dimensionality determinded
    by 'max_steps'.
    """
    x = HWP_2d(alpha, settings)
    return np.kron(x, np.eye(2*settings['max_steps']+1, dtype="complex"))


def HWP_2d(alpha, settings):
    """
    Function to declare an Half-Wave-Plate as 2x2 Matrix.
    """
    x = np.array([[np.cos(2*alpha), np.sin(2*alpha)],
                  [np.sin(2*alpha), -np.cos(2*alpha)]])
    return x


def QWP(alpha, settings):
    """
    Function to declare an Quarter-Wave-Plate with rotation axis alpha with
    full dimensionality determinded by 'max_steps'.
    """
    x = QWP_2d(alpha, settings)
    return np.kron(x, np.eye(2*settings['max_steps']+1, dtype="complex"))


def QWP_2d(alpha, settings):
    """
    Function to declare an Quarter-Wave-Plate with rotation axis alpha as
    2x2 Matrix.
    """
    x = turning_matrix_2d(alpha).dot(np.array(
        [[np.exp(-1j*np.pi/4.), 0], [0, 1j*np.exp(-1j*np.pi/4.)]]).dot(
            turning_matrix_2d(-alpha)))
    return x


def step_old(settings, actual_step, psi):
    """
    Function to create the step operator without fibre couplings (but still
    needed for consistency checks, since it calculates the matrices for the
    EOM directly -> easier to compare)
    Attention:
        cannot take fibre losses into account!
        Can only do EOM or HWP coin!
    """
    #create step matrix
    x = np.diagflat(np.ones(2*settings['max_steps']), k=1)
    y = np.diagflat(np.ones(2*settings['max_steps']), k=-1) * \
        settings['loss_factor']
    a = np.zeros((2*settings['max_steps']+1, 2*settings['max_steps']+1),
                 dtype="complex")
    b = np.zeros((2*settings['max_steps']+1, 2*settings['max_steps']+1),
                 dtype="complex")
    step = np.vstack([np.hstack([x, a]), np.hstack([b, y])])

    if settings['EOM_active'] == 1:
        try:
            EOM_coin = create_eom_coin(settings, settings['reflect_list'],
                                        int(actual_step-1))
            psi = step.dot(EOM_coin.dot(psi))
#            plt.figure()
#            plt.subplot(1,2,1)
#            plt.title('step_old ' + str(actual_step))
#            plt.imshow(np.real(step.dot(EOM_coin)), interpolation="nearest")
#            plt.colorbar()
#            plt.subplot(1,2,2)
#            plt.imshow(np.imag(step.dot(EOM_coin)), interpolation="nearest")
#            plt.colorbar()
#            print np.around(step.dot(EOM_coin),4)
        except IndexError:

                psi = step.dot(create_eom_coin(settings, [[]], 0).dot(psi))
    else:
           
        psi = step.dot(HWP(settings['HWP_alpha'], settings).dot(psi))

    return psi


def step(settings, actual_step, psi_old):
    """
    Function to create the step operator which takes different fibre couplings
    into account (in settings: HHa, HHb, HVa, HVb, VHa, VHb, VVa, VVb)
    """
    #get the coin matrix depending on the chosen walk pattern
    if (settings['EOM_active'] == 1 and (actual_step % 2 == 1 or
                                         settings['split_step'] == 0)):
    # This makes sure that we only use the EOM modified coins if it is
    # a) active and b) we are in the A mode because we are either in an
    # odd roundtrip or do not use split-step
        try:
            Coin = create_eom_coin(settings, settings['reflect_list'],
                                   int(actual_step-1))
        except IndexError:
            log.warn("Index Error in Theory Step occured")
            Coin = create_eom_coin(settings, [[]], 0)
    else:
        if settings['qwp_mode'] == 1:
            
            if actual_step==1 and settings['new_exp']==1:
                QWP_angle=0
            else:
                QWP_angle=settings['QWP_alpha']
                
            Coin = QWP(QWP_angle, settings)

        else:
            if actual_step==1 and settings['new_exp']==1:
                HWP_angle=0
            else:
                HWP_angle=settings['HWP_alpha']
                
            Coin = HWP(HWP_angle, settings)
#    print np.around(Coin,2)
    if (actual_step % 2 == 1 or settings['split_step'] == 0):
        # odd step number, we are in B-mode, but coupled via A; or only A-mode
        HH = settings['HHa']
        HV = settings['HVa']
        VH = settings['VHa']
        VV = settings['VVa']
    else:  # we coupled via B-mode
        HH = settings['HHb']
        HV = settings['HVb']
        VH = settings['VHb']
        VV = settings['VVb']
    # create lists/arrays for izip
    psi_a_arr = create_psi_list(psi_old[:len(psi_old) * 0.5])
    Coin_b_arr = create_coin_list_h(settings, Coin)
    Coupl_c_arr = np.append(np.ones(len(psi_old) * 0.5) * HH,
                            np.ones(len(psi_old) * 0.5) * HV)
    psi_d_arr = create_psi_list(psi_old[len(psi_old) * 0.5:])
    Coin_e_arr = create_coin_list_v(settings, Coin)
    Coupl_f_arr = np.append(np.ones(len(psi_old) * 0.5) * VH *
                            np.e**(1j * settings['sbc']),
                            np.ones(len(psi_old) * 0.5) * VV)
    psi_new = psi_a_arr * Coin_b_arr * Coupl_c_arr + \
        psi_d_arr * Coin_e_arr * Coupl_f_arr
    log.debug("Psi new: \n%s", pp.pformat(psi_new))
    return psi_new/np.linalg.norm(psi_new)
        # Euklidian normalization necessary due to losses


def create_psi_list(psi):
    """
    Function to create the list for izip in the new step function distributing
    the psi vector elements in the correct order
    expects only one half of the psi vector
    """
    half1 = np.append(psi[1:], 0)
    half2 = np.append(0, psi[:len(psi)-1])
    result = np.append(half1, half2)
    return result


def create_coin_list_h(settings, coin):
    """
    Function to create the top list of the coin_elements used to include in our
    scheme to also include different couplings
    """
    coin_list = []
    for i in np.arange(1, 2*settings['max_steps']+1):
#        print i, i
        coin_list.append(coin[i, i])
    coin_list.append(0)
    coin_list.append(0)
    for i in np.arange(0, 2*settings['max_steps']):
#        print i + settings['max_steps'], i
        coin_list.append(coin[i + (2*settings['max_steps']+1), i])
    log.debug("Coinlist: %s", pp.pformat(coin_list))
    return np.asarray(coin_list)


def create_coin_list_v(settings, coin):
    """
    Function to create the top list of the coin_elements used to include in our
    scheme to also include different couplings
    """
    coin_list = []
    for i in np.arange(1, 2*settings['max_steps']+1):
#        print i, i + settings['max_steps']
        coin_list.append(coin[i, i + 2*settings['max_steps']+1])
    coin_list.append(0)
    coin_list.append(0)
    for i in np.arange(0, 2*settings['max_steps']):
#        print i + settings['max_steps'], i + settings['max_steps']
        coin_list.append(coin[i + (2*settings['max_steps']+1),
                              i + (2*settings['max_steps']+1)])
    log.debug("Coinlist: %s", pp.pformat(coin_list))
    return np.asarray(coin_list)


def create_eom_coin(settings, reflect_list, i):
    """
    Function to recreate the boundary coin in the QW setup
    New by Sonja: multiplication with r_coef and t_coef now takes place at the
    definition of the coin-matrices coin_plus, -minus, -zero, since it is not
    needed (and not correct) for the sbc-mode
    """
    if settings['qwp_mode'] == 1:
        coin_plus = QWP_2d(settings['QWP_alpha'], settings).dot(
            EOM_2d(settings['phiR'], settings))
        coin_minus = QWP_2d(settings['QWP_alpha'], settings).dot(
            EOM_2d(settings['phiT'], settings))
        coin_zero = QWP_2d(settings['QWP_alpha'], settings).dot(
            EOM_2d(0, settings))
        coin_plus[0, 0] *= np.sqrt(1 - settings['r_coef']**2)
        coin_plus[1, 1] *= np.sqrt(1 - settings['r_coef']**2)
        coin_plus[1, 0] *= settings['r_coef']
        coin_plus[0, 1] *= settings['r_coef']
        coin_minus[0, 0] *= settings['t_coef']
        coin_minus[1, 1] *= settings['t_coef']
        coin_minus[1, 0] *= np.sqrt(1 - settings['t_coef']**2)
        coin_minus[0, 1] *= np.sqrt(1 - settings['t_coef']**2)
    elif settings['sbc_mode'] == 1:
        coin_plus = EOM_2d(settings['phiR'], settings).dot(EOM_2d(
            settings['sbc_phi'], settings))   # switch 2 * angle
        coin_minus = EOM_2d(settings['phiT'], settings).dot(EOM_2d(
            settings['sbc_phi'], settings))  # identity switch back and forth
        coin_zero = EOM_2d(0, settings).dot(EOM_2d(
            settings['sbc_phi'], settings))   # default: switch angle
    else:
        coin_plus = HWP_2d(settings['HWP_alpha'], settings).dot(
            EOM_2d(settings['phiR'], settings))
        coin_minus = HWP_2d(settings['HWP_alpha'], settings).dot(
            EOM_2d(-settings['phiT'], settings))
        coin_zero =  HWP_2d(settings['HWP_alpha'], settings).dot(
            EOM_2d(0, settings))
        coin_plus[0, 0] *= np.sqrt(1 - settings['r_coef']**2)
        coin_plus[1, 1] *= np.sqrt(1 - settings['r_coef']**2)
        coin_plus[1, 0] *= settings['r_coef']
        coin_plus[0, 1] *= settings['r_coef']
        coin_minus[0, 0] *= settings['t_coef']
        coin_minus[1, 1] *= settings['t_coef']
        coin_minus[1, 0] *= np.sqrt(1 - settings['t_coef']**2)
        coin_minus[0, 1] *= np.sqrt(1 - settings['t_coef']**2)
    log.debug("Reflect_list: %s", pp.pformat(reflect_list))
#    print  np.around(coin_plus,2)
#    print  np.around(coin_minus,2)
#    print  np.around(coin_zero,2)
    #  parse reflect_list
    reflect_pos = []
    type_rt = []
    for reflections in reflect_list[i]:
        dum = reflections.split()
        reflect_pos.append(int(dum[0]))    # where does it take place?)
        type_rt.append(dum[1])        # what type of EOM operation?
        if abs(int(dum[0])) > i:
            log.warn("Check your reflect list! There is an Error! But maybe \
            it is not an error if you use initial states starting at a \
            position away from zero (2 particle simulations e.g.)")

    # create empty matrix of zeros
    x = np.zeros((4*settings['max_steps']+2, 4*settings['max_steps']+2),
                 dtype="complex")
    # check if positions appear in reflect list
    for pos in np.arange(-settings['max_steps'], settings['max_steps']+1):
        if pos in reflect_pos:
            log.debug('%s found in reflect_pos at %s', pos,
                      reflect_pos.index(pos))
            if (type_rt[reflect_pos.index(pos)] == "r" or
                    type_rt[reflect_pos.index(pos)] == "p"):
                log.debug('Corresponding action (reflection) is %s',
                          type_rt[reflect_pos.index(pos)])
                x[settings['max_steps'] + 1/2 + pos,
                  settings['max_steps'] + 1/2 + pos] = coin_plus[0, 0]
                x[3 * settings['max_steps'] + 3/2 + pos,
                  3 * settings['max_steps'] + 3/2 + pos] = coin_plus[1, 1]
                x[3 * settings['max_steps'] + 3/2 + pos,
                  settings['max_steps'] + 1/2 + pos] = coin_plus[1, 0]
                x[settings['max_steps'] + 1/2 + pos,
                  3 * settings['max_steps'] + 3/2 + pos] = coin_plus[0, 1]
            elif (type_rt[reflect_pos.index(pos)] == "t" or
                    type_rt[reflect_pos.index(pos)] == "m"):
                log.debug('Corresponding action is %s',
                          type_rt[reflect_pos.index(pos)])
                x[settings['max_steps'] + 1/2 + pos,
                  settings['max_steps'] + 1/2 + pos] = coin_minus[0, 0]
                x[3 * settings['max_steps'] + 3/2 + pos,
                  3 * settings['max_steps'] + 3/2 + pos] = coin_minus[1, 1]
                x[3 * settings['max_steps'] + 3/2 + pos,
                  settings['max_steps'] + 1/2 + pos] = coin_minus[1, 0]
                x[settings['max_steps'] + 1/2 + pos,
                  3 * settings['max_steps'] + 3/2 + pos] = coin_minus[0, 1]
            else:
                log.critical('Unknown switchtype %s in line %s' +
                             ' in reflect_list',
                             type_rt[reflect_pos.index(pos)], i)
                break
        else:
            log.debug('%s not found in reflect_pos', pos)
            x[settings['max_steps'] + 1/2 + pos,
              settings['max_steps'] + 1/2 + pos] = coin_zero[0, 0]
            x[3 * settings['max_steps'] + 3/2 + pos,
              3 * settings['max_steps'] + 3/2 + pos] = coin_zero[1, 1]
            x[3 * settings['max_steps'] + 3/2 + pos,
              settings['max_steps'] + 1/2 + pos] = coin_zero[1, 0]
            x[settings['max_steps'] + 1/2 + pos,
              3 * settings['max_steps'] + 3/2 + pos] = coin_zero[0, 1]

#    plt.figure()
#    plt.subplot(1,2,1)
#    plt.imshow(np.real(x), interpolation="nearest")
#    plt.colorbar()
#    plt.subplot(1,2,2)
#    plt.imshow(np.imag(x), interpolation="nearest")
#    plt.colorbar()
#    plt.figure()
#    plt.title('plus')
#    plt.subplot(1,2,1)
#    plt.imshow(np.real(coin_plus), interpolation="nearest")
#    plt.colorbar()
#    plt.subplot(1,2,2)
#    plt.imshow(np.imag(coin_plus), interpolation="nearest")
#    plt.colorbar()
#    plt.figure()
#    plt.title('Zero')
#    plt.subplot(1,2,1)
#    plt.imshow(np.real(coin_zero), interpolation="nearest")
#    plt.colorbar()
#    plt.subplot(1,2,2)
#    plt.imshow(np.imag(coin_zero), interpolation="nearest")
#    plt.colorbar()
#    print np.around(x,2)
    return x


def generate_grid(tau_offset, tau, max_pos):
    """
    Function to generate the grid that defines the bins
    """
    grid = []
    for n in np.arange(0, max_pos+0.1):
        result = tau_offset + n*tau
        grid.append(result)
    grid = np.asarray(grid)
    return grid


def generate_initial_state(settings, plot_step_no=-1):
    """
    Function to generate the initial state psi
    Pulse will always be initialized at position 0
    Possible polarizations: H,V,D,LHC,RHC (default H)
    """
#    print "Settings before change: "
#    pp.pprint(settings)
    if plot_step_no >= 0:
        save_settings = settings['max_steps']
        settings['max_steps'] = plot_step_no
#    print "Settings after change: "
#    pp.pprint(settings)
    psi = np.zeros(4*settings['max_steps']+2, dtype="complex")
    if settings['pol'] == "H" or settings['pol'] == "h":
        psi[settings['max_steps']] = 1
    elif settings['pol'] == "V" or settings['pol'] == "v":
        psi[3*settings['max_steps']+1] = 1
    elif settings['pol'] == "D" or settings['pol'] == "d":
        psi[settings['max_steps']] = np.sqrt(0.5)
        psi[3*settings['max_steps']+1] = np.sqrt(0.5)
    elif settings['pol'] == "A" or settings['pol'] == "a":
        psi[settings['max_steps']] = np.sqrt(0.5)
        psi[3*settings['max_steps']+1] = -np.sqrt(0.5)
    elif settings['pol'] == "RHC" or settings['pol'] == "rhc":
        psi[settings['max_steps']] = np.sqrt(0.5)
        psi[3*settings['max_steps']+1] = -np.sqrt(0.5)*1j
    elif settings['pol'] == "LHC" or settings['pol'] == "lhc":
        psi[settings['max_steps']] = np.sqrt(0.5)
        psi[3*settings['max_steps']+1] = np.sqrt(0.5)*1j
    log.debug("Initial state is: %s", settings['pol'])
    log.debug("Psi: %s", pp.pformat(psi))
    if plot_step_no >= 0:
        settings['max_steps'] = save_settings
    return psi


def time_evolution(settings, psi, plot_step_no=-1):
    """
    Function to do the actual time evolution with step, the coin is now chosen
    in the step function

    Parameters:

    max_steps = step that is supposed to be given back as end state of psi
    psi = initial state
    loss_factor =  to simulate more loss in H than in V
    hwp_alpha = angle of HWP in ° (default 22.5°)
    beta = angle of EOM in ° (default 45°)
    phi = phase shift of the eom (applied at positions from 'time_list')
        (default 0)
    """
   # print "Half-Wave-Plate is at: " + str(settings['hwp_alpha'])
    if plot_step_no >= 0:
        save_settings = settings['max_steps']
        settings['max_steps'] = plot_step_no
    for i in np.arange(1, settings['max_steps']+0.1):
        psi = step(settings, i, psi)
#            plt.figure()
#            plt.title('Step '+str(i))
#            plt.imshow(np.real(step(settings).dot(coin(settings))),
#                       interpolation="nearest")
#            print coin(settings)
#            print psi
    #print step(settings)
    #print coin(settings)
    #print boundary_coin(settings)
#    result = psi * psi.conj()
#    summe = result[2*settings['max_steps']+1:] + \
#        result[:2*settings['max_steps']+1]

    # calculate all the tomography states
    psi_HV = psi
    psi_DA = HWP(np.pi/8, settings).dot(psi)
    psi_RL = QWP(-45/360.*2*np.pi, settings).dot(psi)
    if plot_step_no >= 0:
        settings['max_steps'] = save_settings
    return psi_HV, psi_DA, psi_RL

#fig = plt.figure()
#width =0.5
#ax = fig.add_subplot(111)
#title(r'Step' + str(settings['max_steps']))
#ax.title.set_y(1.02)
#plt.xlabel(r'Position')
#plt.ylabel(r'peak height [a.u.]')
#ax.set_xlim(-(settings['max_steps']+1), settings['max_steps']+1)
#ax.bar(np.arange(-(settings['max_steps']),(settings['max_steps']+0.1),1),
#       (summe[:]), width=0.5, color='g', align ='center')


def RepresentsNumber(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def load_refl_list_old(settings):
    """
    function that loads a file with a reflection list and stores it in the
    correct format in the corresponding setting field
    ATTENTION: The signs of the positions are changed here to be consistent in
    theory and experiment
    ATTENTION: "old" means here that it works only for old reflect lists where
    the first line corresponds to the first step and not to the zeroth step.
    This convention was changed 16.7.2015 when we wanted to include the
    possibility to switch the initial (=0th) pulse as well.
    """

    with open(settings['reflect_list_name'], 'r') as f:
        lines = f.readlines()
        f.closed

    refl_list = [[]]
    for one_line in lines:
        step_switches = []
        one_line = one_line.split()
        for idx, item in enumerate(one_line):
            if idx % 2 == 0:    # even item position:
                                # item supposed to be a number
                if RepresentsNumber(item):
                    if __name__ == '__main__':
                        one_switch = str(1*int(item))   # append
                    else:
                        one_switch = str(-1*int(item))   # change sign and
                                                         # append for other
                                                         # scripts than theory
                else:
                    if (item == 's' or item == 'S'):  # in this step nothing
                        step_switches = []            # has to happen -> []
                        pass
                    else:
                        print "Something very terrible happened! (Probably \
                        invalid char in reflection list file)"
                        sys.exit(1)
            else:   # odd position: item supposed to be 'r' or 't'
                if item == 'r' or item == 't':
                    one_switch += ' '
                    one_switch += item
                    step_switches.append(one_switch)
                else:
                    print "Something very terrible happened! (Probably \
                    invalid char in reflection list file)"
                    sys.exit(1)

        refl_list.append(step_switches)
#    print refl_list
    settings['reflect_list'] = refl_list


def load_refl_list(settings):
    """
    function that loads a file with a reflection list and stores it in the
    correct format in the corresponding setting field
    ATTENTION: The signs of the positions are changed here to be consistent in
    theory and experiment
    ATTENTION: This new function works for reflect list in which the first line
    corresponds to the zeroth (=initial pulse)
    This convention was changed 16.7.2015 when we wanted to include the
    possibility to switch the initial (=0th) pulse as well.
    """

    with open(settings['reflect_list_name'], 'r') as f:
        lines = f.readlines()
        f.closed

    refl_list = []
    for one_line in lines:
        step_switches = []
        one_line = one_line.split()
        for idx, item in enumerate(one_line):
            if idx % 2 == 0:    # even item position:
                                # item supposed to be a number
                if RepresentsNumber(item):
                    if __name__ == '__main__':
                        one_switch = str(1*int(item))   # append
                    else:
                        one_switch = str(-1*int(item))   # change sign and
                                                         # append for other
                                                         # scripts than theory
                else:
                    if (item == 's' or item == 'S'):  # in this step nothing
                        step_switches = []            # has to happen -> []
                        pass
                    else:
                        print "Something very terrible happened! (Probably \
                        invalid char in reflection list file)"
                        sys.exit(1)
            else:   # odd position: item supposed to be 'r' or 't'
                if item == 'r' or item == 't':
                    one_switch += ' '
                    one_switch += item
                    step_switches.append(one_switch)
                else:
                    print "Something very terrible happened! (Probably \
                    invalid char in reflection list file)"
                    sys.exit(1)

        refl_list.append(step_switches)
#    print refl_list
    settings['reflect_list'] = refl_list


def main():
    settings['HWP_alpha'] = settings['HWP_alpha']/360.*2*np.pi
    # conversion to rad
    settings['beta'] = settings['beta']/360.*2*np.pi   # conversion to rad
    settings['QWP_alpha'] = settings['QWP_alpha']/360.*2*np.pi
            # conversion to rad
    """
    initialize reflect list
    """
#    load_refl_list_old(settings)    # for reflect lists older than 16.7.2015
    load_refl_list(settings)

    psi = generate_initial_state(settings)
    psi, psi_DA, psi_RL = time_evolution(settings, psi)
#    print np.around(psi, 3)
    log.debug(psi)


    # Tomography test with SBC at arbitrary angle
#    angle = settings['sbc_phi']+settings['phiR']   # 30/360.*2*np.pi
#    Tom_matrix = turning_matrix_2d(settings['beta']).dot(phase_shift_2d(
#        angle, settings).dot( turning_matrix_2d(-settings['beta'])))
#    Tom_matrix=  np.kron(Tom_matrix, np.eye(2*settings['max_steps']+1, dtype="complex"))
#    psi = Tom_matrix.dot(psi)

    fig = plt.figure()
#    width =0.5
    ax = fig.add_subplot(111)
    realization = ''
    if settings['EOM_active'] == 1:
        realization = ' with active EOM'
    plt.title(r'Step' + str(settings['max_steps']) + realization)
    ax.title.set_y(1.02)
    plt.xlabel(r'Position')
    plt.ylabel(r'peak height [a.u.]')
    ax.set_xlim(-(settings['max_steps']+2), settings['max_steps']+2)
    ax.bar(np.arange(-(settings['max_steps']+0.2),
           (settings['max_steps']+0.3), 1),
           (psi[2*settings['max_steps']+1:] *
           psi[2*settings['max_steps']+1:].conj()) /
           np.sum(psi[:] * psi[:].conj()), width=0.5, color='b',
           align='center')
#        print "blue(horizontal): "
#        print np.real(psi[2*settings['max_steps']+1:] *
#                  psi[2*settings['max_steps']+1:].conj() /
#                  np.sum(psi[:] * psi[:].conj()))
    ax.bar(np.arange(-(settings['max_steps']),
                    (settings['max_steps']+0.1), 1),
           (psi[:2*settings['max_steps']+1] *
           psi[:2*settings['max_steps']+1].conj()) /
           np.sum(psi[:] * psi[:].conj()), width=0.5, color='r',
           align='center')
#        print "red(vertical): "
#        print np.real(psi[:2*settings['max_steps']+1] *
#                  psi[:2*settings['max_steps']+1].conj() /
#                  np.sum(psi[:] * psi[:].conj()))
    plt.show()

    logging.shutdown()
#    scan_phase(settings)

#create_coin_list_v(settings, HWP(np.pi/8, settings))


if __name__ == '__main__':
    main()
