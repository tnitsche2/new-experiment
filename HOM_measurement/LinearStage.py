#!/usr/bin/env python
"""
#------------------------------------------------------------------
# 
#   Python-Program
#
#   file name: PIStageController.py
#   creator:   Malte Avenhaus
#   email:     malte.avenhaus@gmx.de
#   purpose:   Interface for Serial Communication with a
#              PI Linear Stage Motor Controller
#
#-------------------------------------------------------------------

#-------------------------------------------------------------------
#
#   History:
#
#   02.01.2007     Malte Avenhaus         created
#
#--------------------------------------------------------------------
"""

# Includes
import sys
import os
import serial
import time
from ScanSerial import ScanUsbSerial

                           
class LinearStage:
    """Providing Access to a Motor-Controller (Mercury C-862) attached
       Linear Stage from PI"""
    def __init__(self, **kwargs):
        """
        @parameter SerialPort: Port-Number or address
                             Windows: e.g. 0, 1
                             Mac:     e.g. /dev/cu.usbserial-0000101D
                             Linux:   e.g. /dev/usb/ttyUSB[n]
        """
        if kwargs.has_key("SerialPort"):
          SerialPort = [kwargs['SerialPort'],]
        else:
          SerialPort = ScanUsbSerial()
          
        self.ser = None
        print SerialPort
#        SerialPort = ['COM1']
        print SerialPort
        # Trying to start communication with device
        for port in SerialPort:
            try:
                self.ser = serial.Serial(port, 9600, timeout=1)
                self.SetAddress(kwargs.get("Address", 0))
                time.sleep(0.5)
                self.ser.write('xx\rEF\rTP\rMN\r')
                time.sleep(0.5)
                if(self.ser.inWaiting() == 0):
                    self.ser.close()
                else:
                    self.ser.flushInput()
                    break
            except:
                pass                                        # Trying next port
        
        if self.ser == None:
            raise IOError("Could not start communication with Linear Stage over any of the serial ports: " + str(SerialPort))

    def __del__(self):
        """
        Destructor: Close Serial Port
        """
        if self.ser is not None:
            self.ser.close()

    def MoveAbsolute(self, Position):
        """
        Move Stage to absolute Position
        
        @param Position: Move Stage to new Position
        @type Position: int
        """
        self.ser.write('MA'+str(Position)+'\r')
        self.ReadBuffer()
  
    def MoveRelative(self, Steps):
        """
        Move Stage relative to current Position
        
        @param Steps: Number of steps to move
        @type Steps: int
        """
        self.ser.write('MR'+str(Steps)+'\r')
        self.ReadBuffer()

    def MoveHome(self):
        """
        Move Stage to home position
        """
        self.ser.write('GH'+'\r')
        self.ReadBuffer()

    def GetAcceleration(self):
        """
        Returns programmed acceleration setting
                 
        @return: accelaration
        @rtype: int
        """
        self.ser.flushInput()
        self.ser.write("TL\r")
        a = self.ReadBuffer()
        print "Acceleration: ", a[:-1]
        a = int(a[2:-1])
        return a

    def GetFirmwareInformation(self):
        """
        Returns list containing information about Firmware Version, Company and Copyright
        
        @return: Manufacturer, Copyright, Date, Firmware version
        @rtype: dict
        """
        self.ser.flushInput()
        self.ser.write("VE\r")
        self.info = {}
        info = self.ReadBuffer().strip().split(',')
        self.info['Copyright'] = info[0]
        self.info['Manufacturer'] = info[1]
        self.info['Firmware version'] = info[2]
        self.info['Date'] = info[3]
        info = self.info
        print "Copyright: %s\tManufacturer:   %s\nVersion:   %s\nDate:      %s" % (
                info['Copyright'], info['Manufacturer'], info['Firmware version'], info['Date'])
        return self.info
        
    def GetAddress(self):
        """
        Get Board Address

        @return: position
        @rtype: int
        """
        self.ser.flushInput()
        self.ser.write("TB\r")
        addr = self.ReadBuffer()
        print "Board Address: ", addr[:-1]
        addr = int(addr[2:-1])
        return addr
        
    def SetAddress(self, a=0):
        """
        Set address to address a certain controller in the chain
        
        @param v: address < 16
        @type v: int
        """
        self.ser.flushInput()
	if a >=0 and a <=9:
	    sel = chr(1)+chr(ord('0')+a)
	elif a >=10 and a <= 15:
	    sel = chr(1)+chr(ord('A')+a-10)
        self.ser.write(sel)
        self.ReadBuffer()
        
        
    def GetPosition(self):
        """
        Get stage for relative Position

        @return: position
        @rtype: int
        """
        self.ser.flushInput()
        self.ser.write("TP\r")
        pos = self.ReadBuffer()
        # print "Position: ", pos[:-1]
        pos = int(pos[2:-1])
        return pos

    def GetVelocity(self):
        """
        Returns programmed velocity setting
        
        @return: velocity
        @rtype: int
        """
        self.ser.flushInput()
        self.ser.write("TY\r")
        v = self.ReadBuffer()
        print "Velocity: " + v[:-1] + " steps/second"
        v = int(v[2:-1])
        return v

    def SetVelocity(self, v):
        """
        Set velocity of moving stage
        
        @param v: encoder counts per second, 0 < v < 500000
        @type v: int
        """
        v = max(1, v)
        v = min(500000, v)
        self.ser.flushInput()
        self.ser.write("SV"+str(v)+"\r")
        self.ReadBuffer()

    def SetAcceleration(self, a=150000):
        """
        Set acceleration for stage (de)accelerating from/to programmed
        velocity 
        
        @param a: set to acceleration a, whereas 200 < a < 1073741823
                    the value a is given in encoder counts per second squared.
                    Typical acceleration values range from 100000 to 2000000.
        @type a: int
        """
        a = max(200, a)
        a = min(1073741823, a)
        self.ser.flushInput()
        self.ser.write("SA"+str(a)+"\r")
        self.ReadBuffer()

    def ReadBuffer(self):
        """Read Serial Input, sent from linear stage"""
        time.sleep(0.1)
        return self.ser.read(self.ser.inWaiting())        
        
    def DefineHome(self):
        """Define new Home position"""
        self.ser.write("DH\r")
        self.ReadBuffer()
  
    def FindPositiveEdge(self, setHome = False):
        """Go to Positive Edge, Stop Motor
        
           @param setHome: Standard is False -- True sets this as new home position
           @type setHome: boolean
        """
        if(setHome):
            self.ser.write("FE0,WS100,WA500,DH,MN\r")
        else:
            self.ser.write("FE0,WS100,WA500,MN\r")
        self.ReadBuffer()
        self.flush()
        
    def FindNegativeEdge(self, setHome = False):
        """Go to Positive Edge, Stop Motor

           @param setHome: Standard is False -- True sets this as new home position
           @type setHome: boolean
        """
        if(setHome):
            self.ser.write("FE1,WS100,WA500,DH,MN\r")
        else:
            self.ser.write("FE1,WS100,WA500,MN\r")
        self.ReadBuffer()
        self.flush()      
        
    def StopSmoothly(self):
        """Stops current movement smoothly with programmed acceleration"""
        self.ser.write("AB1\r")
        self.ReadBuffer()
        
    def WaitUntilFinished(self):
        """Reads repetively Position Error until destination is reached"""
        Wait = True
        while(Wait):
            self.ser.flushInput()
            time.sleep(0.2)
            self.ser.write("TE\r")
            res = self.ReadBuffer()[2:13]
            print res, len(res)
            #print res == "+0000000000"
            if len(res) == 11 and (res == "+0000000000" or abs(int(res))<350):
                print "ok? ", res
                Wait = False
