# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 17:09:37 2016

@author: User
"""

import bin2histo_new_exp as b2h

import os

import numpy as np

from glob import glob

from time import clock

import matplotlib.pyplot as plt


path= r"D:\Datap620"

list_path= r"D:\Datap620"

name = "160425_110239___-1800000.0th_run_10sec.txt"

list_name= "actual_positions.txt"

#culprit = b2h.detective(path, name, count=-1)

coinc_interval = 50#10e-9/82.3045e-12   #Half of coincidence interval
    
deadtime = 100

coinc_peak_pos = 8570
    
false_peak_pos_1 = 7300

false_peak_pos_2 = 9844

#dead_interval = 3000

coinc_counter = 0

single_1_counter = 0

single_2_counter = 0


Coinc =np.array([])

Vis =np.array([])

Single_1 =np.array([])

Single_2 =np.array([])

   



pos_list = np.genfromtxt(os.path.join(list_path, list_name), comments='#', invalid_raise=True)

n = np.size(pos_list)
pos_list = pos_list[1:n-1]

#data  = np.genfromtxt(os.path.join(path, name), usecols=np.arange(0,13), comments='#', invalid_raise=True)

#       count = 0
#        filename_list_comment = ''
#        filelist = []
#        filelist_errorFiles = []

print 'walk'

print sorted(os.walk(path))



for subfolder in sorted(os.walk(path)):
    
    print 'subfolder'
    
    print subfolder[1]
    
    for k in np.arange(0, np.size(subfolder[1]), 1):
        
        print 'subfolder[k]'
        
        print subfolder[1][k]

        
    
        for pfilename in sorted(glob(os.path.join(path, subfolder[1][k], '*.bin'))):
            
            
            print 'name'
        
        
            name = os.path.split(pfilename)[1]
            
            print name
        #    filename_list_comment += '# ' + name + '\n'
        #    filelist.append(name)
        #    log.info("starting with file: %s  \n", name)
            t1 = clock()
            
            print 'Opening this'
            
            print os.path.join(path, subfolder[1][k], name)
            
        #    data  = np.genfromtxt(pfilename, usecols=np.arange(0,13), comments='#', invalid_raise=True)
            temp_coinc_counter, temp_single_1_counter, temp_single_2_counter  = b2h.get_coinc(os.path.join(path, subfolder[1][k]), name, coinc_interval, deadtime, coinc_peak_pos, false_peak_pos_1, false_peak_pos_2, count=-1)
                          

            
        #    ch_3_counter = np.sum(data[(peak_pos-coinc_window/2.):
        #                  (peak_pos+coinc_window/2.),4])
        #    
        #    ch_6_counter = np.sum(data[(peak_pos-coinc_window/2.):
        #                  (peak_pos+coinc_window/2.),5])
                          

            
            
            coinc_counter = coinc_counter + temp_coinc_counter
            
            single_1_counter = single_1_counter + temp_single_1_counter
            
            
            single_2_counter = single_2_counter + temp_single_2_counter
            
            print 'coinc'
            
            print coinc_counter
            
            print 'single 1'
            
            print single_1_counter
            
            
            print 'single 2'
            
            print single_2_counter
            
            
            
#            print 'coinc_counter'
#                     
#            print coinc_counter
            
#            print type(coinc_counter)
            
            
#            print 'single_counter'
#                     
#            print single_counter
#            
#            print type(single_counter)
            
            
#            
#            print 'Vis'
##            
#            print Vis
            
            
        if k == 0:
            
            base_single_1_counter =  single_1_counter
            
            base_single_2_counter =  single_2_counter
            
        
        Vis_1 = float(coinc_counter)/(single_1_counter**2*single_2_counter**2)*(base_single_1_counter**2*base_single_2_counter**2)
            
        Coinc = np.append(Coinc, coinc_counter)
        
        Single_1 = np.append(Single_1, single_1_counter)
        
        Single_2 = np.append(Single_2, single_2_counter)
        
        Vis =  np.append(Vis,Vis_1)
    
#        print 'appended vis'
#    
#        print float(coinc_counter)/single_counter
    
        print 'Vis'
        
        print Vis
        
        print 'Single_1'
        
        print Single_1
        
        print 'Single_2'
        
        print Single_2
        
        print 'base_single_1_counter'
        
        print base_single_1_counter
        
        
    
        coinc_counter = 0
        
        single_1_counter = 0
        
        single_2_counter = 0
        
#        single_counter = 0
    

    
    


    t2 = clock()
    print 'Time for loading data and creating one histo: '+str(t2-t1)+'s\n'
#    count += 1



print 'pos_list'

print pos_list

print 'Vis'

print Vis


print 'Coinc'

print Coinc

print 'Single_1'

print Single_1

print 'Single_2'

print Single_2


np.savetxt("Vis.txt", Vis)






plt.plot(pos_list/200000*4.6, Vis, color='b', label='Vis')

plt.plot(pos_list/200000*4.6, (Single_1/base_single_1_counter), color='r', label='rel. Singles 1')

plt.plot(pos_list/200000*4.6, (Single_2/base_single_2_counter), color='k', label='rel. Singles 2')

#plt.plot(pos_list/200000*4.6, Coinc, color='g', label='rel. Coinc')




#plt.plot(data[:,1], data[:,3] , color='r', label='ch_3')
#
#plt.plot(data[:,1], data[:,4] , color='k', label='ch_6')
###
#plt.plot(data[:,1], data[:,8] , color='b', label='coinc')
#
#plt.plot(data[:,1], data[:,11] , color='g', label='coinc')



plt.legend(loc=1)
        
plt.xlabel(r'delay / ps', fontsize=20)
plt.ylabel(r'events', fontsize=20)

plt.show()

#import numpy as np
#import scipy.special as sc
#import matplotlib.pyplot as plt
#import os
#
##Plotting binomial distribution
#
#path = r"C:\Users\User\Desktop\Measurements\160422\160422_162316___2x10s"
#praefix="160422_162317___th_run_10sec.txt"
#
#data  = np.genfromtxt(os.path.join(path, praefix), usecols=np.arange(0,13), comments='#', invalid_raise=True)
#
#bins = data[:,1]
#
#chan4 = data[:,4]
#
#print bins
#
#
#
#
##plt.plot(x1, s(n1, x1))
#
##plt.plot(PL, events(PL), color='b', label='events')
#
##plt.figure()
##
##plt.plot(steps, clicks(steps) , color='y', label='clicks depending on step number')
##
##
##
###plt.plot(PL, test(PL) , color='g', label='test')
##
###plt.plot(PL, false_events(PL), color='r', label='false events')
###
###plt.plot(PL, filtered_events(PL)*10, color='g', label='filtered events * 10')
###
#
#plt.plot(bins, chan4, color='k', label='Counts Ch 4')
#
#plt.legend(loc=1)
#        
#plt.xlabel(r'Bins', fontsize=20)
#plt.ylabel(r'events', fontsize=20)
#
#plt.show()