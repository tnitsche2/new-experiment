import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from time import strftime
from glob import glob
import logging
import logging.config
import logging.handlers
from time import clock

sys.path.append('//fs-cifs.uni-paderborn.de/upb/groups/iqo/data/Students/tnitsche/DataAnalysis')
#logging.config.fileConfig('loggingConfig.txt')

log = logging.getLogger("myLogger.bin2histo_new_exp")


class FuncFilter(logging.Filter):
    def __init__(self, name=None):
        pass

    def filter(self, rec):
        if rec.funcName in ['sum_over_grid', 'generate_errorbars']:
            return False
        else:
            return True

funcfilter = FuncFilter()
log.addFilter(funcfilter)
#########################################################################
# Begin of function declarations
#########################################################################


culprit =np.array([])



#def addevent(histovec, event, width_hist, min_hist):
#    """
#    adds an event (event[1] = channel, event[0] = timestamp distance to last
#    trigger to the histovec
#    """
#    histovec[(event[0]-min_hist)/width_hist, event[1]-1] += 1
#    return histovec
def sum_over_peak(histovec, peak_pos, peak_width):
    
    count_array = np.sum(histovec[peak_pos-(peak_width/2.):peak_pos+(peak_width/2.)], axis=0)
    
    return count_array
    
            


def create_comment(filename, comment):
    comment_list = comment.split('\n')
    comment = '# '
    for c in comment_list:
        comment += c
        comment += '\n#'
    basename = filename.split('_ca')
    binfilename = os.path.join(basename[0] + ".bin")
    return '#####################################\n# ' + \
        "File created from binary file " + binfilename + " at " +  \
        strftime("%y%m%d") + ' ' + strftime("%H%M%S") + '\n# ' + \
        'channel order 2,3,4,5' + '\n' + comment + \
        '###################################\n'


def create_filename_fromList(filelist):
    """
    Function to create a filename from a filelist of measurements performed at
    the same day with the same name.
    This function takes the earliest time as start time and the sum of
    the measuring times as meastime
    """
    meastime = 0
    starttimes = []
    for item in filelist:
        # find the measuringtime information
        ind1 = item.find('sec')
        i = 0
        while item.find('_', ind1-i, ind1) == -1:
            i += 1
            ind2 = item.find('_', ind1-i, ind1)
        timesec = int(item[ind2+1:ind1])
        meastime += timesec
        # find the earliest starting time
        ind3 = item.find('_')
        ind4 = item.find('_', ind3+1)
        starttime_value = int(item[ind3+1:ind4])
        starttimes.append(starttime_value)
    orig_file = os.path.splitext(filelist[-1])[0]  # based on last filename (xx run instead of x run)
    filename = orig_file[:ind3+1] + str(min(starttimes)) + \
        orig_file[ind4:ind2+1] + str(meastime) + orig_file[ind1:]
    return filename


def savehisto2txt(filename, data, comment):
    """
    Function to save the histogram data in a textfile called "filename" in the
    same style than the new tdc
    """
    try:
        target = open(filename, 'w')
#        print "Temporary file created!
        comment = create_comment(filename, comment)
        target.write(comment)
        for l in np.arange(np.size(data[:, 0])):    # run through all lines
            line = data[l, :]
            for item in line[1:]:   # run trough the items from 1 til last
                target.write(str(int(item)) + '\t')
            target.write('\n')
        target.close()
    except IOError:
        log.critical("Could not open file")
        sys.exit("could not open file")
#    print "txt file saved!"
        
        


def analyse(path, name, settings, count=-1):
    
    
#    connected_channels = [3,6]
    number_of_channels = 2
    timebin2bin = 1.            # number of tdc bins merged to one histogram binnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
    data_chan, data_time = bread(os.path.join(path, name), count=count)
    
#    data_time = data_time[:25000]
    
    trig_ind = np.where(data_chan == 0)[0]  # find trigger signals = channel 0
    
    trig_dif = data_time[trig_ind[1:]] - data_time[trig_ind[0:np.size(trig_ind)-1]]
    
    print trig_dif
    
   
        # max number of counts in one frame = between two triggers
    maxcounts = np.max(trig_ind[1:]-trig_ind[0:np.size(trig_ind)-1])
    
#    print "Trig_ind"
#    
#    print trig_ind
    
   
#    coinc_counter = 0
    
    data3_ind = np.where(data_chan==3)[0]
    
    data6_ind = np.where(data_chan==6)[0]
    

    
    
#   Applying dead time filter to Channel 3 
   
    testarray = np.zeros(np.size(data_chan))
    
    testarray[data3_ind] = data_time[data3_ind] 
    
#    print 'data3_ind'
#    
#    print data3_ind
    
    loopvar = True
    
    k = 1
    

#    print testarray
    while loopvar: 
        testarray2 =  np.concatenate((np.zeros(k), testarray[0:-k]))
        diffarray = testarray - testarray2
        #print "diffarrray" + str(diffarray)
        #if data_chan(np.where(diffarray < deadtime)) == data_chan(np.where(diffarray < deadtime)-k):
        index = np.where((diffarray<settings['deadtime']) & (diffarray >0))
#        print index
        data_chan[index]=8        
        data_time[index]=data_time[index]+4000
#        print 'filtered here 3'
#        print index
        k+=1
        if k==maxcounts +5:
            loopvar = False
            
    k=1
    
    
#   Applying dead time filter to Channel 6 
            
    testarray = np.zeros(np.size(data_chan))
    
    testarray[data6_ind] = data_time[data6_ind] 
    
#    print 'data6_ind'
#    
#    print data6_ind
    
    loopvar = True 
    k = 1
    
#    print testarray
    while loopvar: 
        testarray2 =  np.concatenate((np.zeros(k), testarray[0:-k]))
        diffarray = testarray - testarray2
        #print "diffarrray" + str(diffarray)
        #if data_chan(np.where(diffarray < deadtime)) == data_chan(np.where(diffarray < deadtime)-k):
        index = np.where((diffarray<settings['deadtime']) & (diffarray >0))
#        print index
        data_chan[index]=8        
        data_time[index]=data_time[index]+4000
#        print 'filtered here 6'
#        print index
        k+=1
        if k==maxcounts +5:
            loopvar = False
            
            
#    print 'data time'
#    
#    print data_time
    
    data3_ind = np.where(data_chan==3)[0]
    
    data6_ind = np.where(data_chan==6)[0]
    
#    print 'data3_ind'
#    
#    print data3_ind
#    
#    print 'data6_ind'
#    
#    print data6_ind
    

            
            
            
# Looking for 4-fold coincidences
            
            


    arr1 = np.zeros(np.size(data_chan))
#    trigg_diff = np.zeros(np.size(data_chan))
#    trigg_diff[trig_ind] = data_time[trig_ind] - data_time[trig_ind -1]
    arr1[trig_ind] = data_time[trig_ind]
#    print data_time
#    print arr1

    timestamps_mod = data_time-arr1
#    print timestamps_mod[:100]
    
    neg_ind = []
    
    
    for i in np.arange(1, maxcounts):
#        print timestamps_mod
        arr1 = np.concatenate((np.zeros(1), arr1[0:-1]))
#        print arr1
#        print '--------------------------'
        timestamps_mod = timestamps_mod-arr1
#        print timestamps_mod
        neg_ind = np.where(timestamps_mod < 0)
        if np.size(neg_ind) > 0:
            timestamps_mod[neg_ind] = 0   # replace in result negative values by 0
           # neg_ind = (neg_ind-np.zeros(np.size(neg_ind))).astype(int)
            arr1[neg_ind] = 0
#            print neg_ind
#            print arr1   

            

    
    arr2 = np.copy(timestamps_mod)
    sumarray = np.zeros(np.size(data_chan))
    
    
    
    for i in np.arange(1, maxcounts):
#        print timestamps_mod
        arr2 = np.concatenate((np.zeros(1), arr2[0:-1]))
#        print arr1
#        print '--------------------------'
        timestamps_mod2 = timestamps_mod-arr2
#        print timestamps_mod
        neg_ind = np.where(timestamps_mod2 < 0)
        if np.size(neg_ind) > 0:
              # replace in result negative values by 0
           # neg_ind = (neg_ind-np.zeros(np.size(neg_ind))).astype(int)
        
        
            
            herald_1_peak = np.where((-timestamps_mod2<(settings['herald_1_peak_pos']+settings['coinc_interval'])) &(-timestamps_mod2>(settings['herald_1_peak_pos']-settings['coinc_interval'])))
            herald_2_peak = np.where((-timestamps_mod2<(settings['herald_2_peak_pos']+settings['coinc_interval'])) &(-timestamps_mod2>(settings['herald_2_peak_pos']-settings['coinc_interval'])))
            step = np.where((-timestamps_mod2<(settings['step_pos']+settings['step_width'])) &(-timestamps_mod2>(settings['step_pos']-settings['step_width'])))
            

#            
            
            
#            peak_1 = np.where((-timestamps_mod2<(peak_1_pos+peak_width)) &(-timestamps_mod2>(peak_1_pos-peak_width)))
#            peak_2 = np.where((-timestamps_mod2<(peak_2_pos+peak_width)) &(-timestamps_mod2>(peak_2_pos-peak_width)))
#            peak_3 = np.where((-timestamps_mod2<(peak_3_pos+peak_width)) &(-timestamps_mod2>(peak_3_pos-peak_width)))
#            peak_4 = np.where((-timestamps_mod2<(peak_4_pos+peak_width)) &(-timestamps_mod2>(peak_4_pos-peak_width)))
#            peak_5 = np.where((-timestamps_mod2<(peak_5_pos+peak_width)) &(-timestamps_mod2>(peak_5_pos-peak_width)))
#            peak_6 = np.where((-timestamps_mod2<(peak_6_pos+peak_width)) &(-timestamps_mod2>(peak_6_pos-peak_width)))
#            peak_7 = np.where((-timestamps_mod2<(peak_7_pos+peak_width)) &(-timestamps_mod2>(peak_7_pos-peak_width)))
#            peak_8 = np.where((-timestamps_mod2<(peak_8_pos+peak_width)) &(-timestamps_mod2>(peak_8_pos-peak_width)))
#            peak_9 = np.where((-timestamps_mod2<(peak_9_pos+peak_width)) &(-timestamps_mod2>(peak_9_pos-peak_width)))
#            peak_10 = np.where((-timestamps_mod2<(peak_10_pos+peak_width)) &(-timestamps_mod2>(peak_10_pos-peak_width)))
     
#               print 'peak2'
#            print peak2
            if np.size(herald_1_peak)>0:
                sumarray[herald_1_peak] += 1000000
            if np.size(herald_2_peak)>0:
                sumarray[herald_2_peak] += 1000
            #if np.size(peak2b)>0:
            #    sumarray[peak2b] += 3                
            if np.size(step)>0:
                sumarray[step] += 2             
            #timestamps_mod2[neg_ind] = 0 
            arr2[neg_ind] = 0
    global debugarray 
    debugarray = sumarray
#    print 'coinc ...' 
    
    coinc = np.where(sumarray==1001004)
    
    print 'coinc'
    
    print coinc
    
    print 'Channels'
    
    print data_chan[coinc]
    
    singles_1 = np.where(sumarray==1000000   )
    single_1_counter = np.size(singles_1)  
    
    singles_2 = np.where(sumarray==1000)
    single_2_counter = np.size(singles_2)
    
    
    temp_coinc = coinc[0]
    

    
#    print 'temp_coinc'
#    
#    print temp_coinc
#    
#    print 'Coinc'
    
#    print coinc
#    
    shift_coinc = np.zeros(np.size(temp_coinc))
    
#    print 'size'
#    
#    print np.size(temp_coinc)
    
#    print 'arange'
#    
#    print np.arange(0, np.size(temp_coinc), 1)
    
    j=0
    n=0
    
#    print 'search here'
#    
#    print np.arange(0, np.size(temp_coinc), 1)
#    
#    print 'uuu'
#    
#    print 1/2
    
    while (j/2) in np.arange(0, np.size(temp_coinc), 1):
#        print 'j'
#        print (j/2)
#        print 'n'
#        print n
#        print np.int32(temp_coinc[(j/2)]-n)
               
        shift_coinc[(j/2)] = np.int32(temp_coinc[(j/2)]-n)
        
#        print 'data_chan'
#        
#        print data_chan[shift_coinc[j]]
        
        if (data_chan[shift_coinc[(j/2)]]==3 and (timestamps_mod[shift_coinc[(j/2)]]<(settings['step_pos']+settings['step_width'])) and (timestamps_mod[shift_coinc[(j/2)]]>(settings['step_pos']-settings['step_width']))):
            data_chan[shift_coinc[(j/2)]]=4
#            print 'new_data_chan'
#            print data_chan[shift_coinc[(j/2)]]  
            n=0
            j +=1   
#            print 'ok'
#            print j
#            print (j/2)
        elif (data_chan[shift_coinc[(j/2)]]==6 and (timestamps_mod[shift_coinc[(j/2)]]<(settings['step_pos']+settings['step_width'])) and (timestamps_mod[shift_coinc[(j/2)]]>(settings['step_pos']-settings['step_width']))):
            data_chan[shift_coinc[(j/2)]]=7
#            print 'new_data_chan'
#            print data_chan[shift_coinc[(j/2)]]
            n=0
            j +=1
#            print 'ok'
#            print j
#            print (j/2)
            
        elif (data_chan[shift_coinc[(j/2)]]==8 and (timestamps_mod[shift_coinc[(j/2)]]<(settings['step_pos']+settings['step_width'])) and (timestamps_mod[shift_coinc[(j/2)]]>(settings['step_pos']-settings['step_width']))):
            n += 1
            print 'oh fuck'
        else:
            n += 1
            
            if n >maxcounts:
                j += 1
                print 'This should not happen'
            
   

    
    
     # determine histogram size, max = consistent with new tdc-------------------
    min_hist = 0        # in unit: timestamps
#    max_hist = timebin2bin*100000-1   # for old fibres to compare with old analysis
    max_hist = np.ceil(np.mean(trig_dif))    # 494105  np.ceil(np.mean(trig_dif))   # new since 15-04-09 by Sonja
                        # new since 15-06-02 that all files for longfile have the same length
    
    print 'right border of histogram: ' + str(max_hist)
    nrofbins = np.ceil((max_hist-min_hist) / timebin2bin)
    print "nrofbins" 
    print nrofbins
    width_hist = (max_hist-min_hist) / nrofbins   # = timebin2bin
#    print ' width of histogram bins: ' + str(width_hist)
    histovec = np.zeros((nrofbins, number_of_channels+1))  # empty histogram for all 2 channels
    histovec[:, 0] = np.arange(0, nrofbins) * width_hist
    for i in np.arange(number_of_channels):
        selected_channels = [4,7]
        chan = selected_channels[i]
        chan_ind = np.where(data_chan == chan)[0]
        print 'channel'
        print chan
        print 'chan_ind'
        print chan_ind
        print ' time stamps'
        print timestamps_mod[chan_ind]
        hist, bin_edges = np.histogram(timestamps_mod[chan_ind],
                                       bins=nrofbins,
                                       range=(min_hist, max_hist))
        histovec[:, i+1] = hist   # fill result vector with histogram data
        
    
#    print 'histovec'    
#    print hist[chan_ind]
        
        
    return histovec, single_1_counter, single_2_counter
    
    
def analyse_2fold(path, name, settings, count=-1):
    
    
#    connected_channels = [3,6]
    number_of_channels = 2
    timebin2bin = 1.            # number of tdc bins merged to one histogram binnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
    data_chan, data_time = bread(os.path.join(path, name), count=count)
    
#    data_time = data_time[:25000]
    
    trig_ind = np.where(data_chan == 0)[0]  # find trigger signals = channel 0
    
    trig_dif = data_time[trig_ind[1:]] - data_time[trig_ind[0:np.size(trig_ind)-1]]
    
    print trig_dif
    
   
        # max number of counts in one frame = between two triggers
    maxcounts = np.max(trig_ind[1:]-trig_ind[0:np.size(trig_ind)-1])
    
#    print "Trig_ind"
#    
#    print trig_ind
    
   
#    coinc_counter = 0
    
    data3_ind = np.where(data_chan==3)[0]
    
    data6_ind = np.where(data_chan==6)[0]
    

    
    
#   Applying dead time filter to Channel 3 
   
    testarray = np.zeros(np.size(data_chan))
    
    testarray[data3_ind] = data_time[data3_ind] 
    
#    print 'data3_ind'
#    
#    print data3_ind
    
    loopvar = True
    
    k = 1
    

#    print testarray
    while loopvar: 
        testarray2 =  np.concatenate((np.zeros(k), testarray[0:-k]))
        diffarray = testarray - testarray2
        #print "diffarrray" + str(diffarray)
        #if data_chan(np.where(diffarray < deadtime)) == data_chan(np.where(diffarray < deadtime)-k):
        index = np.where((diffarray<settings['deadtime']) & (diffarray >0))
#        print index
        data_chan[index]=8        
        data_time[index]=data_time[index]+4000
#        print 'filtered here 3'
#        print index
        k+=1
        if k==maxcounts +5:
            loopvar = False
            
    k=1
    
    
#   Applying dead time filter to Channel 6 
            
    testarray = np.zeros(np.size(data_chan))
    
    testarray[data6_ind] = data_time[data6_ind] 
    
#    print 'data6_ind'
#    
#    print data6_ind
    
    loopvar = True 
    k = 1
    
#    print testarray
    while loopvar: 
        testarray2 =  np.concatenate((np.zeros(k), testarray[0:-k]))
        diffarray = testarray - testarray2
        #print "diffarrray" + str(diffarray)
        #if data_chan(np.where(diffarray < deadtime)) == data_chan(np.where(diffarray < deadtime)-k):
        index = np.where((diffarray<settings['deadtime']) & (diffarray >0))
#        print index
        data_chan[index]=8        
        data_time[index]=data_time[index]+4000
#        print 'filtered here 6'
#        print index
        k+=1
        if k==maxcounts +5:
            loopvar = False
            
            
#    print 'data time'
#    
#    print data_time
    
    data3_ind = np.where(data_chan==3)[0]
    
    data6_ind = np.where(data_chan==6)[0]
    
#    print 'data3_ind'
#    
#    print data3_ind
#    
#    print 'data6_ind'
#    
#    print data6_ind
    

            
            
            
# Looking for 2-fold coincidences
            
            


    arr1 = np.zeros(np.size(data_chan))
#    trigg_diff = np.zeros(np.size(data_chan))
#    trigg_diff[trig_ind] = data_time[trig_ind] - data_time[trig_ind -1]
    arr1[trig_ind] = data_time[trig_ind]
#    print data_time
#    print arr1

    timestamps_mod = data_time-arr1
#    print timestamps_mod[:100]
    
    neg_ind = []
    
    
    for i in np.arange(1, maxcounts):
#        print timestamps_mod
        arr1 = np.concatenate((np.zeros(1), arr1[0:-1]))
#        print arr1
#        print '--------------------------'
        timestamps_mod = timestamps_mod-arr1
#        print timestamps_mod
        neg_ind = np.where(timestamps_mod < 0)
        if np.size(neg_ind) > 0:
            timestamps_mod[neg_ind] = 0   # replace in result negative values by 0
           # neg_ind = (neg_ind-np.zeros(np.size(neg_ind))).astype(int)
            arr1[neg_ind] = 0
#            print neg_ind
#            print arr1   

            

    
    arr2 = np.copy(timestamps_mod)
    sumarray = np.zeros(np.size(data_chan))
    
    
    
    for i in np.arange(1, maxcounts):
#        print timestamps_mod
        arr2 = np.concatenate((np.zeros(1), arr2[0:-1]))
#        print arr1
#        print '--------------------------'
        timestamps_mod2 = timestamps_mod-arr2
#        print timestamps_mod
        neg_ind = np.where(timestamps_mod2 < 0)
        if np.size(neg_ind) > 0:
              # replace in result negative values by 0
           # neg_ind = (neg_ind-np.zeros(np.size(neg_ind))).astype(int)
        
        
            
            peak = np.where((-timestamps_mod2<(settings['peak_pos']+settings['coinc_interval'])) &(-timestamps_mod2>(settings['peak_pos']-settings['coinc_interval'])))
            
           
            
            if np.size(peak)>0:
                sumarray[peak] += 1
            #if np.size(peak2b)>0:
            #    sumarray[peak2b] += 3                       
            #timestamps_mod2[neg_ind] = 0 
            arr2[neg_ind] = 0
    global debugarray 
    debugarray = sumarray
#    print 'coinc ...' 
    
    coinc = np.where(sumarray==2)
    
    coinc_counter=np.size(coinc)  
    

    
        
    return coinc_counter
    


            
           
def generate_output_file(filename, result_lines):
    """
    Function to generate the outputfile, which can be read by the delay
    generator
    """
    with open(filename, 'w') as f:
        for lines in result_lines:
            f.write(str(lines))
        #write pulses to file
        f.closed           
    
    
    
    
        

def get_coinc(path, name, coinc_interval, deadtime, coinc_peak_pos, false_peak_pos_1, false_peak_pos_2, count=-1):
    
    
    connected_channels = [3,6]
    number_of_channels = 2
    timebin2bin = 1.            # number of tdc bins merged to one histogram binnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
    data_chan, data_time = bread(os.path.join(path, name), count=count)
    
    trig_ind = np.where(data_chan == 0)[0]  # find trigger signals = channel 0
    
    trig_dif = data_time[trig_ind[1:]] - data_time[trig_ind[0:np.size(trig_ind)-1]]
    
    print trig_dif
    
   
        # max number of counts in one frame = between two triggers
    maxcounts = np.max(trig_ind[1:]-trig_ind[0:np.size(trig_ind)-1])
    
#    print "Trig_ind"
#    
#    print trig_ind


    coinc_counter = 0
    
    data3_ind = np.where(data_chan==3)[0]
    
    data6_ind = np.where(data_chan==6)[0]
    
    
#   Applying dead time filter to Channel 3 
   
    testarray = np.zeros(np.size(data_chan))
    
    testarray[data3_ind] = data_time[data3_ind] 
    
    loopvar = True
    
    k = 1
    

#    print testarray
    while loopvar: 
        testarray2 =  np.concatenate((np.zeros(k), testarray[0:-k]))
        diffarray = testarray - testarray2
        #print "diffarrray" + str(diffarray)
        #if data_chan(np.where(diffarray < deadtime)) == data_chan(np.where(diffarray < deadtime)-k):
        index = np.where((diffarray<deadtime) & (diffarray >0))
#        print index
        data_chan[index]=8
        data_time[index]=data_time[index]+4000
        
        k+=1
        if k==maxcounts +5:
            loopvar = False
            
    k=1
    
    
#   Applying dead time filter to Channel 6 
            
    testarray = np.zeros(np.size(data_chan))
    
    testarray[data6_ind] = data_time[data6_ind] 
    
    loopvar = True 
    k = 1
    
#    print testarray
    while loopvar: 
        testarray2 =  np.concatenate((np.zeros(k), testarray[0:-k]))
        diffarray = testarray - testarray2
        #print "diffarrray" + str(diffarray)
        #if data_chan(np.where(diffarray < deadtime)) == data_chan(np.where(diffarray < deadtime)-k):
        index = np.where((diffarray<deadtime) & (diffarray >0))
#        print index
        data_chan[index]=8   
        data_time[index]=data_time[index]+4000
        
        k+=1
        if k==maxcounts +5:
            loopvar = False
    

            
            
            
# Looking for Singles in Channel 3 +6 
            
            
    arr1 = np.zeros(np.size(data_chan))
#    trigg_diff = np.zeros(np.size(data_chan))
#    trigg_diff[trig_ind] = data_time[trig_ind] - data_time[trig_ind -1]
    arr1[trig_ind] = data_time[trig_ind]
#    print data_time
#    print arr1

    timestamps_mod = data_time-arr1
#    print timestamps_mod[:100]
    neg_ind = []
#    print arr1        # error feedback----------------------------------------------------
#    if len(filelist_errorFiles) > 0:
#        print 'The following Files have not been converted due to errors in memmap'
#        print filelist_errorFiles
#
#    if np.sum(histo_sum) != -1:
#        #----------save histo-------------------------------------------------
#        listname = create_filename_fromList(filelist)
#        txtfilename = os.path.join(path, listname + "_calcfrombin.txt")
#        comment = 'Used function data2histo_arrayOp \n# Included the following files:\n'
#        comment = comment + filename_list_comment + '#'
#        savehisto2txt(txtfilename, histo_sum, comment)
    
    
    for i in np.arange(1, maxcounts):
#        print timestamps_mod
        arr1 = np.concatenate((np.zeros(1), arr1[0:-1]))
#        print arr1
#        print '--------------------------'
        timestamps_mod = timestamps_mod-arr1
#        print timestamps_mod
        neg_ind = np.where(timestamps_mod < 0)
        if np.size(neg_ind) > 0:
            timestamps_mod[neg_ind] = 0   # replace in result negative values by 0
           # neg_ind = (neg_ind-np.zeros(np.size(neg_ind))).astype(int)
            arr1[neg_ind] = 0
#            print neg_ind
#            print arr1   
            
#        print'############################'
    arr2 = np.copy(timestamps_mod)
    sumarray = np.zeros(np.size(data_chan))
    
    for i in np.arange(1, maxcounts):
#        print timestamps_mod
        arr2 = np.concatenate((np.zeros(1), arr2[0:-1]))
#        print arr1
#        print '--------------------------'
        timestamps_mod2 = timestamps_mod-arr2
#        print timestamps_mod
        neg_ind = np.where(timestamps_mod2 < 0)
        if np.size(neg_ind) > 0:
              # replace in result negative values by 0
           # neg_ind = (neg_ind-np.zeros(np.size(neg_ind))).astype(int)
            
            peak1 = np.where((-timestamps_mod2<(false_peak_pos_1+coinc_interval)) &(-timestamps_mod2>(false_peak_pos_1-coinc_interval)))
            peak2 = np.where((((-timestamps_mod2<(coinc_peak_pos+coinc_interval)) &(-timestamps_mod2>(coinc_peak_pos-coinc_interval)))))
            #peak2b = np.where(((-timestamps_mod2[neg_ind]<(coinc_peak_pos+coinc_interval)) &(-timestamps_mod2[neg_ind]>(coinc_peak_pos-coinc_interval)))&(data_chan[neg_ind-i]==6))
            peak3 = np.where((-timestamps_mod2<(false_peak_pos_2+coinc_interval)) &(-timestamps_mod2>(false_peak_pos_2-coinc_interval)))
#            print 'peak2'
#            print peak2
            if np.size(peak1)>0:
                sumarray[peak1] += 1000000
            if np.size(peak2)>0:
                sumarray[peak2] += 2
            #if np.size(peak2b)>0:
            #    sumarray[peak2b] += 3                
            if np.size(peak3)>0:
                sumarray[peak3] += 1000             
            #timestamps_mod2[neg_ind] = 0 
            arr2[neg_ind] = 0
    global debugarray 
    debugarray = sumarray
#    print 'coinc ...'    
    coinc = np.where(sumarray==1001004)
#    print coinc    
    
    print 'coinc'
    
    print coinc
    
    print 'Channels'
    
    print data_chan[coinc]
    
    coinc_counter=np.size(coinc)     
    
    singles_1 = np.where(sumarray==1000000   )
    single_1_counter = np.size(singles_1)  
    
    singles_2 = np.where(sumarray==1000)
    single_2_counter = np.size(singles_2)
    
#    print timestamps_mod[np.where(timestamps_mod != 0)]
 # determine histogram size, max = consistent with new tdc-------------------
#    min_hist = 0        # in unit: timestamps
##    max_hist = timebin2bin*100000-1   # for old fibres to compare with old analysis
#    max_hist = np.ceil(np.mean(trig_dif))    # 494105  np.ceil(np.mean(trig_dif))   # new since 15-04-09 by Sonja
#                        # new since 15-06-02 that all files for longfile have the same length
##    culprit = np.where(trig_dif > 13000)
#    
##    print 'culprit'
##    
##    print culprit
#    
##    print 'Std(trig_diff)'
##    
##    print np.std(trig_dif)
#
##    print 'right border of histogram: ' + str(max_hist)
#    nrofbins = np.ceil((max_hist-min_hist) / timebin2bin)
##    print "nrofbins" 
##    print nrofbins
#    width_hist = (max_hist-min_hist) / nrofbins   # = timebin2bin
##    print ' width of histogram bins: ' + str(width_hist)
#    histovec = np.zeros((nrofbins, number_of_channels+1))  # empty histogram for all 2 channels
#    histovec[:, 0] = np.arange(0, nrofbins) * width_hist
#    for i in np.arange(number_of_channels):
#        chan = connected_channels[i]
#        chan_ind = np.where(data_chan == chan)[0]
#        hist, bin_edges = np.histogram(timestamps_mod[chan_ind],
#                                       bins=nrofbins,
#                                       range=(min_hist, max_hist))
#        histovec[:, i+1] = hist   # fill result vector with histogram data
#        
##  Sum Singles in Channel 3
#        
#    ch_3_counter = np.sum(histovec[(coinc_peak_pos-coinc_interval/2):
#                                (coinc_peak_pos+coinc_interval/2), 1])
#                                
##  Sum Singles in Channel 6
#                                
#    ch_6_counter = np.sum(histovec[(coinc_peak_pos-coinc_interval/2):
#                                (coinc_peak_pos+coinc_interval/2), 2])
#            
            
#    ch_6_counter = 0
#    
#    arr1[trig_ind] = data_time[trig_ind]
            
            

    
#    k = 0
#    
##    print testarray
#    while loopvar: 
#        arr1 = np.concatenate((np.zeros(1), arr1[0:-1]))
#        diffarray = diffarray - arr1
#        #print "diffarrray" + str(diffarray)
#        #if data_chan(np.where(diffarray < deadtime)) == data_chan(np.where(diffarray < deadtime)-k):
##        if ((diffarray - offset <coinc_interval) & (diffarray >0)):
##            ch_6_counter+=1
##        print index
#        
#        k+=1
#        if k==maxcounts:
#            loopvar = False
#            
#        neg_ind = np.where(diffarray < 0)
#        
#        if np.size(neg_ind) > 0:
#            diffarray[neg_ind] = 0   # replace in result negative values by 0
#           # neg_ind = (neg_ind-np.zeros(np.size(neg_ind))).astype(int)
#            arr1[neg_ind] = 0
#            
#            
#        
#            
#        condition = np.abs(diffarray- offset) < coinc_interval
#        ch_3_array = np.extract(condition, diffarray)
#        ch_3_counter = np.size(ch_3_array)
        

#        print timestamps_mod


#    coinc_counter = 1

            

            
        
           
    return coinc_counter, single_1_counter, single_2_counter
            
            
            
#def detective(path, name, count=-1):
#    
#
#    
#    connected_channels = [3,6]
#    number_of_channels = 2
#    timebin2bin = 1.            # number of tdc bins merged to one histogram binnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
#    data_chan, data_time = bread(os.path.join(path, name), count=count)
#    
#    trig_ind = np.where(data_chan == 0)[0]  # find trigger signals = channel 0
#    
#    trig_dif = data_time[trig_ind[1:]] - data_time[trig_ind[0:np.size(trig_ind)-1]]
#    
#        # max number of counts in one frame = between two triggers
#    maxcounts = np.max(trig_ind[1:]-trig_ind[0:np.size(trig_ind)-1])
#    
##    print "Trig_ind"
##    
##    print trig_ind
#
#    # max number of counts in one frame = between two triggers
#    maxcounts = np.max(trig_ind[1:]-trig_ind[0:np.size(trig_ind)-1])
#
#    
##    print timestamps_mod[np.where(timestamps_mod != 0)]
# # determine histogram size, max = consistent with new tdc-------------------
#    min_hist = 0        # in unit: timestamps
##    max_hist = timebin2bin*100000-1   # for old fibres to compare with old analysis
#    max_hist = np.ceil(np.mean(trig_dif))    # 494105  np.ceil(np.mean(trig_dif))   # new since 15-04-09 by Sonja
#                        # new since 15-06-02 that all files for longfile have the same length
#    culprit = np.where(trig_dif > 13000)
#    
#    print 'culprit'
#    
#    print culprit
#    
#    print 'Std(trig_diff)'
#    
#    print np.std(trig_dif)
#    
#    return culprit

    
def readData_createHisto(path, name, count=-1):   # modified by Georg on 20160222; including previous data2histo_array0p
    connected_channels = [3,6]
    number_of_channels = 2
    timebin2bin = 1.            # number of tdc bins merged to one histogram binnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
    data_chan, data_time = bread(os.path.join(path, name), count=count)
    
    trig_ind = np.where(data_chan == 0)[0]  # find trigger signals = channel 0
    
    trig_dif = data_time[trig_ind[1:]] - data_time[trig_ind[0:np.size(trig_ind)-1]]
    
    print trig_dif
    global test
    test = trig_dif
#    print "Trig_ind"
#    
#    print trig_ind

    # max number of counts in one frame = between two triggers
    maxcounts = np.max(trig_ind[1:]-trig_ind[0:np.size(trig_ind)-1])
    
#    print "max counts"
#    
#    print maxcounts
#    

#    loopvar = True
#    k=1
#    deadtime = 500e-9/82.3045e-12
#    
#    data3_ind = np.where(data_chan==3)[0]
#    
#    data6_ind = np.where(data_chan==6)[0]
#    
#    testarray = np.zeros(np.size(data_chan))
#    
#    testarray[data3_ind] = data_time[data3_ind] 
##    print testarray
#    while loopvar: 
#        testarray2 =  np.concatenate((np.zeros(k), testarray[0:-k]))
#        diffarray = testarray - testarray2
#        #print "diffarrray" + str(diffarray)
#        #if data_chan(np.where(diffarray < deadtime)) == data_chan(np.where(diffarray < deadtime)-k):
#        index = np.where((diffarray<deadtime) & (diffarray >0))
##        print index
#        data_chan[index]=8        
#        
#        k+=1
#        if k==maxcounts +5:
#            loopvar = False
#            
#    k=1
#            
#    testarray = np.zeros(np.size(data_chan))
#            
#            
#    testarray[data6_ind] = data_time[data6_ind] 
##    print testarray
#    while loopvar: 
#        testarray2 =  np.concatenate((np.zeros(k), testarray[0:-k]))
#        diffarray = testarray - testarray2
#        #print "diffarrray" + str(diffarray)
#        #if data_chan(np.where(diffarray < deadtime)) == data_chan(np.where(diffarray < deadtime)-k):
#        index = np.where((diffarray<deadtime) & (diffarray >0))
##        print index
#        data_chan[index]=8        
#        
#        k+=1
#        if k==maxcounts +5:
#            loopvar = False
#        else:
#            loopvar= False
    # ----calculate the difference of every click to its corresponding trigger
    # array with the triggertimes where the triggers are and 0 else
    arr1 = np.zeros(np.size(data_chan))
#    trigg_diff = np.zeros(np.size(data_chan))
#    trigg_diff[trig_ind] = data_time[trig_ind] - data_time[trig_ind -1]
    arr1[trig_ind] = data_time[trig_ind]
#    print data_time
#    print arr1

    timestamps_mod = data_time-arr1
#    print timestamps_mod[:100]
    neg_ind = []
#    print arr1
    for i in np.arange(1, maxcounts):
#        print timestamps_mod
        arr1 = np.concatenate((np.zeros(1), arr1[0:-1]))
#        print arr1
#        print '--------------------------'
        timestamps_mod = timestamps_mod-arr1
#        print timestamps_mod
        neg_ind = np.where(timestamps_mod < 0)
        if np.size(neg_ind) > 0:
            timestamps_mod[neg_ind] = 0   # replace in result negative values by 0
           # neg_ind = (neg_ind-np.zeros(np.size(neg_ind))).astype(int)
            arr1[neg_ind] = 0
#            print neg_ind
#            print arr1   
#        print'############################'
    
#    print timestamps_mod[np.where(timestamps_mod != 0)]
 # determine histogram size, max = consistent with new tdc-------------------
    min_hist = 0        # in unit: timestamps
#    max_hist = timebin2bin*100000-1   # for old fibres to compare with old analysis
    max_hist = np.ceil(np.mean(trig_dif))    # 494105  np.ceil(np.mean(trig_dif))   # new since 15-04-09 by Sonja
                        # new since 15-06-02 that all files for longfile have the same length
    
    
    print 'Std(trig_diff)'
    
    print np.std(trig_dif)
    
    print 'mean trip_diff'
    
    print np.mean(trig_dif)
    
    culprit = np.where(trig_dif > 13000)
    
    print 'culprit'
    
    print culprit

    print 'right border of histogram: ' + str(max_hist)
    nrofbins = np.ceil((max_hist-min_hist) / timebin2bin)
    print "nrofbins" 
    print nrofbins
    width_hist = (max_hist-min_hist) / nrofbins   # = timebin2bin
#    print ' width of histogram bins: ' + str(width_hist)
    histovec = np.zeros((nrofbins, number_of_channels+1))  # empty histogram for all 2 channels
    histovec[:, 0] = np.arange(0, nrofbins) * width_hist
    for i in np.arange(number_of_channels):
        chan = connected_channels[i]
        chan_ind = np.where(data_chan == chan)[0]
        hist, bin_edges = np.histogram(timestamps_mod[chan_ind],
                                       bins=nrofbins,
                                       range=(min_hist, max_hist))
        histovec[:, i+1] = hist   # fill result vector with histogram data
    return histovec
    
def readData_createHisto_with_deadtime_filter(path, name, count=-1):   # modified by Georg on 20160222; including previous data2histo_array0p
    connected_channels = [3,6]
    number_of_channels = 2
    timebin2bin = 1. 
    deadtime=100           # number of tdc bins merged to one histogram binnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
    data_chan, data_time = bread(os.path.join(path, name), count=count)
    
    trig_ind = np.where(data_chan == 0)[0]  # find trigger signals = channel 0
    
    trig_dif = data_time[trig_ind[1:]] - data_time[trig_ind[0:np.size(trig_ind)-1]]
    
    print trig_dif
    global test
    test = trig_dif
#    print "Trig_ind"
#    
#    print trig_ind

    # max number of counts in one frame = between two triggers
    maxcounts = np.max(trig_ind[1:]-trig_ind[0:np.size(trig_ind)-1])
    
    data3_ind = np.where(data_chan==3)[0]
    
    data6_ind = np.where(data_chan==6)[0]
    

    
    
#   Applying dead time filter to Channel 3 
   
    testarray = np.zeros(np.size(data_chan))
    
    testarray[data3_ind] = data_time[data3_ind] 
    
#    print 'data3_ind'
#    
#    print data3_ind
    
    loopvar = True
    
    k = 1
    

#    print testarray
    while loopvar: 
        testarray2 =  np.concatenate((np.zeros(k), testarray[0:-k]))
        diffarray = testarray - testarray2
        #print "diffarrray" + str(diffarray)
        #if data_chan(np.where(diffarray < deadtime)) == data_chan(np.where(diffarray < deadtime)-k):
        index = np.where((diffarray<deadtime) & (diffarray >0))
#        print index
        data_chan[index]=8        
        data_time[index]=data_time[index]+4000
#        print 'filtered here 3'
#        print index
        k+=1
        if k==maxcounts +5:
            loopvar = False
            
    k=1
    
    
#   Applying dead time filter to Channel 6 
            
    testarray = np.zeros(np.size(data_chan))
    
    testarray[data6_ind] = data_time[data6_ind] 
    
#    print 'data6_ind'
#    
#    print data6_ind
    
    loopvar = True 
    k = 1
    
#    print testarray
    while loopvar: 
        testarray2 =  np.concatenate((np.zeros(k), testarray[0:-k]))
        diffarray = testarray - testarray2
        #print "diffarrray" + str(diffarray)
        #if data_chan(np.where(diffarray < deadtime)) == data_chan(np.where(diffarray < deadtime)-k):
        index = np.where((diffarray<deadtime) & (diffarray >0))
#        print index
        data_chan[index]=8        
        data_time[index]=data_time[index]+4000
#        print 'filtered here 6'
#        print index
        k+=1
        if k==maxcounts +5:
            loopvar = False
            
            
#    print 'data time'
#    
#    print data_time
    # ----calculate the difference of every click to its corresponding trigger
    # array with the triggertimes where the triggers are and 0 else
    arr1 = np.zeros(np.size(data_chan))
#    trigg_diff = np.zeros(np.size(data_chan))
#    trigg_diff[trig_ind] = data_time[trig_ind] - data_time[trig_ind -1]
    arr1[trig_ind] = data_time[trig_ind]
#    print data_time
#    print arr1

    timestamps_mod = data_time-arr1
#    print timestamps_mod[:100]
    neg_ind = []
#    print arr1
    for i in np.arange(1, maxcounts):
#        print timestamps_mod
        arr1 = np.concatenate((np.zeros(1), arr1[0:-1]))
#        print arr1
#        print '--------------------------'
        timestamps_mod = timestamps_mod-arr1
#        print timestamps_mod
        neg_ind = np.where(timestamps_mod < 0)
        if np.size(neg_ind) > 0:
            timestamps_mod[neg_ind] = 0   # replace in result negative values by 0
           # neg_ind = (neg_ind-np.zeros(np.size(neg_ind))).astype(int)
            arr1[neg_ind] = 0
#            print neg_ind
#            print arr1   
#        print'############################'
    
#    print timestamps_mod[np.where(timestamps_mod != 0)]
 # determine histogram size, max = consistent with new tdc-------------------
    min_hist = 0        # in unit: timestamps
#    max_hist = timebin2bin*100000-1   # for old fibres to compare with old analysis
    max_hist = np.ceil(np.mean(trig_dif))    # 494105  np.ceil(np.mean(trig_dif))   # new since 15-04-09 by Sonja
                        # new since 15-06-02 that all files for longfile have the same length
    
    
    print 'Std(trig_diff)'
    
    print np.std(trig_dif)
    
    print 'mean trip_diff'
    
    print np.mean(trig_dif)
    
    culprit = np.where(trig_dif > 13000)
    
    print 'culprit'
    
    print culprit

    print 'right border of histogram: ' + str(max_hist)
    nrofbins = np.ceil((max_hist-min_hist) / timebin2bin)
    print "nrofbins" 
    print nrofbins
    width_hist = (max_hist-min_hist) / nrofbins   # = timebin2bin
#    print ' width of histogram bins: ' + str(width_hist)
    histovec = np.zeros((nrofbins, number_of_channels+1))  # empty histogram for all 2 channels
    histovec[:, 0] = np.arange(0, nrofbins) * width_hist
    for i in np.arange(number_of_channels):
        chan = connected_channels[i]
        chan_ind = np.where(data_chan == chan)[0]
        hist, bin_edges = np.histogram(timestamps_mod[chan_ind],
                                       bins=nrofbins,
                                       range=(min_hist, max_hist))
        histovec[:, i+1] = hist   # fill result vector with histogram data
    return histovec
#def read_memmap(binfilename, limit):
#    
#    
#    """
#    creates a memory map to a large file on disk
#    """
#    log.debug("Creating Memory Map")
#    dt = np.dtype([('channel', "u8"), ('slope', "int"), ('timetag', "int")])
#    try:
#        bread_data = bread(binfilename)
#        print 'ok'
#        data = np.memmap(bread_data, mode='r')
#        print 'ok'
#        s = np.size(data)
#        print s
#        if limit != -1:
#            data = data[limit[0]:limit[1]]
#    except:   # such an error occurs if the file is too large or there is something wrong with the dtype, maybe due to data loss
#        name = os.path.split(binfilename)[1]
#        log.warn("Error creating memmap of file " + name)
#        return -1, -1, -1
#    data = data.astype([('time', 'i8'), ('channel', 'i8')], copy=False)
#    log.debug("Map created")
#    return np.asarray(data['time'], dtype=np.int64), \
#        np.asarray(data['channel'], dtype=np.int8), s

    
    
def bread(fname, count=-1):
    """
    Reads data from a binary file taken with the TTM8000 in I-Mode flat64
    
    Input:
       fname: 
           filename of the datafile
       count: 
           take only the first count timetags; -1 means take all
       slope: 
           return the slope of each trigger? Saves some time if False
    Return:
        tuple of numpy arrrays (channel, slope, timetag) or (channel, timetag) with
            channel: 
                uint8
            timetag: 
                uint64 in units of 82.3045e-12 seconds
    """
    
    
    #Version for 64-bit system
#    
#    f = open(fname, 'rb')
#    f.read(30)  # skip 76bytes of header
#    f.read(300) # skip 20 events because the recording program seems to write events from previous measurments at the beginning
#    data = np.fromfile(f, dtype='u8', count=count)  # u8 means uint64 means 64bits: 3bits channel, 1bit slope, 60bits time
#    f.close()
#    return np.array(data/2**61, dtype='u1'), np.mod(data, 2**60)
    
    # Version for 32-bit system
    
    f = open(fname, 'rb')
    f.read(30)  # skip 76bytes of header
    #f.read(300) # skip 20 events because the recording program seems to write events from previous measurments at the beginning
    data = np.fromfile(f, dtype='u8', count=count)  # u8 means uint64 means 64bits: 3bits channel, 1bit slope, 60bits time
    f.close()
    return np.array(data/2**61, dtype='u1'), np.mod(data, 2**60)



def main(argv):
    """np.mean(trig_dif)
    chose one of the options:
    file -> converts a single file to a histo
    path -> converts each file in this folder to a histo
    longfile -> converts all files in this folder and saves the histo sum
    """

    path_or_file = 'path'  # 'file', 'path', 'longfile'
    path = r"\\131.234.203.7\QW_Data\160818\Convert"

    name = "160706_093234_stay_calm_1sec.bin"
    

    count = -1

    if path_or_file == 'file':
    # convert a single bin-file to a histogram-file###########################
        log.info("Loading file: %s", name)
        t1 = clock()
        histo = readData_createHisto_with_deadtime_filter(path, name, count=count)
        if np.sum(histo) == -1:  # if there was an error
            return
        t2 = clock()
        print '\nTime for loading data and creating the histo: '+str(t2-t1)+'s'

        #----plot histogram------------------------------------------------------
#        timeconst = 161.91e-12   # [s] per doublebin for new tdc
#        histo[:, 0] = histo[:, 0] * timeconst * 1e6  # time in micro sec
#        plothisto(histo, name)

        #----------save histo----------------------------------------------------
        txtfilename = os.path.join(path, os.path.splitext(name)[0] +
                                   "_calcfrombin.txt")
        comment = 'used function data2histo_arrayOp'
        savehisto2txt(txtfilename, histo, comment)
        del histo

    elif path_or_file == 'path':
    # convert all bin-Files in the folder path to histogram-files##########
        filelist_errorFiles = []
        for pfilename in glob(os.path.join(path, '*.bin'))[::-1]:
            name = os.path.split(pfilename)[1]
            log.info("starting with file: %s \n", name)
            t1 = clock()
            histo = readData_createHisto_with_deadtime_filter(path, name)
            if np.sum(histo) == -1:  # if there was an error
                filelist_errorFiles.append(name)
                continue
            t2 = clock()
            print 'Time for loading data and creating the histo: '+str(t2-t1)+'s\n'

            #----------save histo---------------------------------------------
            txtfilename = os.path.join(path, os.path.splitext(pfilename)[0] +
                                       "_calcfrombin.txt")
            comment = 'used function data2histo_arrayOp'
            savehisto2txt(txtfilename, histo, comment)
            del histo
         # error feedback----------------------------------------------------
        if len(filelist_errorFiles) > 0:
            print 'The following Files have not been converted due to errors in memmap'
            print filelist_errorFiles

    elif path_or_file == 'longfile':
    # loads all the single files in a path and makes only one histogram file out of them
        count = 0
        filename_list_comment = ''
        filelist = []
        filelist_errorFiles = []
        for pfilename in glob(os.path.join(path, '*.bin')):
            name = os.path.split(pfilename)[1]
            filename_list_comment += '# ' + name + '\n'
            filelist.append(name)
            log.info("starting with file: %s  \n", name)
            t1 = clock()
            if ('histo_sum' in locals()) == False:   # is variable already existing? = first valid file
                histo_sum = readData_createHisto_with_deadtime_filter(path, name)
                if np.sum(histo_sum) == -1:  # if there was an error
                    print 'this file is not considered for histogram! \n'
                    filelist_errorFiles.append(name)
                    continue
            else:    # sum up for all other file
                histo_tmp = readData_createHisto_with_deadtime_filter(path, name)
                if np.sum(histo_tmp) == -1:  # if there was an error
                    print 'this file is not considered for histogram! \n'
                    filelist_errorFiles.append(name)
                    continue
                histo_sum += histo_tmp
            t2 = clock()
            print 'Time for loading data and creating one histo: '+str(t2-t1)+'s\n'
            count += 1

        # error feedback----------------------------------------------------
        if len(filelist_errorFiles) > 0:
            print 'The following Files have not been converted due to errors in memmap'
            print filelist_errorFiles

        if np.sum(histo_sum) != -1:
            #----------save histo-------------------------------------------------
            listname = create_filename_fromList(filelist)
            txtfilename = os.path.join(path, listname + "_calcfrombin.txt")
            comment = 'Used function data2histo_arrayOp \n# Included the following files:\n'
            comment = comment + filename_list_comment + '#'
            savehisto2txt(txtfilename, histo_sum, comment)

            #----plot histogram---------------------------------------------------
    #        timeconst = 161.91e-12   # [s] per doublebin for new tdc
    #        histo_sum[:, 0] = histo_sum[:, 0] * timeconst * 1e6  # time [microsec]
    #        plothisto(histo_sum, listname)
            del histo_sum

    elif path_or_file == 'longfile_subfolder':
    # goes through all subfolders in path and loads all the single files per
    # subfolder in one histogram file out of them
        subdir_list = os.listdir(path)
        for subdir in subdir_list:   #run through all subdirectories in path
            count = 0
            filename_list_comment = ''
            filelist = []
            filelist_errorFiles = []
            subpath = os.path.join(path, subdir)
            print 'actual subdirectory' + subdir
            for pfilename in glob(os.path.join(subpath, '*.bin')):
                name = os.path.split(pfilename)[1]
                filename_list_comment += '# ' + name + '\n'
                filelist.append(name)
                log.info("starting with file: %s  \n", name)
                t1 = clock()
                if ('histo_sum' in locals()) == False:   # is variable already existing? = first valid file
                    histo_sum = readData_createHisto_with_deadtime_filter(subpath, name)
                    if np.sum(histo_sum) == -1:  # if there was an error
                        print 'this file is not considered for histogram! \n'
                        filelist_errorFiles.append(name)
                        continue
                else:    # sum up for all other file
                    histo_tmp = readData_createHisto_with_deadtime_filter(subpath, name)
                    if np.sum(histo_tmp) == -1:  # if there was an error
                        print 'this file is not considered for histogram! \n'
                        filelist_errorFiles.append(name)
                        continue
                    histo_sum += histo_tmp
                t2 = clock()
                print 'Time for loading data and creating one histo: '+str(t2-t1)+'s\n'
                count += 1

            # error feedback----------------------------------------------------
            if len(filelist_errorFiles) > 0:
                print 'The following Files have not been converted due to errors in memmap'
                print filelist_errorFiles

            if ('histo_sum' in locals()) == True and np.sum(histo_sum) != -1:
                #----------save histo-------------------------------------------------
                listname = create_filename_fromList(filelist)
                txtfilename = os.path.join(path, listname + "_calcfrombin.txt")
                comment = 'Used function data2histo_arrayOp \n# Included the following files:\n'
                comment = comment + filename_list_comment + '#'
                savehisto2txt(txtfilename, histo_sum, comment)
                del histo_sum


if __name__ == "__main__":
    main(sys.argv[1:])
