# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 17:09:37 2016

@author: User
"""

import bin2histo_new_exp as b2h

import os

import numpy as np

from glob import glob

from time import clock




path= r"D:\160719"


settings = dict(

    coinc_interval=50,     #Half of coincidence interval
    
    step_width=50,
    
    herald_1_peak_pos = 7313,#7295,#7340,#7360,#7280,#7360,#7270,
    
    herald_2_peak_pos = 9857,#9841,#9905,#9827,#9905, #9810,
    
    step_pos = 8585,#8569,#8630,#8555,#8630, #8540,
    
#    peak_1_pos = 4,
#    
#    peak_2_pos = 5,
#    
#    peak_3_pos = 6,
#    
#    peak_4_pos = 7,
#    
#    peak_5_pos = 8,
#    
#    peak_6_pos = 9,
#    
#    peak_7_pos = 10,
#    
#    peak_8_pos = 11,
#    
#    peak_9_pos = 12,
#    
#    peak_10_pos = 13,
    
#    coinc_interval = 14,
    
    deadtime = 100,
    
#    coinc_peak_pos = 16,
#  
#    dead_interval = 17,
    

)

   


#data  = np.genfromtxt(os.path.join(path, name), usecols=np.arange(0,13), comments='#', invalid_raise=True)

#       count = 0
#        filename_list_comment = ''
#        filelist = []
#        filelist_errorFiles = []

print 'walk'

print sorted(os.walk(path))

filelist_errorFiles = []

Single_1 =[]

Single_2 =[]

data_time=np.array([])




for subfolder in sorted(os.walk(path)):
    
    count = 0
    filename_list_comment = ''
    filelist = []
    filelist_errorFiles = []
    
    print 'subfolder'
    
    print subfolder[1]
    
    for k in np.arange(0, np.size(subfolder[1]), 1):
        
        print 'subfolder[k]'
        
        print subfolder[1][k]

        
    
        for pfilename in sorted(glob(os.path.join(path, subfolder[1][k], '*.bin'))):
            
            
            print 'name'
        
        
            name = os.path.split(pfilename)[1]
            
            print name
        #    filename_list_comment += '# ' + name + '\n'
        #    filelist.append(name)
        #    log.info("starting with file: %s  \n", name)
            t1 = clock()
            
            print 'Opening this'
            
            print os.path.join(path, subfolder[1][k], name)
            
        #    data  = np.genfromtxt(pfilename, usecols=np.arange(0,13), comments='#', invalid_raise=True)
        #    temp_coinc_counter, temp_single_1_counter, temp_single_2_counter  = b2h.get_coinc(os.path.join(path, subfolder[1][k]), name, coinc_interval, deadtime, coinc_peak_pos, false_peak_pos_1, false_peak_pos_2, dead_interval, count=-1)
                          

            
        #    ch_3_counter = np.sum(data[(peak_pos-coinc_window/2.):
        #                  (peak_pos+coinc_window/2.),4])
        #    
        #    ch_6_counter = np.sum(data[(peak_pos-coinc_window/2.):
        #                  (peak_pos+coinc_window/2.),5])
                          

            
            
#            coinc_counter = coinc_counter + temp_coinc_counter
#            
#            single_1_counter = single_1_counter + temp_single_1_counter
#            
#            
#            single_2_counter = single_2_counter + temp_single_2_counter
#            
#            print 'coinc'
#            
#            print coinc_counter
#            
#            print 'single 1'
#            
#            print single_1_counter
#            
#            
#            print 'single 2'
#            
#            print single_2_counter
            
#                    count = 0

            if ('histo_sum' in locals()) == False:   # is variable already existing? = first valid file
                histo_sum, single_1_counter, single_2_counter = b2h.analyse(os.path.join(path, subfolder[1][k]), name, settings)
                if np.sum(histo_sum) == -1:  # if there was an error
                    print 'this file is not considered for histogram! \n'
                    filelist_errorFiles.append(name)
                    continue
            else:    # sum up for all other file
                histo_tmp, single_1_counter_tmp, single_2_counter_tmp = b2h.analyse(os.path.join(path, subfolder[1][k]), name, settings)
                if np.sum(histo_tmp) == -1:  # if there was an error
                    print 'this file is not considered for histogram! \n'
                    filelist_errorFiles.append(name)
                    continue
                histo_sum += histo_tmp
                single_1_counter += single_1_counter_tmp
                single_2_counter += single_2_counter_tmp
            t2 = clock()
            print 'Time for loading data and creating one histo: '+str(t2-t1)+'s\n'
            
#            print histo_sum
            
#            os.remove(os.path.join(path, subfolder[1][k], name))
#            count += 1


            # error feedback----------------------------------------------------
        if len(filelist_errorFiles) > 0:
            print 'The following Files have not been converted due to errors in memmap'
            print filelist_errorFiles

        if np.sum(histo_sum) != -1:
            #----------save histo-------------------------------------------------
#            listname = b2h.create_filename_fromList(filelist)
            txtfilename = os.path.join(path, subfolder[1][k] + "_calcfrombin.txt")
            comment = 'Used function data2histo_arrayOp \n# Included the following files:\n'
#            comment = comment + filename_list_comment + '#'
            b2h.savehisto2txt(txtfilename, histo_sum, comment)
            
            print 'single_1_counter'
            
            print single_1_counter
            
            print 'single_2_counter'
            
            print single_2_counter
            
            Single_1 = np.append(Single_1, single_1_counter)
            
            Single_2 = np.append(Single_2, single_2_counter)
                
            
            histo_sum =0   
            
            single_1_counter = 0
            
            single_2_counter = 0
            
print 'Single 1'

print Single_1

print 'Single2'

print Single_2

np.savetxt(os.path.join(path, 'singles_1.txt'),Single_1)
    
np.savetxt(os.path.join(path, 'singles_2.txt'),Single_2)



             




#import numpy as np
#import scipy.special as sc
#import matplotlib.pyplot as plt
#import os
#
##Plotting binomial distribution
#
#path = r"C:\Users\User\Desktop\Measurements\160422\160422_162316___2x10s"
#praefix="160422_162317___th_run_10sec.txt"
#
#data  = np.genfromtxt(os.path.join(path, praefix), usecols=np.arange(0,13), comments='#', invalid_raise=True)
#
#bins = data[:,1]
#
#chan4 = data[:,4]
#
#print bins
#
#
#
#
##plt.plot(x1, s(n1, x1))
#
##plt.plot(PL, events(PL), color='b', label='events')
#
##plt.figure()
##
##plt.plot(steps, clicks(steps) , color='y', label='clicks depending on step number')
##
##
##
###plt.plot(PL, test(PL) , color='g', label='test')
##
###plt.plot(PL, false_events(PL), color='r', label='false events')
###
###plt.plot(PL, filtered_events(PL)*10, color='g', label='filtered events * 10')
###
#
#plt.plot(bins, chan4, color='k', label='Counts Ch 4')
#
#plt.legend(loc=1)
#        
#plt.xlabel(r'Bins', fontsize=20)
#plt.ylabel(r'events', fontsize=20)
#
#plt.show()