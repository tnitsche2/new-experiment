# -*- coding: utf-8 -*-
"""
Created on Mon Apr 18 10:34:51 2016

@author: Thomas
"""

import bin2histo_new_exp as b2h

import os

import numpy as np

from glob import glob

from time import clock

import matplotlib.pyplot as plt

path= r"C:\Users\Thomas\Desktop\Messdaten\160425_HOM\160425_141112___2x1s"

name = "160425_141114___-1000.0_1sec.bin"

list_name= "actual_positions.txt"

culprit = b2h.detective(path, name, count=-1)

Vis_ch_3 =np.array([])

Vis_ch_6 =np.array([])

pos_list = np.genfromtxt(os.path.join(path, list_name), comments='#', invalid_raise=True)

#       count = 0
#        filename_list_comment = ''
#        filelist = []
#        filelist_errorFiles = []
for pfilename in sorted(glob(os.path.join(path, '*.bin'))):
    
    
    name = os.path.split(pfilename)[1]
#    filename_list_comment += '# ' + name + '\n'
#    filelist.append(name)
#    log.info("starting with file: %s  \n", name)
    t1 = clock()
    
    coinc_counter, ch_3_counter, ch_6_counter = b2h.get_coinc(path, name, count=-1)
    
    Vis_ch_3 = poslist = np.append(Vis_ch_3,coinc_counter/ch_3_counter)
    
    Vis_ch_6 = poslist = np.append(Vis_ch_6,coinc_counter/ch_6_counter)
    
    


    t2 = clock()
    print 'Time for loading data and creating one histo: '+str(t2-t1)+'s\n'
#    count += 1



plt.plot(pos_list, Vis_ch_3 , color='y', label='Vis_ch_3')

plt.plot(pos_list, Vis_ch_6 , color='y', label='Vis_ch_6')



plt.legend(loc=1)
        
plt.xlabel(r'steps', fontsize=20)
plt.ylabel(r'events', fontsize=20)

plt.show()