# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 10:09:30 2016

@author: User
"""

import bin2histo_new_exp as b2h

import os

import numpy as np

from glob import glob

from time import clock

import matplotlib.pyplot as plt

path= r"D:\Data\160426_175118"

list_path= r"C:\Users\User\Desktop\Measurements\160425"

name = "160426_175118___0.0_10sec.bin"

list_name= "actual_positions_160425_HOM_dip.txt"


coinc_interval = 100#10e-9/82.3045e-12
    
deadtime = 60e-9/82.3045e-12

coinc_peak_pos = 8600
    
false_peak_pos_1 = 7320

false_peak_pos_2 = 9870

dead_interval = 3000
global debugarray 

coinc, single_3, single_6, histo, t = b2h.get_coinc(path, name, coinc_interval, deadtime, coinc_peak_pos, false_peak_pos_1, false_peak_pos_2, dead_interval, count=-1)

print 'coinc'

print coinc

print 'single_3'

print single_3

print 'single_6'

print single_6

comment = 'used function data2histo_arrayOp'



txtfilename = os.path.join(path, name +
                                   "filtered_calcfrombin.txt")

b2h.savehisto2txt(txtfilename, histo, comment)