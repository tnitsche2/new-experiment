# -*- coding: utf-8 -*-
"""
Created on Mon May 23 13:38:32 2016

@author: Thomas
"""

import os

import numpy as np

from glob import glob

import bin2histo_new_exp as b2h

import matplotlib.pyplot as plt

path= r"D:\160719"

list_path= r"D:\160719"

list_name= "actual_positions.txt" 

singles_1_name="Singles_1.txt"

singles_2_name="Singles_2.txt"

pos_list = np.genfromtxt(os.path.join(list_path, list_name), comments='#', invalid_raise=True)

n = np.size(pos_list)

pos_list = pos_list[1:n-1]

singles_1 = np.genfromtxt(os.path.join(list_path, singles_1_name), comments='#', invalid_raise=True)
#
singles_2 = np.genfromtxt(os.path.join(list_path, singles_2_name), comments='#', dtype=np.float64)

#print 'singles_1[0]'
#
#print singles_1[0]

save = False



#s1=np.size(singles_1_list)
#  
#s2=np.size(singles_2_list)

#print 's1'
#
#print s1
#
#print singles_1_list
#
#print 's2'
#
#print s2
#
#print singles_2_list
#
#pos_list = pos_list[1:n-1]

#pos_list=np.linspace(1, 160, 160)


i = 0

counts = []

counts_norm=[]

data=np.zeros((6035,2))

for pfilename in sorted(glob(os.path.join(path,  '*_calcfrombin.txt'))):
    

    
#        data = np.genfromtxt(pfilename, comments='#', invalid_raise=True)
#        
#        print 'initial temp data'
#        
#        print temp_data
        
#        print 'Opening this'
#            
#        print pfilename
        
        temp_data = np.genfromtxt(pfilename, comments='#', invalid_raise=True)
        
        print 'opening this'
        
        print pfilename
        
#        print 'wqwwwwwwwwwwwwwwwwwwwwwwwwwww'
#        
#        print temp_data
        
        
        counts.append(np.sum(temp_data[:,0])) 
        
        if i==0:
            

            
            base_singles_1=singles_1[i]
            base_singles_2=singles_2[i]
        
        counts_norm.append(np.sum(temp_data[:,0])/(singles_1[i]**2*singles_2[i]**2)*(base_singles_1**2* base_singles_2**2))
        
             
        
#        print i
#        
#        print  'singles_1[i]'
#        
#        print singles_1[i]
#        
#        print  'singles_2[i]'
#        
#        print singles_2[i]
#        
#        print 'base_singles_1'
#        
#        print base_singles_1
#        
#        print 'base_singles_2'
#        
#        print base_singles_2
#        
#        print 'ratio'
#        
#        print (singles_1[i]**2*singles_2[i]**2)/(base_singles_1**2* base_singles_2**2)
        
#        print np.sum(temp_data[:,0])
#        
#        print 'ch 1'
#        
#        print np.sum(temp_data[:,0])
#        
#        print 'ch 2'
#        
#        print np.sum(temp_data[:,1])
#        
#        print 'ch 3'
#        
#        print np.sum(temp_data[:,2])
#        
#        print 'ch 4'
#        
#        print np.sum(temp_data[:,3])
#        
#        
#        print 'ch 5'
#        
#        print np.sum(temp_data[:,4])
        
        
        
        data = data + temp_data[:6035]
        
#        if i==0:
#            
#
#            
#            base_singles_1=singles_1[i]
#            base_singles_2=singles_2[i]
            
#            print 'base singles_1'
#            
#            print base_singles_1
#            
#            print 'base singles_2'
#            
#            print base_singles_2
        

        
        #data_norm=data/(singles_1[i]**2*singles_2[i]**2)*(base_singles_1**2* base_singles_2**2)
#        

#        
#        print 'ratio'
#        
#        print (singles_1[i]**2*singles_2[i]**2)/(base_singles_1**2* base_singles_2**2)
#        
#        print 'data_norm'
#        
#        print data_norm
#        
#        counts_norm.append(np.sum(data_norm[:,0])) 
        
        temp_data=[]
        
        i+=1
        
#        print temp_data
        
    
    
#print data
#
#print np.size(data)

time_bin=np.arange(0, np.size(data)/2, 1)

time_bin = time_bin.reshape((np.size(data)/2,1))  



#for i in np.arange(0, np.size(data), 1):
#
#    time_bin[:, i] = i

#print 'time_bin'
#
#print time_bin

data= np.column_stack((time_bin, data))

#m = np.size(counts)
#
#counts = counts[:m-1]

#print data

txtfilename = os.path.join(path + "\merged_calcfrombin.txt")

#print txtfilename

comment ='bli, bla, blub'


if save==True:


    b2h.savehisto2txt(txtfilename, data, comment)

#print 'done'
#
#print counts
#
#print pos_list
#
#print np.size(pos_list)
#
#print np.size(counts)

plt.plot(pos_list/200000*4.6, counts, color='b', label='Counts_unnorm')

plt.plot(pos_list/200000*4.6, counts_norm, color='g', label='Counts_norm')

plt.legend(loc=1)
        
plt.xlabel(r'delay / ps', fontsize=20)
plt.ylabel(r'events', fontsize=20)

plt.show()
    


