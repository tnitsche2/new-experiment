# -*- coding: utf-8 -*-
"""
Created on Fri Jun 27 17:10:34 2014

@author: tnitsche
"""

import numpy as np
import scipy.special as sc
import matplotlib.pyplot as plt

#Plotting binomial distribution


steps = np.arange(0, 30, 1)   #Single photon gen prob

Posspace = 104*10**(-9)

pgen = 0.05

pdetect= 0.35

l = 0.8




def clicks(steps):

    return 1/(Posspace*steps*(steps+1)) * pgen * pdetect**2 * l**(2*steps)/(steps+1)

    

    



#plt.plot(x1, s(n1, x1))

#plt.plot(PL, events(PL), color='b', label='events')

plt.figure()

plt.plot(steps, clicks(steps) , color='y', label='clicks depending on step number')



#plt.plot(PL, test(PL) , color='g', label='test')

#plt.plot(PL, false_events(PL), color='r', label='false events')
#
#plt.plot(PL, filtered_events(PL)*10, color='g', label='filtered events * 10')
#
#plt.plot(PL, ratio(PL), color='k', label='ratio true/false / 10')

plt.legend(loc=1)
        
plt.xlabel(r'steps', fontsize=20)
plt.ylabel(r'events', fontsize=20)

plt.show()