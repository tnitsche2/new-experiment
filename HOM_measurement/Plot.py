# -*- coding: utf-8 -*-
"""
Created on Wed May 11 09:11:07 2016

@author: User
"""


import matplotlib.pyplot as plt


path= r"D:\Data\160510_172116_1_2_power"

#list_path= r"D:\Data\160510_172116_1_2_power"

name = "Vis.txt"

list_name= "actual_positions.txt"


Vis = np.genfromtxt(os.path.join(path, name), comments='#', invalid_raise=True)

print Vis


pos_list = np.genfromtxt(os.path.join(path, list_name), comments='#', invalid_raise=True)

n = np.size(pos_list)
pos_list = pos_list[1:n-1]

print pos_list


plt.plot(pos_list/200000*4.6, Vis, color='b', label='Vis')

#plt.plot(pos_list/200000*4.6, (Single_1/base_single_1_counter), color='r', label='rel. Singles 1')
#
#plt.plot(pos_list/200000*4.6, (Single_2/base_single_2_counter), color='k', label='rel. Singles 2')


plt.legend(loc=1)
        
plt.xlabel(r'delay / ps', fontsize=20)
plt.ylabel(r'events', fontsize=20)

plt.show()